package com.alvinhkh.ftpsync.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.alvinhkh.ftpsync.BatteryState;
import com.alvinhkh.ftpsync.Constants;

public class PowerReceiver extends BroadcastReceiver {

    private static String TAG = "PowerReceiver";

    @Override
    public void onReceive(Context context, Intent mIntent) {

        BatteryState batteryState = new BatteryState(context);

        Log.d(TAG, "isCharging: " + batteryState.isCharging().toString());
        Log.d(TAG, "onUSB: " + batteryState.onUSB().toString());
        Log.d(TAG, "onAC: " + batteryState.onAC().toString());
        Log.d(TAG, "onBattery: " + batteryState.onBattery().toString());
        Log.d(TAG, "batteryPercentage: " + batteryState.getPercentage().toString());

        Intent intent = new Intent(Constants.ACTION.BATTERY_STATE);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }
}