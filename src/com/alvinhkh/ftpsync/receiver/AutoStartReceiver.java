package com.alvinhkh.ftpsync.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AutoStartReceiver extends BroadcastReceiver
{
    private final String TAG = "AutoStart";

    public void onReceive(Context mContent, Intent mIntent)
    {
        Log.d(TAG, "Received");
        /*
        Intent i = new Intent(mContent, ForegroundService.class);
        mContent.startService(i);
        Log.i(TAG, "Auto Started");
        */
    }
}