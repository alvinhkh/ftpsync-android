package com.alvinhkh.ftpsync.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.sync.SyncService;
import com.alvinhkh.ftpsync.sync.TransferService;

public class CancelReceiver extends BroadcastReceiver {

    private static final String TAG = "CancelReceiver";

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        Bundle extras = intent.getExtras();
        if (intent.getAction().equals(Constants.TRIGGER.CANCEL)) {
            if (extras.getBoolean("click") == true) {
                Intent sIntent = new Intent(mContext, SyncService.class);
                mContext.stopService(sIntent);
                Intent tIntent = new Intent(mContext, TransferService.class);
                mContext.stopService(tIntent);

                LocalBroadcastManager.getInstance(mContext).sendBroadcast(
                        new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                                .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.cancel_operation)
                                .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false));
                Toast.makeText(mContext, mContext.getString(R.string.cancel_operation), Toast.LENGTH_SHORT).show();

                Intent syncIntent = new Intent(mContext, SyncService.class);
                syncIntent.setAction(Constants.ACTION.STOP_SERVICE);
                mContext.startService(syncIntent);

                Intent stopIntent = new Intent(mContext, TransferService.class);
                stopIntent.setAction(Constants.ACTION.STOP_SERVICE);
                mContext.startService(stopIntent);
            }
        }

    }

}