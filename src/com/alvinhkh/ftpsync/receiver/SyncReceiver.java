package com.alvinhkh.ftpsync.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.log.LogProvider;
import com.alvinhkh.ftpsync.log.LogTable;
import com.alvinhkh.ftpsync.sync.ForegroundService;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;

import java.util.Calendar;
import java.util.Date;

public class SyncReceiver extends BroadcastReceiver {

    private static final String TAG = "SyncReceiver";

    private PendingIntent pAlarmIntent;
    private AlarmManager mAlarm;
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

        // Intent to fire Sync Action
        Intent intentSync = new Intent(context, ForegroundService.class)
                .setAction(Constants.ACTION.SYNC)
                .putExtra(Constants.TRIGGER.IS, Constants.TRIGGER.ALARM);

        // Check alarm service started
        boolean alarmStarted = (PendingIntent.getService(context.getApplicationContext(), 0,
                intentSync, PendingIntent.FLAG_NO_CREATE) != null);

        Bundle data = intent.getExtras();
        Boolean startClock = data.getBoolean(Constants.ACTION.START_CLOCK);
        Boolean stopClock = data.getBoolean(Constants.ACTION.STOP_CLOCK);

        PreferencesHelper syncSettings = new PreferencesHelper().getSyncSettings(context);
        Boolean autoSyncEnable = syncSettings.getSyncAuto();
        Integer syncInterval = syncSettings.getSyncInterval();

        Boolean start = startClock && (autoSyncEnable == true) && (syncInterval > 0);
        Boolean stop = stopClock || (autoSyncEnable == false) || (syncInterval < 1);

        // Setup AlarmManager
        mAlarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        // Retrieve a PendingIntent that will perform a broadcast
        pAlarmIntent = PendingIntent.getService(context.getApplicationContext(), 0, intentSync, PendingIntent.FLAG_UPDATE_CURRENT);


        if (start == true) {

            Log.d(TAG, "Auto Sync - Start (" + syncInterval + ")");
            // Set the alarm to start at 4 seconds
            Calendar startCal = Calendar.getInstance();
            startCal.setTimeInMillis(System.currentTimeMillis());
            startCal.add(Calendar.SECOND, 4);
            // Set the alarm to start at 10:30 AM
            //startCal.setTimeInMillis(System.currentTimeMillis());
            //startCal.set(Calendar.HOUR_OF_DAY, 10);
            //startCal.set(Calendar.MINUTE, 30);
            // Repeating on every X time interval
            mAlarm.setRepeating(AlarmManager.RTC_WAKEUP, startCal.getTimeInMillis(),
                    syncInterval, pAlarmIntent);
            addMessageToLog(0, mContext.getString(R.string.auto_sync_enabled));

        } else if (stop == true && alarmStarted) {

            Log.d(TAG, "Auto Sync - Stop (" + syncInterval + ")");
            mAlarm.cancel(pAlarmIntent);
            if (stopClock) {
                addMessageToLog(0, mContext.getString(R.string.auto_sync_disabled));
            }

        }

    }

    private long addMessageToLog(long logID, String action) {
        if (mContext == null) return 0;

        ContentValues values = new ContentValues();
        values.put(LogTable.COLUMN_LOCAL, "");
        values.put(LogTable.COLUMN_REMOTE, "");
        values.put(LogTable.COLUMN_ACTION, action);
        values.put(LogTable.COLUMN_DATE, new Date().getTime());
        values.put(LogTable.COLUMN_STATUS, true);
        values.put(LogTable.COLUMN_TYPE, "message");

        Uri logUri = Uri.parse(LogProvider.CONTENT_URI + "/" + logID);
        int updated = mContext.getContentResolver().update(logUri, values, LogTable.COLUMN_ACTION + " = ?", new String[] {action});
        if (updated > 0) {
            Log.d(TAG, "Update Log " + logID + " (" + updated + "): " + action);
        } else {
            logUri = mContext.getContentResolver().insert(LogProvider.CONTENT_URI, values);
            if (logUri != null) {
                try {
                    logID = Long.parseLong(logUri.getFragment());
                } catch (NumberFormatException e) {
                    logID = 0;
                }
            }
        }
        return logID;
    }

}