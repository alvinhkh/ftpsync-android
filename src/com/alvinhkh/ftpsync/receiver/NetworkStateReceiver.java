package com.alvinhkh.ftpsync.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.net.Connectivity;

public class NetworkStateReceiver extends BroadcastReceiver
{
    private final String TAG = "NetworkStateReceiver";

    public void onReceive(Context context, Intent mIntent)
    {
        String state = new Connectivity(context).getState();
        Log.d(TAG, state);

        Intent intent = new Intent(Constants.ACTION.NETWORK_STATE)
                .putExtra(Constants.ACTION.NETWORK_STATE, state);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}