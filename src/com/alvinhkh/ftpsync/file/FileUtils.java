package com.alvinhkh.ftpsync.file;

import android.util.Log;

import org.apache.commons.io.comparator.NameFileComparator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class FileUtils {

    private static String TAG = "FileUtils";

    /**
     * Get a list of filenames in this folder.
     * @param folder  Full path of directory 
     * @param fileNameFilterPattern  Regular expression
     * @param sort  Use 1 for CASE_INSENSITIVE_ORDER, 0 for no sort, -1 for reverse sort
     * @return list of filenames (the names only, not the full path),
     *     or null if folder doesn't exist or isn't a directory,
     *     or if nothing matches fileNameFilterPattern
     * @throws PatternSyntaxException if fileNameFilterPattern is non-null and isn't a
     *     valid Java regular expression
     */
    public static List<File> listFiles
    (final String folder, final String fileNameFilterPattern, final int sort)
            throws PatternSyntaxException
    {
        List<File> returnList = new ArrayList<File>();
        File fileDir = new File(folder);
        if(!fileDir.exists() || !fileDir.isDirectory()){
            return null;
        }

        File[] files = fileDir.listFiles();

        if (files.length == 0){
            return null;
        }
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                List<File> subList = listFiles(files[i].getPath(), fileNameFilterPattern, sort);
                if (subList != null) {
                    returnList.addAll(subList);
                } else {
                    returnList.add(files[i]);
                }
            } else {
                if(fileNameFilterPattern == null ||
                        files[i].getName().matches(fileNameFilterPattern)) {
                    returnList.add(files[i]);
                }
            }
        }
        if (returnList.size() == 0)
            return null;

        // Sorting
        if (sort != 0)
        {
            Collections.sort(returnList, NameFileComparator.NAME_COMPARATOR);
            if (sort < 0)
                Collections.sort(returnList, NameFileComparator.NAME_REVERSE);
        }

        return returnList;
    }

    /**
     * Get a list of file in a given directory in form of FileListHelper
     * @return List of files/directories
     */
    public static List<FileListHelper> getFileList(String rootPath)
    {
        List<FileListHelper> localList = new ArrayList<FileListHelper>();
        File rootLocalFile = new File(rootPath);
        try {
            List<File> files = new ArrayList<File>();
            if (rootLocalFile.exists() && rootLocalFile.isFile()) {
                files.add(rootLocalFile);
            } else {
                files = FileUtils.listFiles(rootPath, null, 0);
            }
            for (File file : files) {
                if (file.getName().startsWith(".") || file.getName().startsWith("~")) {
                    if (file.getName().startsWith(".clocksync")) {
                        Log.d(TAG, "delete .clocksync in local directory");
                        Boolean delete = new File(file.getPath()).delete();
                    }
                    continue;
                }
                String filePath = file.getPath();
                File localFile = new File(filePath);
                if (rootPath.equals(filePath)) {
                    filePath = File.separator + file.getName();
                } else {
                    filePath = filePath.replaceFirst(Pattern.quote(rootPath), "");
                }
                FileListHelper fileListHelper = new FileListHelper();
                fileListHelper.setPath(filePath);
                fileListHelper.setLastModified(new Date(file.lastModified()));
                fileListHelper.setIsDirectory(localFile.isDirectory());
                fileListHelper.setIsFile(localFile.isFile());
                localList.add(fileListHelper);
            }
        } catch (PatternSyntaxException e) {
            e.printStackTrace();
        }

        return localList;
    }

    public static void write(File file, String s) throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(s);
        writer.close();
    }

    public static String read(String path) throws IOException
    {
        BufferedReader br = new BufferedReader(new FileReader(path));
        StringBuilder sb = new StringBuilder();
        try {
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }

    public static String getDirectory(String path) {
        File file = new File(path);
        if (file.isFile()) {
            return file.getParent();
        } else if (file.isDirectory()) {
            return path;
        }
        return null;
    }

    public static String getShortPath(String filePath, String rootPath) {
        String returnPath;
        File file = new File(filePath);
        if (rootPath.equals(filePath)) {
            returnPath = File.separator + file.getName();
        } else {
            returnPath = filePath.replaceFirst(Pattern.quote(rootPath), "");
        }
        return replaceSlashes(returnPath);
    }

    public static String replaceSlashes(String filePath) {
        return filePath.replace("//", "/").replace("\\/\\/", "\\/");
    }

    public static Boolean isFile(String path) {
        return new File(path).isFile();
    }

    public static String getName(String path) {
        return new File(path).getName();
    }

/*
    private class FileComparator implements Comparator<FileListHelper> {
        @Override
        public int compare(FileListHelper a, FileListHelper b) {
            if (a.getPath().compareToIgnoreCase(b.getPath()) == 1 &&
                    a.getLastModified().compareTo(b.getLastModified()) == 1) {
                return 1;
            }
            return 0;
        }
    }

    public int isSameFile(FileListHelper a, FileListHelper b) {
        return new FileComparator().compare(a, b);
    }
*/
}
