/* ownCloud Android client application
 *   Copyright (C) 2012 Bartek Przybylski
 *   Copyright (C) 2012-2013 ownCloud Inc.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alvinhkh.ftpsync.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import android.app.Service;
import android.os.FileObserver;
import android.util.Log;

public class RecursiveFileObserver extends FileObserver
{
    private final String TAG = "FileObserver";

    private Service mService;
    private List<SingleFileObserver> mObservers;
    private String mLocalPath;
    private Integer mMask;

    public RecursiveFileObserver(Service service,
                                 String localPath) {
        this(service, localPath, ALL_EVENTS);
    }

    public RecursiveFileObserver(Service service,
                                 String localPath,
                                 Integer mask) {
        super(localPath, mask);
        mService = service;
        mLocalPath = localPath;
        mMask = mask;
    }

    @Override
    public void startWatching() {
        if (mObservers != null) return;
        Log.d(TAG, "startWatching");
        mObservers = new ArrayList<SingleFileObserver>();
        Stack<String> stack = new Stack<String>();
        stack.push(mLocalPath);

        while (!stack.empty()) {
            String parent = stack.pop();
            mObservers.add(new SingleFileObserver(parent, mMask));
            File path = new File(parent);
            File[] files = path.listFiles();
            if (files == null) continue;
            for (int i = 0; i < files.length; ++i) {
                if (files[i].isDirectory() && !files[i].getName().equals(".")
                        && !files[i].getName().equals("..")) {
                    stack.push(files[i].getPath());
                }
            }
        }
        for (int i = 0; i < mObservers.size(); i++)
            mObservers.get(i).startWatching();
    }

    @Override
    public void stopWatching() {
        if (mObservers == null) return;
        Log.d(TAG, "stopWatching");

        for (int i = 0; i < mObservers.size(); ++i)
            mObservers.get(i).stopWatching();

        mObservers.clear();
        mObservers = null;
    }

    @Override
    public void onEvent(int event, String path) {
        File cFile = new File(path);
        switch (event & ALL_EVENTS) {
            case MOVED_TO:
            case CLOSE_WRITE:
            case MODIFY:
                Log.d(TAG, getEventString(event) + " | " + path);
                   /*
                // Upload
                Intent intentUpload = new Intent(mService, TransferService.class);
                intentUpload.putExtra(Constants.ACTION.TRANSFER, Constants.NOTIFICATION_ID.ACTION_UPLOAD);
                if (cFile.exists()) {
                    FileTransferData tFile = new FileTransferData();
                    tFile.setFromFileName(cFile.getName());
                    tFile.setFromDirectory(cFile.getPath());
                    tFile.setToDirectory("/Public");
                    tFile.setToFileName(cFile.getName());
                    intentUpload.putExtra(Constants.TRANSFER.FILES, tFile);
                    mService.startService(intentUpload);
                }*/
                break;
            case DELETE:
                Log.d(TAG, getEventString(event) + " | " + path);
                /*
                // Delete
                Intent intentDelete = new Intent(mService, TransferService.class);
                intentDelete.putExtra(Constants.ACTION.TRANSFER, Constants.NOTIFICATION_ID.ACTION_REMOVE_REMOTE);
                if (cFile.exists()) {
                    FileTransferData tFile = new FileTransferData();
                    tFile.setFromFileName(cFile.getName());
                    tFile.setFromDirectory(cFile.getPath());
                    tFile.setToDirectory("/Public");
                    tFile.setToFileName(cFile.getName());
                    intentDelete.putExtra(Constants.TRANSFER.FILES, tFile);
                    mService.startService(intentDelete);
                }*/
                break;
            case ATTRIB:
            case MOVED_FROM:
            case DELETE_SELF:
            case MOVE_SELF:
                Log.d(TAG, getEventString(event) + " | " + path);
                break;
        }
    }

    private static String getEventString(int event) {
        switch (event) {
            case ACCESS:
                return "ACCESS";
            case MODIFY:
                return "MODIFY";
            case ATTRIB:
                return "ATTRIB";
            case CLOSE_WRITE:
                return "CLOSE_WRITE";
            case CLOSE_NOWRITE:
                return "CLOSE_NOWRITE";
            case OPEN:
                return "OPEN";
            case MOVED_FROM:
                return "MOVED_FROM";
            case MOVED_TO:
                return "MOVED_TO";
            case CREATE:
                return "CREATE";
            case DELETE:
                return "DELETE";
            case DELETE_SELF:
                return "DELETE_SELF";
            case MOVE_SELF:
                return "MOVE_SELF";
            default:
                return "UNKNOWN - " + event;
        }
    }

    private class SingleFileObserver extends FileObserver {
        private String mPath;

        public SingleFileObserver(String path, int mask) {
            super(path, mask);
            mPath = path;
        }

        @Override
        public void onEvent(int event, String path) {
            String newPath = mPath + File.separator + path;
            RecursiveFileObserver.this.onEvent(event, newPath);
        }

    }
}