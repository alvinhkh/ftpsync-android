package com.alvinhkh.ftpsync;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

public class BatteryState {

    Boolean isCharging = false;
    Boolean usbCharge = false;
    Boolean acCharge = false;
    Boolean onBattery = false;

    Integer level = 0;
    Integer scale = 100;

    public BatteryState(Context context) {

        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = context.getApplicationContext().registerReceiver(null, filter);

        // Are we charging / charged?
        Integer status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        // How are we charging?
        Integer chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        onBattery = chargePlug == 0;
        usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        // Battery Level
        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);
        level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

    }

    public Float getPercentage() {
        return level / (float) scale;
    }

    public Boolean isCharging() {
        return isCharging;
    }

    public Boolean onUSB() {
        return usbCharge;
    }

    public Boolean onAC() {
        return acCharge;
    }

    public Boolean onBattery() {
        return onBattery;
    }

}
