package com.alvinhkh.ftpsync.sync;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.alvinhkh.ftpsync.BatteryState;
import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.view.MainActivity;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.log.LogProvider;
import com.alvinhkh.ftpsync.synclist.PathsProvider;
import com.alvinhkh.ftpsync.log.LogTable;
import com.alvinhkh.ftpsync.synclist.PathsTable;
import com.alvinhkh.ftpsync.file.RecursiveFileObserver;
import com.alvinhkh.ftpsync.net.Connectivity;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.receiver.CancelReceiver;
import com.alvinhkh.ftpsync.view.dialog.SyncSelectActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ForegroundService extends Service
        implements Loader.OnLoadCompleteListener<Cursor>,
                   SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "ForegroundService";
    private Boolean serviceStarted = false;
    private Boolean cursorLoaded = false;

    protected NotificationManager mNotifyManager;
    protected Notification.Builder mBuilder;
    private CursorLoader mCursorLoader;

    RecursiveFileObserver fileObserver = null;
    ArrayList<FileTransferHelper> tFiles = null;

    public ForegroundService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent().setAction(Constants.ACTION.FOREGROUND_STATE));
        LocalBroadcastManager.getInstance(this).registerReceiver(progressReceiver, new IntentFilter(Constants.ACTION.TRANSFER_PROGRESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(singleProgressReceiver, new IntentFilter(Constants.ACTION.SINGLE_TRANSFER));

        Log.d(TAG, "Created");

        // Foreground Notification
        mBuilder = new Notification.Builder(getApplicationContext());
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
        mBuilder.setContentTitle(getString(R.string.is_running))
                .setContentInfo(getString(R.string.app_name))
                .setSmallIcon(R.drawable.ic_cloud_white_48dp)
                .setContentIntent(pendingIntent);
        mBuilder.addAction(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.button_cancel),
                getPendingAction(getApplicationContext(), Constants.TRIGGER.CANCEL));
        setNotificationAction(getString(R.string.is_running), "", R.drawable.ic_cloud_white_48dp);

        // Cursor loader, get selected paths from database
        String[] projection = { PathsTable.COLUMN_ID, PathsTable.COLUMN_LOCAL, PathsTable.COLUMN_REMOTE };
        String selection = null;
        String[] selectionArgs = null;
        String orderBy = null;
        mCursorLoader = new CursorLoader(this,
                PathsProvider.CONTENT_URI, projection, selection, selectionArgs, orderBy);
        mCursorLoader.registerListener(Constants.ACTION_ID.CURSOR_LOADER, this);
        mCursorLoader.startLoading();

        // Set up a listener whenever a key changes
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        startingService();
    }

    private PendingIntent getPendingAction(Context context, String action) {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(context, CancelReceiver.class);
        intent.setAction(action);
        intent.putExtra("click", true);
        return PendingIntent.getBroadcast(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void startingService() {
        serviceStarted = true;

        boolean alarmStarted = (PendingIntent.getService(this.getApplicationContext(), 0,
                new Intent(this, ForegroundService.class)
                        .setAction(Constants.ACTION.SYNC),
                PendingIntent.FLAG_NO_CREATE) != null);
        Log.d(TAG, "Alarm Started: " + alarmStarted);

        // Start Sync Clock
        Intent in = new Intent();
        in.setAction(Constants.ACTION.SYNC_CLOCK);
        in.putExtra(Constants.ACTION.START_CLOCK, true);
        sendBroadcast(in);

        // File Observer
        //String localDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/Folder";
        //fileObserver = new RecursiveFileObserver(this, localDir);
        //fileObserver.startWatching();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            stopSelf();
            return -1;
        }
        String action = intent.getAction();
        if (action == null) return START_STICKY;
        if (action.equals(Constants.ACTION.START_FOREGROUND)) {
            /*if (isServiceRunning(this.getClass())) {
                Log.d(TAG, "Started");
            } else {*/
            //Toast.makeText(this, "starting", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Starting");
            startingService();
        } else if (action.equals(Constants.ACTION.STOP_FOREGROUND)) {
            serviceStarted = false;
            Log.i(TAG, "Stopping");

            // Stop Service
            stopForeground(true);
            stopSelf();
        } else if (action.equals(Constants.ACTION.SYNC)) {
            String triggerIs = intent.getStringExtra(Constants.TRIGGER.IS);
            Boolean triggerByAlarm = triggerIs != null && triggerIs.equals(Constants.TRIGGER.ALARM);
            Log.d(TAG, "Sync Trigger by Alarm: " + triggerByAlarm.toString());
            // Connectivity
            Boolean isNotMobile = new Connectivity(this).isInWifiOrEthernet();
            // Power
            BatteryState batteryState = new BatteryState(this);
            Float batteryPercentage = batteryState.getPercentage();
            // Sync Settings
            PreferencesHelper syncSettings = new PreferencesHelper().getSyncSettings(this);
            Boolean autoSyncEnable = syncSettings.getSyncAuto();
            Integer syncInterval = syncSettings.getSyncInterval();
            Boolean wifiOnly = syncSettings.getSyncWifiOnly();
            String powerCondition = syncSettings.getSyncPower();
            Log.d(TAG, "Wifi Only: " + wifiOnly.toString());
            Log.d(TAG, "Sync Power: " + powerCondition);
            Log.d(TAG, "Sync Battery: " + syncSettings.getSyncBattery());
            Log.d(TAG, "Current Battery: " + batteryPercentage);

            Boolean meetPowerCondition = true;
            if (powerCondition != "Any") {
                if (powerCondition.equals("AC") && !batteryState.onAC()) {
                    meetPowerCondition = false;
                }
                if (powerCondition.equals("AC or USB") && !(batteryState.onAC() || batteryState.onUSB())) {
                    meetPowerCondition = false;
                }
            }

            if (triggerByAlarm && (autoSyncEnable == false || syncInterval < 1)) {
                // Stop Sync Clock
                Intent in = new Intent();
                in.setAction(Constants.ACTION.SYNC_CLOCK);
                in.putExtra(Constants.ACTION.STOP_CLOCK, true);
                sendBroadcast(in);
                Log.d(TAG, "Auto Sync is disable, clock stopped.");
            } else if (isNotMobile == false && wifiOnly == true) {
                Log.d(TAG, "Sync in Wifi Only and now it is not.");
                Intent iMessage = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.criteria_wifi_only)
                        .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false);
                LocalBroadcastManager.getInstance(this).sendBroadcast(iMessage);
            } else if (batteryPercentage < syncSettings.getSyncBattery()) {
                Log.d(TAG, "Battery lower than preferable.");
                Intent iMessage = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.criteria_battery_low)
                        .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false);
                LocalBroadcastManager.getInstance(this).sendBroadcast(iMessage);
            } else if (meetPowerCondition == false) {
                Log.d(TAG, "Fail to meet the power condition.");
                Intent iMessage = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.criteria_power_condition)
                        .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false);
                LocalBroadcastManager.getInstance(this).sendBroadcast(iMessage);
            } else {

                if (cursorLoaded) {
                    if (tFiles == null || tFiles.isEmpty()) {
                        Log.d(TAG, "No directories to synchronise.");
                        Intent iMessage = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                                .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.sync_list_empty)
                                .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(iMessage);
                    } else {
                        setNotificationAction(getString(R.string.action_sync),
                                getString(R.string.action_sync) + " " + getString(R.string.started),
                                R.drawable.ic_cloud_white_48dp);
                        Intent i = new Intent(this, SyncService.class);
                        i.putExtra(Constants.TRIGGER.IS, intent.getStringExtra(Constants.TRIGGER.IS));
                        i.putParcelableArrayListExtra(Constants.TRANSFER.FILES, tFiles);
                        startService(i);
                    }
                } else {
                    Log.d(TAG, "Cursor is not loaded.");
                }

            }
        } else if (action.equals(Constants.ACTION.UPLOAD)) {
            if (cursorLoaded) {
                if (tFiles == null) {
                    Toast.makeText(this, getString(R.string.sync_list_empty), Toast.LENGTH_SHORT).show();
                } else {
                    setNotificationAction(getString(R.string.action_upload),
                            getString(R.string.action_upload) + " " + getString(R.string.started),
                            R.drawable.ic_file_upload_white_48dp);

                    commandTransfer(tFiles, Constants.TRANSFER.UPLOAD);
                }
            } else {
                Log.d(TAG, "Cursor is not loaded.");
            }
        } else if (action.equals(Constants.ACTION.DOWNLOAD)) {
            if (cursorLoaded) {
                if (tFiles == null) {
                    Toast.makeText(this, getString(R.string.sync_list_empty), Toast.LENGTH_SHORT).show();
                } else {
                    setNotificationAction(getString(R.string.action_download),
                            getString(R.string.action_download) + " " + getString(R.string.started),
                            R.drawable.ic_file_download_white_48dp);

                    commandTransfer(tFiles, Constants.TRANSFER.DOWNLOAD);
                }
            } else {
                Log.d(TAG, "Cursor is not loaded.");
            }
        } else if (action.equals(Constants.ACTION.REMOVE_REMOTE)) {
            if (cursorLoaded) {
                if (tFiles == null) {
                    Toast.makeText(this, getString(R.string.sync_list_empty), Toast.LENGTH_SHORT).show();
                } else {
                    setNotificationAction(getString(R.string.action_remove_remote),
                            getString(R.string.action_remove_remote) + " " + getString(R.string.started),
                            R.drawable.ic_remove_circle_outline_white_48dp);

                    commandTransfer(tFiles, Constants.TRANSFER.REMOVE_REMOTE);
                }
            } else {
                Log.d(TAG, "Cursor is not loaded.");
            }
        } else if (action.equals(Constants.ACTION.DELETE_LOCAL)) {
            if (cursorLoaded) {
                if (tFiles == null) {
                    Toast.makeText(this, getString(R.string.sync_list_empty), Toast.LENGTH_SHORT).show();
                } else {
                    setNotificationAction(getString(R.string.action_delete_local),
                            getString(R.string.action_delete_local) + " " + getString(R.string.started),
                            R.drawable.ic_remove_circle_outline_white_48dp);

                    commandTransfer(tFiles, Constants.TRANSFER.DELETE_LOCAL);
                }
            } else {
                Log.d(TAG, "Cursor is not loaded.");
            }
        }
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onLoadComplete(Loader<Cursor> loader, Cursor data) {
        // Bind data to UI, etc
        if (data != null) {
            Log.d(TAG, "Cursor Load Complete");
            cursorLoaded = true;
            tFiles = new ArrayList<FileTransferHelper>();
            if (data.moveToFirst()) {
                do {
                    String localPath = data.getString(data.getColumnIndex("local"));
                    String remotePath = data.getString(data.getColumnIndex("remote"));
                    FileTransferHelper tFile = new FileTransferHelper();
                    tFile.setLocalAbsolutePath(localPath);
                    tFile.setRemoteAbsolutePath(remotePath);
                    tFile.setIsDirectoryOnly(true);
                    tFiles.add(tFile);
                } while (data.moveToNext());
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent().setAction(Constants.ACTION.FOREGROUND_STATE));
        Log.d(TAG, "Stopped");

        // Stop Sync Clock
        Intent in = new Intent();
        in.setAction(Constants.ACTION.SYNC_CLOCK);
        in.putExtra(Constants.ACTION.STOP_CLOCK, true);
        sendBroadcast(in);

        // Stop File Observer
        if (fileObserver != null) {
            fileObserver.stopWatching();
        }

        // Stop the cursor loader
        if (mCursorLoader != null) {
            mCursorLoader.unregisterListener(this);
            mCursorLoader.cancelLoad();
            mCursorLoader.stopLoading();
        }

        // Unregister the listener whenever a key changes
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        // Unregister Local Broadcast Receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(progressReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(singleProgressReceiver);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        /*
        Log.d(TAG, "Preferences Changed.");
        // Get All Preferences
        Map<String, ?> allEntries = sp.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d(TAG, entry.getKey() + ": " + entry.getValue().toString());
        }*/
        if (key.matches("sync_automatically|sync_interval")) {
            // Restart Sync Clock
            Intent in = new Intent();
            in.setAction(Constants.ACTION.SYNC_CLOCK);
            in.putExtra(Constants.ACTION.START_CLOCK, true);
            sendBroadcast(in);
            mCursorLoader.startLoading();
        } else if (key.matches("notification_always_on")) {
            // Update Notification
            if (sp.getBoolean("notification_always_on", true)) {
                startForeground(Constants.NOTIFICATION.FOREGROUND_SERVICE, mBuilder.build());
            } else {
                stopForeground(true);
                mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotifyManager.notify(Constants.NOTIFICATION.FOREGROUND_SERVICE, mBuilder.build());
            }
        }
    }

    private void setNotificationAction(String title, String text, Integer res) {

        PreferencesHelper notificationSettings = new PreferencesHelper().getNotificationSettings(this);
        if (notificationSettings.getNotificationAlwaysOn()) {
            startForeground(Constants.NOTIFICATION.FOREGROUND_SERVICE, mBuilder.build());
        } else {
            stopForeground(true);
        }

        if (mBuilder != null) {
            mBuilder.setContentTitle(title)
                    .setContentText(text)
                    .setSmallIcon(res);
            // Update Notification
            mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotifyManager.notify(Constants.NOTIFICATION.FOREGROUND_SERVICE, mBuilder.build());
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(
                new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE, text)
                        .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false));

    }

    private BroadcastReceiver progressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals(Constants.ACTION.TRANSFER_PROGRESS)) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    // Indeterminate Progress
                    Boolean indeterminate = extras.getBoolean(Constants.TRANSFER.PROGRESS_INDETERMINATE, false);
                    // Message by string id
                    Integer messageId = extras.getInt(Constants.TRANSFER.MESSAGE_ID, 0);
                    if (messageId > 0) {
                        if (mBuilder != null) mBuilder.setProgress(0, 0, indeterminate);
                        setText(getString(messageId));
                    }
                    // Message by text
                    String message = extras.getString(Constants.TRANSFER.MESSAGE, "");
                    if (!message.equals("")) {
                        if (mBuilder != null) mBuilder.setProgress(0, 0, indeterminate);
                        setText(message);
                    }

                    // Transfer Progress
                    ProgressHelper pData = extras.getParcelable(Constants.TRANSFER.PROGRESS);
                    if (pData != null) {
                        if (pData.getMaxProgress() > 0 && pData.getProgress() > 0) {
                            if (mBuilder != null) mBuilder.setProgress(pData.getMaxProgress(), pData.getProgress(), false);
                        } else if (pData.getProgress() > 0) {
                            if (mBuilder != null) mBuilder.setProgress(0, pData.getProgress(), pData.getIndeterminate());
                        }
                        if (!pData.getMessage().equals("")) {
                            setText(getString(R.string.in_progress) + ": " + pData.getMessage());
                        }
                    }
                    ProgressHelper data = extras.getParcelable(Constants.TRANSFER.SECOND_PROGRESS);
                    if (extras.getBoolean(Constants.TRANSFER.PROGRESS_DONE, false) == true) {
                        progressDone();
                    } else if (data != null) {
                        if (mBuilder != null) {
                            mBuilder.setProgress(data.getMaxProgress(), data.getProgress(), data.getIndeterminate());
                            setText(data.getMessage());
                        }
                    }

                    if (extras.getBoolean(Constants.TRANSFER.RETURN_STATUS) == true) {
                        progressDone();
                        setText(getString(R.string.complete));
                    } else if (extras.getBoolean(Constants.TRANSFER.RETURN_STATUS, true) == false) {
                        progressDone();
                        //setText(getString(R.string.some_fail_to_progress));
                    } else if (extras.getBoolean(Constants.TRANSFER.RETURN_FILE_NOT_FOUND, false) == true) {
                        progressDone();
                        setText(getString(R.string.some_file_not_found));
                    } else if (extras.getBoolean(Constants.TRANSFER.RETURN_CONNECTION_CLOSED, false) == true) {
                        progressDone();
                        setText(getString(R.string.some_connection_lost));
                    }

                    // Update Notification
                    if (mBuilder != null) {
                        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotifyManager.notify(Constants.NOTIFICATION.FOREGROUND_SERVICE, mBuilder.build());
                    }
                }
            }
        }

        private void progressDone() {
            // Removes the Progress bar
            if (mBuilder != null) {
                mBuilder.setProgress(0, 0, false);
            }
        }

        private void setText(String text) {
            if (text == null || text.equals("") || mBuilder == null) return;
            if (text.equals(getString(R.string.complete))) {
                mBuilder.setContentTitle(getString(R.string.is_running))
                        .setSmallIcon(R.drawable.ic_cloud_white_48dp);
            }
            mBuilder.setContentText(text);
            //mBuilder.setTicker(text);
            String pattern = android.text.format.DateFormat.getBestDateTimePattern(Locale.getDefault(), "yyyy-MM-dd HH:mm:ss");
            String currentTime = new SimpleDateFormat(pattern).format(System.currentTimeMillis());
            Notification.InboxStyle style = new Notification.InboxStyle();
            style.addLine(text);
            //style.setBigContentTitle("Here Your Messages");
            style.setSummaryText(currentTime);
            mBuilder.setStyle(style);
            addMessageToLog(0, text);
        }
    };

    private BroadcastReceiver singleProgressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() != null && intent.getAction().equals(Constants.ACTION.SINGLE_TRANSFER)) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    List<FileTransferHelper> tFiles = extras.getParcelableArrayList(Constants.TRANSFER.FILES);

                    String action = extras.getString(Constants.TRANSFER.ACTION, Constants.TRANSFER.UNKNOWN);
                    String tAction = getTransferType(action);

                    if (tFiles != null) {
                        for (FileTransferHelper tFile : tFiles) {
                            Log.d(TAG, tAction + " - status=" + tFile.getStatus() + " skip=" + tFile.getSkip() + " Local>" + tFile.getLocalPath() + " Remote>" + tFile.getRemotePath());
                            long logId = extras.getLong(Constants.TRANSFER.LOG_ID);
                            if (logId > 0) {
                                addToLog(tFile, logId, "verbose");
                            }

                            if (action.equals(Constants.TRANSFER.CONFLICT)) {
                                // Start SyncSelectActivity, let user to select upload or download file
                                Intent i = new Intent(getBaseContext(), SyncSelectActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                i.putExtra(Constants.TRANSFER.ACTION, action);
                                i.putExtra(Constants.TRANSFER.FILE, tFile);
                                i.putExtra(Constants.TRANSFER.LOG_ID, logId);
                                getApplication().startActivity(i);
                            }
                        }
                    }
                }
            }
        }

        private String getTransferType(String action) {
            String tAction = getString(R.string.action_unknown);
            if (action.equals(Constants.TRANSFER.UPLOAD)) {
                tAction = getString(R.string.action_upload);
            } else if (action.equals(Constants.TRANSFER.DOWNLOAD)) {
                tAction = getString(R.string.action_download);
            } else if (action.equals(Constants.TRANSFER.DELETE_LOCAL)) {
                tAction = getString(R.string.action_delete_local);
            } else if (action.equals(Constants.TRANSFER.REMOVE_REMOTE)) {
                tAction = getString(R.string.action_remove_remote);
            } else if (action.equals(Constants.TRANSFER.CONFLICT)) {
                tAction = getString(R.string.action_conflict);
            } else if (action.equals(Constants.TRANSFER.SKIP)) {
                tAction = getString(R.string.action_skip);
            }
            return tAction;
        }
    };

    private void commandTransfer(ArrayList<FileTransferHelper> tFiles, String action) {
        if (tFiles == null || action == null || action.isEmpty()) {
            Log.d(TAG, "somethings wrong with commandTransfer");
        } else {
            PreferencesHelper preferencesHelper = new PreferencesHelper().getFtpsSettings(getApplicationContext());
            if (!preferencesHelper.isSet()) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                                .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.missing_ftps_settings)
                                .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false));
            } else {
                Intent i = new Intent(this, TransferService.class);
                i.putExtra(Constants.ACTION.TRANSFER, action);
                i.putParcelableArrayListExtra(Constants.TRANSFER.FILES, tFiles);
                startService(i);
            }
        }
    }

    private long addToLog(FileTransferHelper tFile, long logID, String type) {
        int status = tFile.getStatus() ? 1 : 0;
        status = tFile.getSkip() ? 2 : status;
        ContentValues values = new ContentValues();
        values.put(LogTable.COLUMN_LOCAL, tFile.getLocalPath());
        values.put(LogTable.COLUMN_REMOTE, tFile.getRemotePath());
        values.put(LogTable.COLUMN_ACTION, tFile.getAction());
        values.put(LogTable.COLUMN_DATE, new Date().getTime());
        values.put(LogTable.COLUMN_STATUS, status);
        values.put(LogTable.COLUMN_TYPE, type);

        Uri logUri = Uri.parse(LogProvider.CONTENT_URI + "/" + logID);
        int updated = getContentResolver().update(logUri, values, LogTable.COLUMN_LOCAL + " = ?", new String[] {tFile.getLocalPath()});
        if (updated > 0) {
            //Log.d(TAG, "Update Log " + logID + " (" + updated + "): " + tFile.getLocalPath());
        } else {
            logUri = getContentResolver().insert(LogProvider.CONTENT_URI, values);
            if (logUri != null) {
                try {
                    logID = Long.parseLong(logUri.getFragment());
                } catch (NumberFormatException e) {
                    logID = 0;
                }
            }
        }
        return logID;
    }

    private long addMessageToLog(long logID, String action) {
        if (action.contains("%")) return 0; // Ignore messages with percentage
        ContentValues values = new ContentValues();
        values.put(LogTable.COLUMN_LOCAL, "");
        values.put(LogTable.COLUMN_REMOTE, "");
        values.put(LogTable.COLUMN_ACTION, action);
        values.put(LogTable.COLUMN_DATE, new Date().getTime());
        values.put(LogTable.COLUMN_STATUS, true);
        values.put(LogTable.COLUMN_TYPE, "message");

        Uri logUri = Uri.parse(LogProvider.CONTENT_URI + "/" + logID);
        int updated = getContentResolver().update(logUri, values, LogTable.COLUMN_ACTION + " = ?", new String[] {action});
        if (updated > 0) {
            //Log.d(TAG, "Update Log " + logID + " (" + updated + "): " + action);
        } else {
            logUri = getContentResolver().insert(LogProvider.CONTENT_URI, values);
            if (logUri != null) {
                try {
                    logID = Long.parseLong(logUri.getFragment());
                } catch (NumberFormatException e) {
                    logID = 0;
                }
            }
        }
        return logID;
    }

}