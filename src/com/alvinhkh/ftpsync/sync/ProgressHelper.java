package com.alvinhkh.ftpsync.sync;

import android.os.Parcel;
import android.os.Parcelable;

public class ProgressHelper implements Parcelable {
    private String message = "";
    private Integer progress = 0;
    private Integer maxProgress = 100;
    private Boolean indeterminate = false;

    public ProgressHelper(String message, Integer progress)
    {
        this(message, progress, false);
    }
    public ProgressHelper(String message, Integer progress, Boolean indeterminate)
    {
        this(message, progress, indeterminate ? 0 : 100, indeterminate);
    }
    public ProgressHelper(String message, Integer progress, Integer maxProgress)
    {
        this(message, progress, maxProgress, false);
    }
    public ProgressHelper(String message, Integer progress, Integer maxProgress, Boolean indeterminate)
    {
        this.message = message;
        this.progress = progress;
        this.maxProgress = maxProgress;
        this.indeterminate = indeterminate;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void setProgress(Integer progress)
    {
        this.progress = progress;
    }

    public void setMaxProgress(Integer maxProgress)
    {
        this.maxProgress = maxProgress;
    }

    public void setIndeterminate(Boolean indeterminate) {
        this.indeterminate = indeterminate;
    }

    public String getMessage()
    {
        return message;
    }

    public Integer getProgress()
    {
        return progress;
    }

    public Integer getMaxProgress()
    {
        return maxProgress;
    }

    public Boolean getIndeterminate () {
        return indeterminate;
    }

    // Parcelling part
    public ProgressHelper(Parcel p) {
        this.message = p.readString();
        this.progress = p.readInt();
        this.maxProgress = p.readInt();
        this.indeterminate = p.readByte() == 1;
    }

    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeString(this.message);
        p.writeInt(this.progress);
        p.writeInt(this.maxProgress);
        p.writeByte((byte) (this.indeterminate ? 1 : 0));
    }
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ProgressHelper createFromParcel(Parcel in) {
            return new ProgressHelper(in);
        }

        public ProgressHelper[] newArray(int size) {
            return new ProgressHelper[size];
        }
    };
}
