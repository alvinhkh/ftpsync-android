package com.alvinhkh.ftpsync.sync;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.log.LogProvider;
import com.alvinhkh.ftpsync.log.LogTable;
import com.alvinhkh.ftpsync.net.FtpsUtils;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransferService extends IntentService {

    protected static final String TAG = "TransferService";

    public FtpsUtils ftps = null;
    protected PreferencesHelper ftpsSettings = new PreferencesHelper();
    protected List<FileTransferHelper> tFiles = new ArrayList<FileTransferHelper>();
    private volatile boolean isStopped = false;
    private volatile boolean isAskToStop = false;

    String mAction = "";

    public TransferService() {
        super("TransferService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isStopped = false;
        if (intent.getAction() != null && intent.getAction().equals(Constants.ACTION.STOP_SERVICE)) {
            Log.d(TAG, "Service asked to stop");
            isAskToStop = true;
            stopSelf();
        } else {
            isAskToStop = false;
        }
        Log.d(TAG, "Service Starting");
        ftpsSettings = new PreferencesHelper().getFtpsSettings(this);
        onStart(intent, startId);
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (isStopped == true) {
            // Check is service asked to stop
            cancelOperation();
            return;
        }
        if (intent == null) return;
        Bundle extras = intent.getExtras();
        if (extras == null) return;
        if (ftpsSettings == null) {
            Log.d(TAG, "Fail to retrieve preferences ");
            return;
        }

        mAction = intent.getStringExtra(Constants.ACTION.TRANSFER);
        if ( mAction.equals(Constants.TRANSFER.UNKNOWN) || mAction == null ) return;

        // Retrieve Log Id
        long logId = 0;
        if (extras != null) {
            logId = extras.getLong(Constants.TRANSFER.LOG_ID);
        }

        // Connect to FTPS
        if (ftps == null || !ftps.isConnected()) {
            ftps = new FtpsUtils(ftpsSettings);
            ftps.setup(this);
        }

        // Get Transfer Files
        tFiles = extras.getParcelableArrayList(Constants.TRANSFER.FILES);

        if (tFiles == null) {
            FileTransferHelper tFile = extras.getParcelable(Constants.TRANSFER.FILE);
            if (tFile == null) {
                return;
            }
            tFiles = new ArrayList<FileTransferHelper>();
            tFiles.add(tFile);
        }

        for (FileTransferHelper aFile : tFiles) {
            // Add to Log
            logId = addToLog(aFile, logId);
        }

        if (isStopped == true) {
            // Check is service asked to stop
            cancelOperation();
            return;
        }

        ArrayList<FileTransferHelper> cFiles = new ArrayList<FileTransferHelper>();

        if (mAction.equals(Constants.TRANSFER.UPLOAD)) {

            cFiles = (ArrayList<FileTransferHelper>) ftps.upload(tFiles);

        } else if (mAction.equals(Constants.TRANSFER.DOWNLOAD)) {

            cFiles = (ArrayList<FileTransferHelper>) ftps.download(tFiles);

        } else if (mAction.equals(Constants.TRANSFER.DELETE_LOCAL)) {

            cFiles = (ArrayList<FileTransferHelper>) ftps.deleteLocal(tFiles);

        } else if (mAction.equals(Constants.TRANSFER.REMOVE_REMOTE)) {

            cFiles = (ArrayList<FileTransferHelper>) ftps.remove(tFiles);

        } else if (mAction.equals(Constants.TRANSFER.CONFLICT)) {

            PreferencesHelper syncSettings = new PreferencesHelper().getSyncSettings(this);
            Integer syncConflictAction = syncSettings.getSyncConflictAction();
            // 0: Skip, 1: Local (Upload), 2: Remote (Download), 9: User Select
            if (syncConflictAction >= 3) {
                // User Select
                Log.d(TAG, "Sync Conflict: user select");
                for (FileTransferHelper tFile : tFiles) {
                    tFile.setAction(Constants.ACTION.CONFLICT);
                    tFile.setStatus(false);
                    tFile.setSkip(true);
                    cFiles.add(tFile);
                }
            } else if (syncConflictAction == 2) {
                // Remote (Download)
                Log.d(TAG, "Sync Conflict: default download");
                cFiles = (ArrayList<FileTransferHelper>) ftps.download(tFiles);
            } else if (syncConflictAction == 1) {
                // Local (Upload)
                Log.d(TAG, "Sync Conflict: default upload");
                cFiles = (ArrayList<FileTransferHelper>) ftps.upload(tFiles);
            } else {
                // Skip
                Log.d(TAG, "Sync Conflict: default skip");
                for (FileTransferHelper tFile : tFiles) {
                    tFile.setAction(Constants.ACTION.SKIP);
                    tFile.setStatus(true);
                    tFile.setSkip(true);
                    cFiles.add(tFile);
                }
            }

        } else if (mAction.equals(Constants.TRANSFER.SKIP)) {

            for (FileTransferHelper tFile : tFiles) {
                tFile.setAction(Constants.ACTION.SKIP);
                tFile.setStatus(true);
                tFile.setSkip(true);
                cFiles.add(tFile);
            }

        }

        for (FileTransferHelper cFile : cFiles) {
            // Add to Log
            logId = addToLog(cFile, logId);
        }
        return;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service Done");
        isStopped = true;
        if (isAskToStop != true) {
            // Send end progress
            Intent endIntent = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                    .putExtra(Constants.TRANSFER.PROGRESS_DONE, true)
                    .putExtra(Constants.TRANSFER.RETURN_STATUS, true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(endIntent);
        }
        super.onDestroy();
    }

    private void cancelOperation() {
        Log.d(TAG, "Cancel Operation");
        Toast.makeText(this, getString(R.string.cancel_operation), Toast.LENGTH_SHORT).show();
    }

    private long addToLog(FileTransferHelper tFile, long logID) {
        int status = tFile.getStatus() ? 1 : 0;
        status = tFile.getSkip() ? 2 : status;
        ContentValues values = new ContentValues();
        values.put(LogTable.COLUMN_LOCAL, tFile.getLocalPath());
        values.put(LogTable.COLUMN_REMOTE, tFile.getRemotePath());
        values.put(LogTable.COLUMN_ACTION, tFile.getAction());
        values.put(LogTable.COLUMN_DATE, new Date().getTime());
        values.put(LogTable.COLUMN_STATUS, status);
        values.put(LogTable.COLUMN_TYPE, "verbose");

        Uri logUri = Uri.parse(LogProvider.CONTENT_URI + "/" + logID);
        int updated = getContentResolver().update(logUri, values, LogTable.COLUMN_LOCAL + " = ?", new String[] {tFile.getLocalPath()});
        if (updated > 0) {
            //Log.d(TAG, "Update Log " + logID + " (" + updated + "): " + tFile.getLocalPath());
        } else {
            logUri = getContentResolver().insert(LogProvider.CONTENT_URI, values);
            if (logUri != null) {
                try {
                    logID = Long.parseLong(logUri.getFragment());
                } catch (NumberFormatException e) {
                    logID = 0;
                }
            }
        }

        ArrayList<FileTransferHelper> tFiles = new ArrayList<FileTransferHelper>();
        tFiles.add(tFile);
        // Send single progress
        Intent localIntent = new Intent(Constants.ACTION.SINGLE_TRANSFER);
        localIntent.putExtra(Constants.TRANSFER.ACTION, mAction);
        localIntent.putParcelableArrayListExtra(Constants.TRANSFER.FILES, tFiles);
        localIntent.putExtra(Constants.TRANSFER.LOG_ID, logID);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

        return logID;
    }


}
