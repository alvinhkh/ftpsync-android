package com.alvinhkh.ftpsync.sync;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.file.FileUtils;
import com.alvinhkh.ftpsync.file.VectorClock;
import com.alvinhkh.ftpsync.log.LogProvider;
import com.alvinhkh.ftpsync.log.LogTable;
import com.alvinhkh.ftpsync.net.FtpsUtils;
import com.alvinhkh.ftpsync.savedlist.LocalSaved;
import com.alvinhkh.ftpsync.savedlist.RemoteSaved;
import com.alvinhkh.ftpsync.savedlist.SavedListUtils;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class SyncService extends IntentService {

    protected static final String TAG = "SyncService";

    public FtpsUtils ftps = null;
    protected PreferencesHelper ftpsSettings = new PreferencesHelper();
    protected List<FileTransferHelper> tFiles = new ArrayList<FileTransferHelper>();
    private volatile boolean isStopped = false;
    private volatile boolean isAskToStop = false;

    volatile String localReplicaId = "046b6c7f-0b8a-43b9-b35d-6489e6daee91";
    volatile String serverReplicaId = "server";

    public SyncService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isStopped = false;
        if (intent.getAction() != null && intent.getAction().equals(Constants.ACTION.STOP_SERVICE)) {
            Log.d(TAG, "Service asked to stop");
            isAskToStop = true;
            stopSelf();
        } else {
            isAskToStop = false;
        }
        Log.d(TAG, "Service Starting");
        ftpsSettings = new PreferencesHelper().getFtpsSettings(this);
        if (!ftpsSettings.getLocalClientId().isEmpty()) {
            localReplicaId = ftpsSettings.getLocalClientId();
        }
        Log.d(TAG, "Client ID: " + localReplicaId);
        onStart(intent, startId);
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (isStopped == true) {
            // Check is service asked to stop
            cancelOperation();
            return;
        }
        if (intent == null) return;
        Bundle extras = intent.getExtras();
        if (extras == null) return;
        if (ftpsSettings == null) {
            Log.d(TAG, "Fail to retrieve preferences ");
            return;
        }

        String triggerIs = intent.getStringExtra(Constants.TRIGGER.IS);
        Boolean triggerByAlarm = triggerIs != null && triggerIs.equals(Constants.TRIGGER.ALARM);
        Boolean manual = triggerIs != null && triggerIs.equals(Constants.TRIGGER.MANUAL);

        // Connect to FTPS
        if (ftps == null || !ftps.isConnected()) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(
                    new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                            .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.connecting)
                            .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, true));
            ftps = new FtpsUtils(ftpsSettings);
            ftps.setup(this);

            if (!ftps.isConnected()) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                                .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.missing_ftps_settings)
                                .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, false));
                return;
            }
        }

        // Get Transfer Files
        tFiles = extras.getParcelableArrayList(Constants.TRANSFER.FILES);

        int count = 0;
        for (FileTransferHelper tFile : tFiles) {

            if (isStopped == true) {
                // Check is service asked to stop
                cancelOperation();
                return;
            }

            long logId = 0;

            //
            LocalBroadcastManager.getInstance(this)
                    .sendBroadcast(new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                            .putExtra(Constants.TRANSFER.PROGRESS,
                                    new ProgressHelper(getString(R.string.get_list), count, tFiles.size())));

            tFile.setAction(Constants.ACTION.SYNC);
            logId = addToLog(tFile, logId, "overview");

            String rootLocalPath = tFile.getLocalPath();
            String rootRemotePath = tFile.getRemotePath();

            // TODO: upload list after conflict solved
            // TODO: sync single file

            RemoteSaved remoteSaved = new RemoteSaved(ftps);
            String fileName = "";
            if (FileUtils.isFile(rootLocalPath)) {
                // If sync file only. Local path is a file, remote path is a directory
                fileName = FileUtils.getName(rootLocalPath);
                rootLocalPath = rootLocalPath.replaceFirst(Pattern.quote(fileName), "");
            }

            File rootLocalPathFile = new File(rootLocalPath);
            if (!ftps.hasAccess(rootRemotePath) ||
                    !(rootLocalPathFile.exists() && rootLocalPathFile.canRead() && rootLocalPathFile.canWrite())) {
                // Send end progress
                Intent localIntent = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.no_access_rights)
                        .putExtra(Constants.TRANSFER.RETURN_STATUS, false);
                LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
                break;
            }

            // Initiate local
            LocalSaved localSaved = new LocalSaved();
            localSaved.setLocalRoot(rootLocalPath);
            localSaved.setRemoteRoot(rootRemotePath);
            JSONArray savedLocalList = localSaved.readFile(fileName, rootLocalPath);
            JSONArray currentLocalList = localSaved.getCurrentList(fileName, rootLocalPath, localReplicaId);
            Log.d(TAG, "savedLocalList: " + (savedLocalList != null ? savedLocalList.toString() : ""));
            Log.d(TAG, "currentLocalList: " + (currentLocalList != null ? currentLocalList.toString() : ""));

            // Initiate remote
            Log.d(TAG, "rootLocalPath: " + rootLocalPath);
            Log.d(TAG, "rootRemotePath: " + rootRemotePath);
            remoteSaved.setLocalRoot(rootLocalPath);
            remoteSaved.setRemoteRoot(rootRemotePath);
            JSONArray localList = new JSONArray();
            JSONArray remoteList = new JSONArray();
            JSONArray savedRemoteList = remoteSaved.readFile(fileName, rootRemotePath);
            JSONArray savedRemoteLocalList = remoteSaved.readLocalFile(fileName, rootLocalPath);
            JSONArray currentRemoteList = remoteSaved.getCurrentList(fileName, rootRemotePath, serverReplicaId);
            Log.d(TAG, "savedRemoteList: " + (savedRemoteList != null ? savedRemoteList.toString() : ""));
            Log.d(TAG, "savedRemoteLocalList: " + (savedRemoteLocalList != null ? savedRemoteLocalList.toString() : ""));
            Log.d(TAG, "currentRemoteList: " + (currentRemoteList != null ? currentRemoteList.toString() : ""));

            // Generate latest local list
            if (savedLocalList != null && currentLocalList != null) {
                localList = localSaved.compare(savedLocalList, currentLocalList, localReplicaId, fileName, rootLocalPath);

                Log.d(TAG, "list after local comparison: " + (localList != null ? localList.toString() : ""));
            } else if (savedLocalList == null && currentLocalList != null) {
                Log.d(TAG, "no saved local list");
                localList = currentLocalList;
            } else if (savedLocalList == null && currentLocalList == null) {
                // New Local Directory?
                // TODO: Ask User
                localList = new JSONArray();
                Log.d(TAG, "Local Directory is empty?");
            } else {
                Log.d(TAG, "Something is wrong: local list");
                localList = null;
            }

            if (isStopped == true) {
                // Check is service asked to stop
                cancelOperation();
                return;
            }

            // Generate latest remote list
            if (savedRemoteLocalList != null && currentRemoteList != null) {
                // Compare remote directory list saved locally, i.e. remote list when last sync
                remoteList = remoteSaved.compare(savedRemoteLocalList, currentRemoteList, serverReplicaId, fileName, rootRemotePath);
                Log.d(TAG, "list after remote comparison: " + (remoteList != null ? remoteList.toString() : ""));
            } else if (savedRemoteLocalList == null && savedRemoteList != null && currentRemoteList != null) {
                // Compare remote directory list saved on server
                Log.d(TAG, "no saved remote list on local but on server");
                remoteList = remoteSaved.compare(savedRemoteList, currentRemoteList, serverReplicaId, fileName, rootRemotePath);
                Log.d(TAG, "list after remote comparison: " + (remoteList != null ? remoteList.toString() : ""));
            } else if (savedRemoteLocalList == null && savedRemoteList == null && currentRemoteList != null) {
                // Use current remote list
                Log.d(TAG, "no saved remote list both local and server");
                remoteList = currentRemoteList;
            } else if (savedRemoteLocalList == null && savedRemoteList == null && currentRemoteList == null) {
                // New Remote Directory?
                // TODO: Ask User
                remoteList = new JSONArray();
                Log.d(TAG, "Remote Directory is empty?");
            } else {
                Log.d(TAG, "Something is wrong: remote list");
                remoteList = null;
            }

            if (isStopped == true) {
                // Check is service asked to stop
                cancelOperation();
                return;
            }

            Boolean success;
            if (localList != null && remoteList != null && localList.isNull(0) && remoteList.isNull(0)) {

                success = true;
                // Send end progress
                Intent localIntent = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.empty_directories)
                        .putExtra(Constants.TRANSFER.RETURN_STATUS, success);
                LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

            } else if (localList != null && remoteList != null) {

                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                                .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.check_list)
                                .putExtra(Constants.TRANSFER.PROGRESS_INDETERMINATE, true));

                try {
                    success = true;
                    JSONArray newLocalList = new JSONArray();
                    JSONArray newServerList = new JSONArray();

                    Map<String, JSONObject> localMap = SavedListUtils.JSONArrayToHashMap(localList);
                    Map<String, JSONObject> serverMap = SavedListUtils.JSONArrayToHashMap(remoteList);
                    Map<String, JSONObject> localRemainMap = localMap;
                    Map<String, JSONObject> serverRemainMap = serverMap;

                    // Compare local list and server list
                    for (Map.Entry<String,JSONObject> cEntry : localMap.entrySet()) {
                        String localPath = cEntry.getKey();
                        JSONObject localObject = cEntry.getValue();
                        for (Map.Entry<String,JSONObject> sEntry : serverMap.entrySet()) {
                            String serverPath = sEntry.getKey();
                            JSONObject serverObject = sEntry.getValue();
                            if (!localObject.isNull(serverPath)) {
                                JSONObject localContent = localObject.getJSONObject(localPath);
                                JSONObject serverContent = serverObject.getJSONObject(serverPath);
                                Log.d(TAG, ">> local> " + localContent);
                                Log.d(TAG, ">> server> " + serverContent);

                                if (localContent.isNull("path")) continue;
                                String cPath = localContent.getString("path");
                                String cFullPath = localContent.getString("localpath");
                                cFullPath = FileUtils.replaceSlashes(cFullPath);
                                Long cMTime = localContent.getLong("mtime");
                                Integer cSize = localContent.getInt("size");
                                JSONObject localVersionObj = localContent.getJSONObject("version");
                                if (localVersionObj.isNull(localReplicaId)) {
                                    localVersionObj.put(localReplicaId, 0);
                                }
                                String sPath = serverContent.getString("path");
                                String sFullPath = serverContent.getString("remotepath");
                                sFullPath = FileUtils.replaceSlashes(sFullPath);
                                Long sMTime = serverContent.getLong("mtime");
                                Integer sSize = serverContent.getInt("size");
                                JSONObject serverVersionObj = serverContent.getJSONObject("version");
                                if (serverVersionObj.isNull(serverReplicaId)) {
                                    serverVersionObj.put(serverReplicaId, 0);
                                }

                                if (!cPath.equals(sPath)) {
                                    Log.d(TAG, "Something is wrong: path - " + cPath + " != " + sPath);
                                }

                                FileTransferHelper tf = new FileTransferHelper();
                                tf.setLocalAbsolutePath(cFullPath);
                                tf.setLocalTime(cMTime);
                                tf.setLocalSize(cSize);
                                tf.setRemoteAbsolutePath(sFullPath);
                                tf.setRemoteTime(sMTime);
                                tf.setRemoteSize(sSize);

                                VectorClock localClock = new VectorClock();
                                VectorClock localClock2 = new VectorClock();
                                Iterator<?> localVersion = localVersionObj.keys();
                                while( localVersion.hasNext() ){
                                    String key = (String) localVersion.next();
                                    if ( localVersionObj.get(key) instanceof Integer ){
                                        Integer version = localVersionObj.getInt(key);
                                        localClock.put(key, version);
                                        if (key.equals(localReplicaId) || key.equals(serverReplicaId))
                                            localClock2.put(key, version);
                                    }
                                }
                                VectorClock serverClock = new VectorClock();
                                VectorClock serverClock2 = new VectorClock();
                                Iterator<?> serverVersion = serverVersionObj.keys();
                                while( serverVersion.hasNext() ){
                                    String key = (String) serverVersion.next();
                                    if ( serverVersionObj.get(key) instanceof Integer ){
                                        Integer version = serverVersionObj.getInt(key);
                                        serverClock.put(key, version);
                                        if (key.equals(localReplicaId) || key.equals(serverReplicaId))
                                            serverClock2.put(key, version);
                                    }
                                }
                                Log.d(TAG, ">>> local clock: " + localClock.toString());
                                Log.d(TAG, ">>> server clock: " + serverClock.toString());
                                VectorClock clockMax = VectorClock.max(localClock, serverClock);
                                Log.d(TAG, ">>> clock max: " + clockMax.toString());

                                Log.d(TAG, ">>> local clock 2: " + localClock2.toString());
                                Log.d(TAG, ">>> server clock 2: " + serverClock2.toString());
                                VectorClock.Comparison clockCompare = VectorClock.compare(localClock2, serverClock2);
                                if (clockCompare.equals(VectorClock.Comparison.EQUAL)) {
                                    Log.d(TAG, "vc compare: EQUAL , " + tf.getLocalPath());
                                    if (!cSize.equals(sSize) || !cMTime.equals(sMTime)) {
                                        commandTransfer(tf, Constants.TRANSFER.CONFLICT);
                                    } else {
                                        commandTransfer(tf, Constants.TRANSFER.SKIP);
                                    }
                                } else if (clockCompare.equals(VectorClock.Comparison.GREATER)) {
                                    Log.d(TAG, "vc compare: GREATER , upload / remove remote " + tf.getRemotePath());
                                    if (cMTime < 0 && cSize < 0) {
                                        commandTransfer(tf, Constants.TRANSFER.REMOVE_REMOTE);
                                    } else {
                                        commandTransfer(tf, Constants.TRANSFER.UPLOAD);
                                    }
                                } else if (clockCompare.equals(VectorClock.Comparison.SMALLER)) {
                                    Log.d(TAG, "vc compare: SMALLER , download / delete local " + tf.getLocalPath());
                                    if (sMTime < 0 && sSize < 0) {
                                        commandTransfer(tf, Constants.TRANSFER.DELETE_LOCAL);
                                    } else {
                                        commandTransfer(tf, Constants.TRANSFER.DOWNLOAD);
                                    }
                                } else {
                                    Log.d(TAG, "vc compare: SIMULTANEOUS, conflict " + tf.getLocalPath());
                                    if (cMTime < 0 && cSize < 0) {
                                        commandTransfer(tf, Constants.TRANSFER.REMOVE_REMOTE);
                                    }
                                    if (sMTime < 0 && sSize < 0) {
                                        commandTransfer(tf, Constants.TRANSFER.DELETE_LOCAL);
                                    }
                                    if (cMTime >= 0 && cSize >= 0 && sMTime >= 0 && sSize >= 0) {
                                        commandTransfer(tf, Constants.TRANSFER.CONFLICT);
                                    }
                                }

                                JSONObject versionNewObj = new JSONObject();
                                String[] cIDs = clockMax.getOrderedIDs();
                                Integer[] cValues = clockMax.getOrderedValues();
                                for (int k = 0; k < cValues.length; k++) {
                                    versionNewObj.put(cIDs[k], cValues[k]);
                                }
                                Log.d(TAG, ">>> new version object: " + versionNewObj);

                                localContent.put("version", versionNewObj);
                                serverContent.put("version", versionNewObj);
                                localObject.put(localPath, localContent);
                                serverObject.put(serverPath, serverContent);

                                newLocalList.put(localObject);
                                newServerList.put(serverObject);

                                // Remove by comparing each local with server
                                localRemainMap.remove(localPath);
                                serverRemainMap.remove(serverPath);
                            }
                        }
                    }
                    for (Map.Entry<String,JSONObject> sEntry : serverMap.entrySet()) {
                        String serverPath = sEntry.getKey();
                        JSONObject serverObject = sEntry.getValue();
                        for (Map.Entry<String,JSONObject> cEntry : localMap.entrySet()) {
                            String localPath = cEntry.getKey();
                            JSONObject localObject = cEntry.getValue();
                            if (!serverObject.isNull(localPath)) {
                                // Remove by comparing each server with local
                                serverRemainMap.remove(serverPath);
                                localRemainMap.remove(localPath);
                            }
                        }
                    }
                    for (Map.Entry<String,JSONObject> sEntry : serverRemainMap.entrySet()) {
                        // remaining, server have, local nope, download
                        String serverPath = sEntry.getKey();
                        JSONObject serverObject = sEntry.getValue();

                        Log.d(TAG, ">>> new to local after last sync> " + serverObject);

                        JSONObject localObject = new JSONObject();
                        JSONObject localContent = new JSONObject();
                        JSONObject serverContent = serverObject.getJSONObject(serverPath);

                        if (serverContent.isNull("path")) continue;
                        String sPath = serverContent.getString("path");
                        String sFullPath = serverContent.getString("remotepath");
                        sFullPath = FileUtils.replaceSlashes(sFullPath);
                        Long sMTime = serverContent.getLong("mtime");
                        Integer sSize = serverContent.getInt("size");
                        JSONObject serverVersionObj = serverContent.getJSONObject("version");
                        if (serverVersionObj.isNull(serverReplicaId)) {
                            serverVersionObj.put(serverReplicaId, 0);
                        }

                        String cFullPath = rootLocalPath + File.separator + sPath;
                        cFullPath = FileUtils.replaceSlashes(cFullPath);
                        localContent.put("path", sPath);
                        localContent.put("localpath", cFullPath);
                        localContent.put("mtime", sMTime);
                        localContent.put("size", sSize);
                        JSONObject localVersionObj = new JSONObject();
                        localVersionObj.put(localReplicaId, 0);

                        FileTransferHelper tf = new FileTransferHelper();
                        tf.setLocalAbsolutePath(cFullPath);
                        tf.setLocalTime(sMTime);
                        tf.setLocalSize(sSize);
                        tf.setRemoteAbsolutePath(sFullPath);
                        tf.setRemoteTime(sMTime);
                        tf.setRemoteSize(sSize);
                        if (sMTime < 0 && sSize < 0) {
                            commandTransfer(tf, Constants.TRANSFER.DELETE_LOCAL);
                        } else {
                            commandTransfer(tf, Constants.TRANSFER.DOWNLOAD);
                        }

                        VectorClock localClock = new VectorClock();
                        Iterator<?> localVersion = localVersionObj.keys();
                        while( localVersion.hasNext() ){
                            String key = (String) localVersion.next();
                            if ( localVersionObj.get(key) instanceof Integer ){
                                Integer version = localVersionObj.getInt(key);
                                localClock.put(key, version);
                            }
                        }
                        VectorClock serverClock = new VectorClock();
                        Iterator<?> serverVersion = serverVersionObj.keys();
                        while( serverVersion.hasNext() ){
                            String key = (String) serverVersion.next();
                            if ( serverVersionObj.get(key) instanceof Integer ){
                                Integer version = serverVersionObj.getInt(key);
                                serverClock.put(key, version);
                            }
                        }
                        Log.d(TAG, ">>> local clock: " + localClock.toString());
                        Log.d(TAG, ">>> server clock: " + serverClock.toString());
                        VectorClock clockMax = VectorClock.max(localClock, serverClock);
                        Log.d(TAG, ">>> clock max: " + clockMax.toString());

                        JSONObject versionNewObj = new JSONObject();
                        String[] cIDs = clockMax.getOrderedIDs();
                        Integer[] cValues = clockMax.getOrderedValues();
                        for (int k = 0; k < cValues.length; k++) {
                            versionNewObj.put(cIDs[k], cValues[k]);
                        }
                        Log.d(TAG, ">>> new version object: " + versionNewObj);

                        localContent.put("version", versionNewObj);
                        serverContent.put("version", versionNewObj);
                        localObject.put(serverPath, localContent);
                        serverObject.put(serverPath, serverContent);

                        newLocalList.put(localObject);
                        newServerList.put(serverObject);

                    }
                    for (Map.Entry<String,JSONObject> cEntry : localRemainMap.entrySet()) {
                        // remaining, local have, server nope, upload
                        String localPath = cEntry.getKey();
                        JSONObject localObject = cEntry.getValue();

                        Log.d(TAG, ">>> new to server after last sync> " + localObject);

                        JSONObject serverObject = new JSONObject();
                        JSONObject serverContent = new JSONObject();
                        JSONObject localContent = localObject.getJSONObject(localPath);

                        String cPath = localContent.getString("path");
                        String cFullPath = localContent.getString("localpath");
                        cFullPath = FileUtils.replaceSlashes(cFullPath);
                        Long cMTime = localContent.getLong("mtime");
                        Integer cSize = localContent.getInt("size");
                        JSONObject localVersionObj = localContent.getJSONObject("version");
                        if (localVersionObj.isNull(localReplicaId)) {
                            localVersionObj.put(localReplicaId, 0);
                        }

                        String sFullPath = rootRemotePath + "/" + cPath;
                        sFullPath = FileUtils.replaceSlashes(sFullPath);
                        serverContent.put("path", cPath);
                        serverContent.put("remotepath", sFullPath);
                        serverContent.put("mtime", cMTime);
                        serverContent.put("size", cSize);
                        JSONObject serverVersionObj = new JSONObject();
                        serverVersionObj.put(serverReplicaId, 0);

                        FileTransferHelper tf = new FileTransferHelper();
                        tf.setLocalAbsolutePath(cFullPath);
                        tf.setLocalTime(cMTime);
                        tf.setLocalSize(cSize);
                        tf.setRemoteAbsolutePath(sFullPath);
                        tf.setRemoteTime(cMTime);
                        tf.setRemoteSize(cSize);
                        if (cMTime < 0 && cSize < 0) {
                            commandTransfer(tf, Constants.TRANSFER.REMOVE_REMOTE);
                        } else {
                            commandTransfer(tf, Constants.TRANSFER.UPLOAD);
                        }

                        VectorClock localClock = new VectorClock();
                        Iterator<?> localVersion = localVersionObj.keys();
                        while( localVersion.hasNext() ){
                            String key = (String) localVersion.next();
                            if ( localVersionObj.get(key) instanceof Integer ){
                                Integer version = localVersionObj.getInt(key);
                                localClock.put(key, version);
                            }
                        }
                        VectorClock serverClock = new VectorClock();
                        Iterator<?> serverVersion = serverVersionObj.keys();
                        while( serverVersion.hasNext() ){
                            String key = (String) serverVersion.next();
                            if ( serverVersionObj.get(key) instanceof Integer ){
                                Integer version = serverVersionObj.getInt(key);
                                serverClock.put(key, version);
                            }
                        }
                        Log.d(TAG, ">>> local clock: " + localClock.toString());
                        Log.d(TAG, ">>> server clock: " + serverClock.toString());
                        VectorClock clockMax = VectorClock.max(localClock, serverClock);
                        Log.d(TAG, ">>> clock max: " + clockMax.toString());

                        JSONObject versionNewObj = new JSONObject();
                        String[] cIDs = clockMax.getOrderedIDs();
                        Integer[] cValues = clockMax.getOrderedValues();
                        for (int k = 0; k < cValues.length; k++) {
                            versionNewObj.put(cIDs[k], cValues[k]);
                        }
                        Log.d(TAG, ">>> new version object: " + versionNewObj);

                        localContent.put("version", versionNewObj);
                        serverContent.put("version", versionNewObj);
                        localObject.put(localPath, localContent);
                        serverObject.put(localPath, serverContent);

                        newLocalList.put(localObject);
                        newServerList.put(serverObject);
                    }

                    Log.d(TAG, "new list: local " + newLocalList.toString());
                    Log.d(TAG, "new list: remote " + newServerList.toString());
                    if (newLocalList != null && !newLocalList.isNull(0)) {
                        localSaved.writeFile(newLocalList, fileName, rootLocalPath);
                    }
                    if (newServerList != null && !newServerList.isNull(0)) {
                        remoteSaved.writeFile(newServerList, fileName, rootRemotePath);
                        remoteSaved.writeLocalFile(newServerList, fileName, rootLocalPath);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    success = false;
                }

                // Send end progress
                Intent localIntent = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.RETURN_STATUS, success);
                LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
            } else {
                Log.e(TAG, "Something is wrong: unable to compare");
                success = false;
                // Send end progress
                Intent localIntent = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                        .putExtra(Constants.TRANSFER.MESSAGE_ID, R.string.fail_to_get_directories)
                        .putExtra(Constants.TRANSFER.RETURN_STATUS, success);
                LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
            }

            // Show Updated
            /*
            savedLocalList = localSaved.readFile(fileName, rootLocalPath);
            savedRemoteList = remoteSaved.readFile(fileName, rootRemotePath);
            savedRemoteLocalList = remoteSaved.readLocalFile(fileName, rootLocalPath);
            Log.d(TAG, "saved local updated: " + (savedLocalList != null ? savedLocalList.toString() : "null"));
            Log.d(TAG, "saved remote updated: " + (savedRemoteList != null ? savedRemoteList.toString() : "null"));
            Log.d(TAG, "saved remote in local updated: " + (savedRemoteLocalList != null ? savedRemoteLocalList.toString() : "null"));
            */

            /*
            // Sync by using timestamp comparison
            List<FileListHelper> localList = FileUtils.getFileList(rootLocalPath);
            List<FileListHelper> remoteList = ftps.getFileList(rootRemotePath);

            for (FileListHelper remote : remoteList) {
                Boolean localFound = false;
                Date remoteFileTime = remote.getLastModified();
                FileTransferHelper tf = new FileTransferHelper();
                tf.setRemoteAbsolutePath(rootRemotePath + remote.getPath());
                tf.setRemoteTime(remoteFileTime.getTime());
                Integer countLocal = 0;
                for (FileListHelper local : localList) {
                    if (local.getPath().equals(remote.getPath())) {
                        // Local has the same file path
                        localFound = true;
                        Date localFileTime = local.getLastModified();
                        if (local.getIsFile()) {
                            tf.setLocalAbsolutePath(rootLocalPath);
                        } else {
                            tf.setLocalAbsolutePath(rootLocalPath + local.getPath());
                        }
                        tf.setLocalTime(localFileTime.getTime());
                        if (localFileTime.equals(remoteFileTime)) {
                            // Same Last Modified
                            // Same File Skip
                            tSkip.add(tf);
                        } else if (localFileTime.after(remoteFileTime)) {
                            // local newer
                            tUpload.add(tf);
                        } else if (localFileTime.before(remoteFileTime)) {
                            // remote newer
                            tDownload.add(tf);
                        } else {
                            Log.d(TAG, "local>" + localFileTime + " remote>" + remoteFileTime);
                        }
                    }
                    if (localFound == false && countLocal >= localList.size() - 1) {
                        // Local does not have the same file path, download
                        if (local.getIsFile()) {
                            tf.setLocalAbsolutePath(rootLocalPath);
                        } else {
                            tf.setLocalAbsolutePath(rootLocalPath + remote.getPath());
                            tDownload.add(tf);
                        }
                        //Log.d(TAG, "local>" + tf.getLocalAbsolutePath() + " remote>" + tf.getRemoteAbsolutePath() + " " + localFound);
                    }
                    countLocal++;
                }
            }

            for (FileListHelper local : localList) {
                Boolean remoteFound = false;
                FileTransferHelper tf = new FileTransferHelper();
                if (local.getIsFile()) {
                    tf.setLocalAbsolutePath(rootLocalPath);
                } else {
                    tf.setLocalAbsolutePath(rootLocalPath + local.getPath());
                }
                tf.setLocalTime(local.getLastModified().getTime());
                Integer countRemote = 0;
                for (FileListHelper remote : remoteList) {
                    if (remote.getPath().equals(local.getPath())) {
                        // Remote has the same file path, done above
                        remoteFound = true;
                        tf.setRemoteAbsolutePath(rootRemotePath + remote.getPath());
                        tf.setRemoteTime(remote.getLastModified().getTime());
                    }
                    if (remoteFound == false && countRemote >= remoteList.size() - 1) {
                        // Remote does not have the same file path, upload
                        Log.d(TAG, remote.getIsDirectory().toString());
                        tf.setRemoteAbsolutePath(rootRemotePath + local.getPath());
                        tUpload.add(tf);
                        //Log.d(TAG, "local>" + tf.getLocalAbsolutePath() + " remote>" + tf.getRemoteAbsolutePath() + " " + remoteFound);
                    }
                    countRemote++;
                }
            }*/

            count++;

            tFile.setStatus(true);
            logId = addToLog(tFile, logId, "overview");
        }
        return;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service Done");
        isStopped = true;
        if (isAskToStop != true) {
            // Send end progress
            Intent localIntent = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                    .putExtra(Constants.TRANSFER.PROGRESS_DONE, true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }
        super.onDestroy();
    }

    private void commandTransfer(FileTransferHelper tFile, String action) {
        ArrayList<FileTransferHelper> tFiles = new ArrayList<FileTransferHelper>();
        tFiles.add(tFile);
        commandTransfer(tFiles, action);
    }

    private void commandTransfer(ArrayList<FileTransferHelper> tFiles, String action) {
        if (isStopped == true) {
            // Check is service asked to stop
            cancelOperation();
            return;
        }
        if (tFiles == null || action == null || action.isEmpty()) {
            Log.d(TAG, "somethings wrong with commandTransfer");
        } else {
            Intent i = new Intent(this, TransferService.class);
            i.putExtra(Constants.ACTION.TRANSFER, action);
            i.putParcelableArrayListExtra(Constants.TRANSFER.FILES, tFiles);
            startService(i);
        }
    }

    private void cancelOperation() {
        Log.d(TAG, "Cancel Operation");
        Toast.makeText(this, getString(R.string.cancel_operation), Toast.LENGTH_SHORT).show();
    }

    private long addToLog(FileTransferHelper tFile, long logID, String type) {
        int status = tFile.getStatus() ? 1 : 0;
        status = tFile.getSkip() ? 2 : status;
        ContentValues values = new ContentValues();
        values.put(LogTable.COLUMN_LOCAL, tFile.getLocalPath());
        values.put(LogTable.COLUMN_REMOTE, tFile.getRemotePath());
        values.put(LogTable.COLUMN_ACTION, tFile.getAction());
        values.put(LogTable.COLUMN_DATE, new Date().getTime());
        values.put(LogTable.COLUMN_STATUS, status);
        values.put(LogTable.COLUMN_TYPE, type);

        Uri logUri = Uri.parse(LogProvider.CONTENT_URI + "/" + logID);
        int updated = getContentResolver().update(logUri, values, LogTable.COLUMN_LOCAL + " = ?", new String[] {tFile.getLocalPath()});
        if (updated > 0) {
            //Log.d(TAG, "Update Log " + logID + " (" + updated + "): " + tFile.getLocalPath());
        } else {
            logUri = getContentResolver().insert(LogProvider.CONTENT_URI, values);
            if (logUri != null) {
                try {
                    logID = Long.parseLong(logUri.getFragment());
                } catch (NumberFormatException e) {
                    logID = 0;
                }
            }
        }
        return logID;
    }

}