package com.alvinhkh.ftpsync.sync;

import android.os.Parcel;
import android.os.Parcelable;

import com.alvinhkh.ftpsync.file.FileUtils;

import java.io.File;

public class FileTransferHelper implements Parcelable
{
    private String localFileName = "";
    private String localDirectory = "";
    private String localAbsolutePath = "";
    
    private String remoteFileName = "";
    private String remoteDirectory = "";
    private String remoteAbsolutePath = "";

    private Boolean isDirectoryOnly = false;
    private Boolean status = false;
    private Boolean skip = false;
    private Boolean fileNotFound = false;
    private Boolean connectionClosed = false;

    private String action = "";
    private Long localTime = 0L;
    private Long remoteTime = 0L;
    private Integer localSize = 0;
    private Integer remoteSize = 0;

    public FileTransferHelper()
    {
    }
    /**
     * Constructs a FileTransferHelper from values
     */
    public FileTransferHelper(String localDirectory, String remoteDirectory, Boolean isDirectoryOnly)
    {
        this.localDirectory = localDirectory;
        this.remoteDirectory = remoteDirectory;
        this.isDirectoryOnly = isDirectoryOnly;
    }
    /**
     * Constructs a FileTransferHelper from values
     */
    public FileTransferHelper(String localDirectory, String remoteDirectory, String localFileName, String remoteFileName)
    {
        this.localFileName = localFileName;
        this.localDirectory = localDirectory;
        this.remoteFileName = remoteFileName;
        this.remoteDirectory = remoteDirectory;
    }

    public String getLocalFileName()
    {
        if (localFileName.equals("") && !getLocalPath().equals("")) {
            return new File(getLocalPath()).getName();
        }
        return localFileName;
    }

    public String getLocalDirectory()
    {
        return localDirectory;
    }

    public String getRemoteFileName()
    {
        if (remoteFileName.equals("") && !getRemotePath().equals("")) {
            return new File(getRemotePath()).getName();
        }
        return remoteFileName;
    }

    public String getRemoteDirectory()
    {
        return remoteDirectory;
    }

    public String getLocalFilePath()
    {
        return localDirectory + File.separator + localFileName;
    }

    public String getRemoteFilePath()
    {
        return remoteDirectory + "/" + remoteFileName;
    }

    public String getLocalAbsolutePath()
    {
        return localAbsolutePath;
    }

    public String getRemoteAbsolutePath()
    {
        return remoteAbsolutePath;
    }

    public String getLocalPath() {
        if (!getLocalAbsolutePath().equals(""))
        {
            return FileUtils.replaceSlashes(getLocalAbsolutePath());
        }
        return FileUtils.replaceSlashes(getLocalFilePath());
    }

    public String getRemotePath() {
        if (!getRemoteAbsolutePath().equals(""))
        {
            return FileUtils.replaceSlashes(getRemoteAbsolutePath());
        }
        return FileUtils.replaceSlashes(getRemoteFilePath());
    }

    public void setLocalFileName(String localFileName)
    {
        this.localFileName = localFileName;
    }

    public void setLocalDirectory(String localDirectory)
    {
        this.localDirectory = localDirectory;
    }

    public void setRemoteFileName(String remoteFileName)
    {
        this.remoteFileName = remoteFileName;
    }

    public void setRemoteDirectory(String remoteDirectory)
    {
        this.remoteDirectory = remoteDirectory;
    }

    public void setLocalAbsolutePath(String localAbsolutePath)
    {
        this.localAbsolutePath = localAbsolutePath;
    }

    public void setRemoteAbsolutePath(String remoteAbsolutePath)
    {
        this.remoteAbsolutePath = remoteAbsolutePath;
    }

    public Boolean getStatus()
    {
        return status;
    }

    public Boolean getSkip()
    {
        return skip;
    }

    public Boolean getFileNotFound()
    {
        return fileNotFound;
    }

    public Boolean getConnectionClosed()
    {
        return connectionClosed;
    }

    public void setStatus(Boolean status)
    {
        this.status = status;
    }

    public void setSkip(Boolean skip)
    {
        this.skip = skip;
    }

    public void setFileNotFound(Boolean fileNotFound)
    {
        this.fileNotFound = fileNotFound;
    }

    public void setConnectionClosed(Boolean connectionClosed)
    {
        this.connectionClosed = connectionClosed;
    }

    public Boolean getIsDirectoryOnly()
    {
        return isDirectoryOnly;
    }

    public void setIsDirectoryOnly(Boolean isDirectoryOnly)
    {
        this.isDirectoryOnly = isDirectoryOnly;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getLocalTime() {
        return localTime;
    }

    public void setLocalTime(Long localTime) {
        this.localTime = localTime;
    }

    public Long getRemoteTime() {
        return remoteTime;
    }

    public void setRemoteTime(Long remoteTime) {
        this.remoteTime = remoteTime;
    }

    public Integer getLocalSize() {
        return localSize;
    }

    public void setLocalSize(Integer localSize) {
        this.localSize = localSize;
    }

    public Integer getRemoteSize() {
        return remoteSize;
    }

    public void setRemoteSize(Integer remoteSize) {
        this.remoteSize = remoteSize;
    }

    // Parcelling
    /**
     * Constructs a FileTransferHelper from a Parcel
     * @param p Source Parcel
     */
    public FileTransferHelper(Parcel p) {
        this.localAbsolutePath = p.readString();
        this.remoteAbsolutePath = p.readString();
        this.localDirectory = p.readString();
        this.remoteDirectory = p.readString();
        this.localFileName = p.readString();
        this.remoteFileName = p.readString();

        this.isDirectoryOnly = p.readByte() == 1;
        this.status = p.readByte() == 1;
        this.skip = p.readByte() == 1;
        this.fileNotFound = p.readByte() == 1;
        this.connectionClosed = p.readByte() == 1;

        this.action = p.readString();
        this.localTime = p.readLong();
        this.remoteTime = p.readLong();
        this.localSize = p.readInt();
        this.remoteSize = p.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    // Required method to write to Parcel
    @Override
    public void writeToParcel(Parcel p, int flags) {
        p.writeString(this.localAbsolutePath);
        p.writeString(this.remoteAbsolutePath);
        p.writeString(this.localDirectory);
        p.writeString(this.remoteDirectory);
        p.writeString(this.localFileName);
        p.writeString(this.remoteFileName);

        p.writeByte((byte) (this.isDirectoryOnly ? 1 : 0));
        p.writeByte((byte) (this.status ? 1 : 0));
        p.writeByte((byte) (this.skip ? 1 : 0));
        p.writeByte((byte) (this.fileNotFound ? 1 : 0));
        p.writeByte((byte) (this.connectionClosed ? 1 : 0));

        p.writeString(this.action);
        p.writeLong(this.localTime);
        p.writeLong(this.remoteTime);
        p.writeInt(this.localSize);
        p.writeInt(this.remoteSize);
    }
    // Method to recreate a FileTransferHelper from a Parcel
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public FileTransferHelper createFromParcel(Parcel in) {
            return new FileTransferHelper(in);
        }

        public FileTransferHelper[] newArray(int size) {
            return new FileTransferHelper[size];
        }
    };
}
