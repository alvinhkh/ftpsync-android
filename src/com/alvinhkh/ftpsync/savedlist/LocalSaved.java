package com.alvinhkh.ftpsync.savedlist;

import android.util.Log;

import com.alvinhkh.ftpsync.file.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.PatternSyntaxException;

public class LocalSaved extends SavedListUtils implements SavedList {

    private String TAG = "LocalSavedList";

    public LocalSaved() {

    }

    @Override
    public JSONArray readFile(String filePath, String rootPath)
    {
        return readLocalFile(filePath, rootPath);
    }

    @Override
    public Boolean writeFile(JSONArray array, final String filePath, final String rootPath)
    {
        return writeLocalFile(array, filePath, rootPath);
    }

    @Override
    public JSONArray getCurrentList(String fileName, String rootPath, final String replicaId)
    {
        String fullPath = FileUtils.replaceSlashes(fileName.equals("") ? rootPath : rootPath + File.separator + fileName);
        File rootPathFile = new File(fullPath);
        JSONArray currentList = new JSONArray();

        JSONObject previousState = new JSONObject();
        JSONArray previousList = new JSONArray();
        try {
            File savedFile = getLocalFile(fullPath);
            if (savedFile != null) {
                if (savedFile.exists() && savedFile.isFile()) {
                    String file = FileUtils.read(savedFile.getAbsolutePath());
                    previousState = new JSONObject(file);
                }

                if (previousState.isNull("list") == false) {
                    previousList = previousState.getJSONArray("list");
                }

                try {
                    List<File> files = new ArrayList<File>();
                    if (rootPathFile.exists() && rootPathFile.isFile()) {
                        files.add(rootPathFile);
                    } else {
                        files = FileUtils.listFiles(fullPath, null, 0);
                    }
                    if (files == null) return null;
                    for (File file : files) {
                        if (file.getName().startsWith(".") || file.getName().startsWith("~") || file.getName().contains(".ftpsync")) {
                            if (file.getName().startsWith(".clocksync")) {
                                Log.d(TAG, "delete .clocksync in local directory");
                                new File(file.getPath()).delete();
                            }
                            continue;
                        }
                        String filePath = FileUtils.getShortPath(file.getPath(), rootPath);

                        JSONObject fileVersion = new JSONObject();
                        if (!previousList.isNull(0)) {
                            for (int i = 0; i < previousList.length(); i++) {
                                JSONObject previousObject = previousList.getJSONObject(i);
                                if (!previousObject.isNull(filePath)) {
                                    //Log.d(TAG, "previous: " + previousObject.getJSONObject(filePath));
                                    JSONObject fileObject = previousObject.getJSONObject(filePath);
                                    if (fileObject.isNull("version") == false) {
                                        fileVersion = fileObject.getJSONObject("version");
                                    } else {
                                        fileVersion = new JSONObject();
                                    }
                                    //Log.d(TAG, "previous version: " + fileVersion);
                                }
                            }
                        }
                        if (fileVersion.isNull(replicaId)) {
                            fileVersion.put(replicaId, 0);
                        }

                        JSONObject currentObject = new JSONObject();
                        JSONObject currentFileContent = new JSONObject();
                        currentFileContent.put("path", filePath);
                        currentFileContent.put("localpath", FileUtils.replaceSlashes(file.getPath()));
                        currentFileContent.put("mtime", new Date(file.lastModified()).getTime());
                        currentFileContent.put("size", new File(file.getPath()).length());
                        currentFileContent.put("version", fileVersion);
                        currentObject.put(filePath, currentFileContent);
                        currentList.put(currentObject);
                        //Log.d(TAG, "current: " + currentFileContent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (PatternSyntaxException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return currentList;
    }

    @Override
    public JSONObject getFileObject(String fileName, String rootPath, String replicaId) throws JSONException {
        JSONObject object = null;
        String fullPath = FileUtils.replaceSlashes(fileName.equals("") ? rootPath : rootPath + File.separator + fileName);
        File file = new File(fullPath);
        if (file.exists()) {
            object = new JSONObject();
            JSONObject content = new JSONObject();
            content.put("path", FileUtils.getShortPath(fileName, rootPath));
            content.put("localpath", FileUtils.replaceSlashes(file.getPath()));
            content.put("mtime", new Date(file.lastModified()).getTime());
            content.put("size", new File(file.getPath()).length());
            JSONObject fileVersion = new JSONObject();
            fileVersion.put(replicaId, 0);
            content.put("version", fileVersion);
            object.put(FileUtils.getShortPath(fileName, rootPath), content);
        }
        return object;
    }

}
