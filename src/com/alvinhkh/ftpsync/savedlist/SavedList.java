package com.alvinhkh.ftpsync.savedlist;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface SavedList {

    JSONArray readFile(String filePath, String rootPath);

    Boolean writeFile(JSONArray array, final String filePath, final String rootPath);

    JSONArray getCurrentList(String fileName, String rootPath, final String replicaId);

    JSONArray compare(JSONArray savedArray, JSONArray currentArray, final String replicaId, final String fileName, final String rootPath);

    JSONObject incrementVersion(JSONObject fileObject, final String replicaId);

    JSONArray edit(String fileName, String filePath, JSONObject fileObject, JSONArray oldList, final String rootPath);

    JSONArray add(String fileName, String filePath, JSONObject fileObject, JSONArray oldList, final String rootPath);

    JSONArray markAsDelete(String fileName, String filePath, JSONArray oldList, final String rootPath, final String replicaId);

    JSONObject getFileObject(String fullPath, String rootPath, String replicaId) throws JSONException;

}
