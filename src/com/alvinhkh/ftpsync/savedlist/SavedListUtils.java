package com.alvinhkh.ftpsync.savedlist;

import android.util.Log;

import com.alvinhkh.ftpsync.file.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SavedListUtils {

    private static String TAG = "SavedList";

    private String localRoot = "";

    private String remoteRoot = "";

    public String getLocalRoot() {
        return localRoot;
    }

    public void setLocalRoot(String localRoot) {
        this.localRoot = localRoot;
    }

    public String getRemoteRoot() {
        return remoteRoot;
    }

    public void setRemoteRoot(String remoteRoot) {
        this.remoteRoot = remoteRoot;
    }


    public JSONArray readFile(String filePath, String rootPath) {
        Log.d(TAG, "!!!readFile");
        return null;
    }

    public Boolean writeFile(JSONArray array, final String filePath, final String rootPath) {
        Log.d(TAG, "!!!writeFile");
        return false;
    }

    public JSONObject getFileObject(String filePath, String rootPath, String replicaId) throws JSONException {
        Log.d(TAG, "!!!getFileObject");
        return null;
    }

    protected String getLocalFileName() {
        return ".ftpsync";
    }

    public JSONArray compare(JSONArray savedArray, JSONArray currentArray, final String replicaId, final String fileName, final String rootPath) {
        JSONArray currentList = new JSONArray();
        try {
            currentList = savedArray == null ? new JSONArray() : new JSONArray(savedArray.toString());
            Map<String, JSONObject> previousMap = SavedListUtils.JSONArrayToHashMap(savedArray);
            Map<String, JSONObject> currentMap = SavedListUtils.JSONArrayToHashMap(currentArray);
            Map<String, JSONObject> previousRemainMap = previousMap;
            Map<String, JSONObject> currentRemainMap = currentMap;

            // Compare previous local list and current local list
            for (Map.Entry<String,JSONObject> pEntry : previousMap.entrySet()) {
                String previousPath = pEntry.getKey();
                JSONObject previousObject = pEntry.getValue();
                for (Map.Entry<String,JSONObject> cEntry : currentMap.entrySet()) {
                    String currentPath = cEntry.getKey();
                    JSONObject currentObject = cEntry.getValue();
                    if (!previousObject.isNull(currentPath)) {
                        JSONObject previousContent = previousObject.getJSONObject(previousPath);
                        JSONObject currentContent = currentObject.getJSONObject(currentPath);
                        Log.d(TAG, ">> p> " + previousContent);
                        Log.d(TAG, ">> c> " + currentContent);

                        String pPath = previousContent.getString("path");
                        String pFullPath = "";
                        String pFullPathIs = "";
                        if (!previousContent.isNull("localpath")) {
                            pFullPath = previousContent.getString("localpath");
                            pFullPathIs = "localpath";
                        } else if (!previousContent.isNull("remotepath")) {
                            pFullPath = previousContent.getString("remotepath");
                            pFullPathIs = "remotepath";
                        }
                        Long pMTime = previousContent.getLong("mtime");
                        Integer pSize = previousContent.getInt("size");
                        String cPath = currentContent.getString("path");
                        String cFullPath = "";
                        String cFullPathIs = "";
                        if (!currentContent.isNull("localpath")) {
                            cFullPath = currentContent.getString("localpath");
                            cFullPathIs = "localpath";
                        } else if (!currentContent.isNull("remotepath")) {
                            cFullPath = currentContent.getString("remotepath");
                            cFullPathIs = "remotepath";
                        }
                        Long cMTime = currentContent.getLong("mtime");
                        Integer cSize = currentContent.getInt("size");
                        JSONObject fileVersion = currentContent.getJSONObject("version");
                        if (fileVersion.isNull(replicaId)) {
                            fileVersion.put(replicaId, 0);
                        }

                        Boolean equalPath = pPath.equals(cPath);
                        Boolean equalFullPath = pFullPath.equals(cFullPath);
                        Boolean equalMTime = pMTime.equals(cMTime);
                        Boolean equalSize = pSize.equals(cSize);

                        if (!equalPath) {
                            Log.d(TAG, "Something is wrong: path - " + pPath + " != " + cPath);
                        }
                        if (!equalFullPath) {
                            Log.d(TAG, "Something is wrong: full path - " + pFullPath + " != " + cFullPath);
                            if (FileUtils.replaceSlashes(pFullPath).equals(FileUtils.replaceSlashes(cFullPath))) {
                                pFullPath = FileUtils.replaceSlashes(pFullPath);
                                cFullPath = FileUtils.replaceSlashes(cFullPath);
                                Log.d(TAG, "Full path is correct after removing double slash - " + pFullPath + " == " + cFullPath);
                                previousContent.put(pFullPathIs, pFullPath);
                                currentContent.put(cFullPathIs, cFullPath);
                                JSONArray list = edit(fileName, cPath, currentContent, currentList, rootPath);
                                if (list != null) currentList = list;
                            }
                        }

                        if (!equalMTime || !equalSize) {
                            Log.d(TAG, "mismatch: last modification time or file size");
                            Log.d(TAG, "previous time: " + pMTime);
                            Log.d(TAG, "current time: " + cMTime);
                            Log.d(TAG, "previous size: " + pSize);
                            Log.d(TAG, "current size: " + cSize);
                            // Edit and Version +1
                            fileVersion = incrementVersion(currentContent, replicaId);
                            currentContent.put("version", fileVersion);
                            JSONArray list = edit(fileName, cPath, currentContent, currentList, rootPath);
                            if (list != null) currentList = list;
                        }

                        // Remove by comparing each previous with current
                        currentRemainMap.remove(currentPath);
                        previousRemainMap.remove(previousPath);
                    }
                }
            }
            for (Map.Entry<String,JSONObject> cEntry : currentMap.entrySet()) {
                String currentPath = cEntry.getKey();
                JSONObject currentObject = cEntry.getValue();
                for (Map.Entry<String,JSONObject> pEntry : previousMap.entrySet()) {
                    String previousPath = pEntry.getKey();
                    JSONObject previousObject = pEntry.getValue();
                    if (!currentObject.isNull(previousPath)) {
                        // Remove by comparing each current with previous
                        currentRemainMap.remove(currentPath);
                        previousRemainMap.remove(previousPath);
                    }
                }
            }
            for (Map.Entry<String,JSONObject> entry : currentRemainMap.entrySet()) {
                // remaining, current have, previous nope, newly added
                JSONObject object = entry.getValue();
                if (!object.isNull(entry.getKey())) {
                    JSONObject content = object.getJSONObject(entry.getKey());
                    if (!content.isNull("path")) {
                        String path = content.getString("path");
                        Log.d(TAG, ">>> newly added after last sync> " + object);
                        JSONArray list = add(fileName, path, content, currentList, rootPath);
                        if (list != null) currentList = list;
                    }
                }
            }
            for (Map.Entry<String,JSONObject> entry : previousRemainMap.entrySet()) {
                // remaining, previous have, currently nope, deleted
                JSONObject object = entry.getValue();
                if (!object.isNull(entry.getKey())) {
                    JSONObject content = object.getJSONObject(entry.getKey());
                    if (!content.isNull("path")) {
                        String path = content.getString("path");
                        Log.d(TAG, ">>> deleted after last sync> " + entry.getValue());
                        JSONArray list = markAsDelete(fileName, path, currentList, rootPath, replicaId);
                        if (list != null) currentList = list;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return currentList;
    }

    public JSONObject incrementVersion(JSONObject fileObject, final String replicaId)
    {
        JSONObject fileVersion = new JSONObject();
        try {
            if (fileObject.isNull("version") == false) {
                fileVersion = fileObject.getJSONObject("version");
            } else {
                fileVersion = new JSONObject();
            }
            if (fileVersion.isNull(replicaId) == false) {
                fileVersion.put(replicaId, fileVersion.getInt(replicaId) + 1);
            } else {
                fileVersion.put(replicaId, 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return fileVersion;
    }

    public JSONArray edit(String fileName, String filePath, JSONObject fileObject, JSONArray oldList, final String rootPath)
    {
        if (oldList == null) oldList = new JSONArray();
        JSONArray newList = null;
        try {
            Boolean fileFound = false;
            newList = new JSONArray();
            String shortPath = FileUtils.getShortPath(filePath, rootPath);
            for (int i = 0; i < oldList.length(); i++) {
                if (!(oldList.get(i) instanceof JSONObject)) continue;
                JSONObject currentObject = oldList.getJSONObject(i);
                if (!currentObject.isNull(shortPath)) {
                    fileFound = true;
                    if (fileObject == null) {
                        newList.put(oldList.getJSONObject(i));
                    } else {
                        currentObject.put(shortPath, fileObject);
                        newList.put(currentObject);
                    }
                } else {
                    newList.put(oldList.getJSONObject(i));
                }
            }
            if (fileFound) {
            } else {
                Log.d(TAG, "File not found: " + filePath + " root: " + rootPath);
            }
            Log.d(TAG, "list after edit: " + newList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newList;
    }

    public JSONArray add(String fileName, String filePath, JSONObject fileObject, JSONArray oldList, final String rootPath)
    {
        if (oldList == null) oldList = new JSONArray();
        JSONArray newList = null;
        try {
            Boolean fileFound = false;
            newList = new JSONArray(oldList.toString());

            if (fileObject != null) {
                fileFound = true;
                JSONObject currentObject = new JSONObject();
                String shortPath = FileUtils.getShortPath(filePath, rootPath);
                currentObject.put(shortPath, fileObject);
                newList.put(currentObject);
            }

            if (fileFound) {
            } else {
                Log.d(TAG, "File not found: " + fileName + " root: " + rootPath);
            }
            Log.d(TAG, "list after add: " + newList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newList;
    }

    public JSONArray markAsDelete(String fileName, String filePath, JSONArray oldList, final String rootPath, final String replicaId)
    {
        if (oldList == null) oldList = new JSONArray();
        JSONArray newList = null;
        try {
            Boolean fileFound = false;
            newList = new JSONArray();
            String path = FileUtils.getShortPath(filePath, rootPath);
            for (int i = 0; i < oldList.length(); i++) {
                if (!(oldList.get(i) instanceof JSONObject)) continue;
                JSONObject currentObject = oldList.getJSONObject(i);
                if (!currentObject.isNull(path)) {
                    fileFound = true;
                    JSONObject fileObject = currentObject.getJSONObject(path);
                    if (fileObject.isNull("mtime") == false && fileObject.getInt("mtime") < 0
                            && fileObject.isNull("size") == false && fileObject.getInt("size") < 0) {
                        // marked before
                        Log.d(TAG, "Marked as -1 before. " + fileObject);
                    } else {
                        // Version +1
                        JSONObject fileVersion = incrementVersion(fileObject, replicaId);
                        // mark as delete
                        fileObject.put("mtime", -1);
                        fileObject.put("size", -1);
                        fileObject.put("version", fileVersion);
                        currentObject.put(path, fileObject);
                    }
                }
                newList.put(currentObject);
            }
            if (fileFound) {
            } else {
                Log.d(TAG, "File not found: " + filePath + " root: " + rootPath);
            }
            Log.d(TAG, "list after mark delete: " + newList.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return newList;
    }

    public JSONArray readLocalFile(String fileName, String rootPath)
    {
        JSONObject jsonObject = new JSONObject();
        JSONArray savedList = null;

        try {
            File savedFile = getLocalFile(rootPath);
            if (savedFile != null) {
                if (savedFile.exists() && savedFile.isFile()) {
                    String file = FileUtils.read(savedFile.getAbsolutePath());
                    jsonObject = new JSONObject(file);
                    String root = "";
                    if (jsonObject.isNull("root") == false) {
                        root = jsonObject.getString("root");
                    }
                    if (root.equals("")) {
                        root = rootPath;
                    }
                    if (jsonObject.isNull("list") == false) {
                        savedList = jsonObject.getJSONArray("list");
                    }
                    if (root.equals(rootPath)) {
                        //Log.d(TAG, "root: " + root);
                        //Log.d(TAG, "list: " + savedList.toString());
                    } else {
                        //savedList = new JSONArray();
                        Log.d(TAG, ".ftpsync root path mismatch  json:" + root + " rootPath:" + rootPath);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return savedList;
    }

    public Boolean writeLocalFile(JSONArray array, final String fileName, final String rootPath)
    {
        if (array == null) return null;
        JSONObject currentFile = new JSONObject();

        try {
            File file = getLocalFile(rootPath);
            if (file != null) {
                String fullPath = fileName.equals("") ? rootPath : rootPath + File.separator + fileName;
                currentFile.put("root", rootPath);
                currentFile.put("list", array);
                FileUtils.write(file, currentFile.toString());
                Log.d(TAG, "Write Local: " + file);
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public File getLocalFile(String rootPath) throws IOException {
        String filename = getLocalFileName();
        String directory = FileUtils.getDirectory(rootPath);
        File file = null;
        if (directory != null) {
            // Delete if it is not a file
            file = new File(directory + File.separator + filename);
            if (file.exists() && !file.isFile()) {
                file.delete();
            }
            //
            file = new File(directory + File.separator + filename);
        }
        return file;
    }

    public static Map<String, JSONObject> JSONArrayToHashMap(JSONArray array) throws JSONException {
        Map<String, JSONObject> returnMap = new ConcurrentHashMap<>();
        for (int i = 0; i < array.length(); i++) {
            if (!(array.get(i) instanceof  JSONObject)) continue;
            JSONObject object = array.getJSONObject(i);
            Iterator<?> keys = object.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (object.get(key) instanceof JSONObject) {
                    returnMap.put(key, object);
                }
            }
        }
        return returnMap;
    }

}
