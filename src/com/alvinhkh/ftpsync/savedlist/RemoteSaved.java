package com.alvinhkh.ftpsync.savedlist;

import android.util.Log;

import com.alvinhkh.ftpsync.file.FileUtils;
import com.alvinhkh.ftpsync.net.FTPFileHelper;
import com.alvinhkh.ftpsync.net.FtpsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.regex.PatternSyntaxException;

public class RemoteSaved extends SavedListUtils implements SavedList {

    private String TAG = "RemoteSavedList";

    private FtpsUtils ftps;

    public RemoteSaved(FtpsUtils ftps) {
        this.ftps = ftps;
    }

    @Override
    public String getLocalFileName() {
        return ".ftpsync-remote";
    }

    @Override
    public JSONArray readFile(String filePath, String rootPath) {
        return readRemoteFile(filePath, rootPath);
    }

    public JSONArray readRemoteFile(String filePath, String rootPath) {
        if (!isConnected()) return null;
        JSONObject jsonObject = new JSONObject();
        JSONArray savedList = null;
        try {
            String savedFile = null;
            String savedFilePath = getFilePath(filePath, rootPath);
            if (savedFilePath != null) {
                savedFile = ftps.readFile(savedFilePath);
            }
            if (savedFile != null) {
                jsonObject = new JSONObject(savedFile);
                String root = "";
                if (jsonObject.isNull("root") == false) {
                    root = jsonObject.getString("root");
                }
                if (jsonObject.isNull("list") == false) {
                    savedList = jsonObject.getJSONArray("list");
                }
                if (root.equals(rootPath)) {
                    //Log.d(TAG, "root: " + root);
                    //Log.d(TAG, "list: " + savedList.toString());
                } else {
                    //savedList = new JSONArray();
                    Log.d(TAG, ".ftpsync root path mismatch");
                }
                Log.d(TAG, "Read json: " + root + " rootPath: " + rootPath);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savedList;
    }

    @Override
    public Boolean writeFile(JSONArray array, final String filePath, final String rootPath) {
        return writeRemoteFile(array, filePath, rootPath);
        //return writeLocalFile(array, getLocalRoot());
    }

    public Boolean writeRemoteFile(JSONArray array, final String filePath, final String rootPath) {
        if (array == null) return null;
        if (!isConnected()) return null;
        JSONObject currentFile = new JSONObject();

        try {
            String file = getFilePath(filePath, rootPath);
            if (file != null) {
                currentFile.put("root", rootPath);
                currentFile.put("list", array);

                Boolean done = ftps.writeFile(file, currentFile.toString());
                Log.d(TAG, "Write Remote: (" + done + ") " + file);
                return done;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Boolean isConnected() {
        if (ftps == null) return null;
        Boolean connected = false;
        try {
            ftps.connect();
            connected = true;
        } catch (IOException e) {
            connected = false;
            e.printStackTrace();
        }
        return connected;
    }

    /**
     * Get a list of file in a given directory in form of JSONArray
     * @return List of files/directories
     */
    @Override
    public JSONArray getCurrentList(String fileName, String rootPath, final String replicaId)
    {
        JSONArray currentList = null;
        try {
            if (!isConnected()) return null;

            JSONArray previousList = readRemoteFile(fileName, rootPath);

            String fullPath = FileUtils.replaceSlashes(fileName.equals("") ? rootPath : rootPath + "/" + fileName);

            List<FTPFileHelper> files = ftps.getFTPFileList(fullPath);
            if (files == null) return null;
            currentList = new JSONArray();
            for (FTPFileHelper file : files) {
                String filePath = FileUtils.getShortPath(file.getPath(), rootPath);

                String name = new File(filePath).getName();
                String[] tokens = name.split("\\.(?=[^\\.]+$)");
                //(tokens.length > 1 && tokens[1].startsWith(".backup"))
                if (name.startsWith(".") || name.startsWith("~") || name.contains(".ftpsync")
                        || (tokens.length > 1 && (tokens[1].startsWith(".") || tokens[1].startsWith("~")) )) {
                    continue;
                }

                JSONObject fileVersion = new JSONObject();

                if (previousList != null && !previousList.isNull(0)) {
                    for (int i = 0; i < previousList.length(); i++) {
                        if (!(previousList.get(i) instanceof JSONObject)) continue;
                        JSONObject previousObject = previousList.getJSONObject(i);
                        if (!previousObject.isNull(filePath)) {
                            Log.d(TAG, "previous: " + previousObject.getJSONObject(filePath));
                            JSONObject fileObject = previousObject.getJSONObject(filePath);
                            if (fileObject.isNull("version") == false) {
                                fileVersion = fileObject.getJSONObject("version");
                            } else {
                                fileVersion = new JSONObject();
                            }
                            //Log.d(TAG, "previous version: " + fileVersion);
                        }
                    }
                }
                if (fileVersion.isNull(replicaId)) {
                    fileVersion.put(replicaId, 0);
                }

                JSONObject currentObject = new JSONObject();
                JSONObject currentFileContent = new JSONObject();
                currentFileContent.put("path", filePath);
                currentFileContent.put("remotepath", FileUtils.replaceSlashes(file.getPath()));
                currentFileContent.put("mtime", file.lastModified().getTime());
                currentFileContent.put("size", file.getSize());
                currentFileContent.put("version", fileVersion);
                currentObject.put(filePath, currentFileContent);
                currentList.put(currentObject);
                Log.d(TAG, "current: " + currentFileContent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PatternSyntaxException e) {
            e.printStackTrace();
        }
        return currentList;
    }

    @Override
    public JSONObject getFileObject(String fileName, String rootPath, String replicaId) throws JSONException {
        JSONObject object = null;
        FTPFileHelper file = null;
        String fullPath = FileUtils.replaceSlashes(fileName.equals("") ? rootPath : rootPath + "/" + fileName);
        file = ftps.getFTPFile(fullPath);
        if (file != null) {
            object = new JSONObject();
            JSONObject content = new JSONObject();
            content.put("path", FileUtils.getShortPath(fileName, rootPath));
            content.put("remotepath", FileUtils.replaceSlashes(file.getPath()));
            Date lastModified = file.lastModified();
            content.put("mtime", lastModified == null ? 0 : file.lastModified().getTime());
            content.put("size", new File(file.getPath()).length());
            JSONObject fileVersion = new JSONObject();
            fileVersion.put(replicaId, 0);
            content.put("version", fileVersion);
            object.put(FileUtils.getShortPath(fileName, rootPath), content);
        }
        return object;
    }

    protected String getDirectory(String filePath, String rootPath) {
        if (filePath.equals(rootPath)) {
            return new File(rootPath).getParent();
        } else {
            return rootPath;
        }
    }

    public String getFilePath(String filePath, String rootPath) throws IOException {
        if (!isConnected()) return null;
        String path = null;
        String filename = ".ftpsync";
        String directory = getDirectory(filePath, rootPath);
        //Log.d(TAG, "Directory: " + (directory == null ? "null" : directory) + " filePath: " + filePath + " rootPath: " + rootPath);
        if (directory != null) {
            path = directory + File.separator + filename;
            /*
            // Delete if it is not a file
            if (ftps.isExist(path) == true && !ftps.isFile(path)) {
                ftps.deleteFile(path);
            }*/
        }
        return path;
    }

}
