package com.alvinhkh.ftpsync.filepicker;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.alvinhkh.ftpsync.net.FTPFileHelper;
import com.nononsenseapps.filepicker.AbstractFilePickerActivity;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;

public class FtpsFilePickerActivity extends AbstractFilePickerActivity<FTPFileHelper> {

    private ProgressDialog pDialog;

    public interface ACTION {
        String DIALOG = "com.alvinhkh.ftpsync.filepicker.DIALOG";
        String START = "com.alvinhkh.ftpsync.filepicker.START";
        String DONE = "com.alvinhkh.ftpsync.filepicker.DONE";
        String MESSAGE = "com.alvinhkh.ftpsync.filepicker.MESSAGE";
        String TOAST = "com.alvinhkh.ftpsync.filepicker.TOAST";
    }

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);

        pDialog = new ProgressDialog(this);

        init();
        Intent intent = new Intent(ACTION.DIALOG).putExtra(ACTION.START, true);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void init() {
        LocalBroadcastManager.getInstance(this).registerReceiver(progressReceiver, new IntentFilter(ACTION.DIALOG));
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(progressReceiver);
        // Removes the loading dialog
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    protected AbstractFilePickerFragment<FTPFileHelper> getFragment(
            final String startPath, final int mode, final boolean allowMultiple,
            final boolean allowCreateDir) {
        AbstractFilePickerFragment fragment = new FtpsFilePickerFragment();
        fragment.setArgs(startPath, mode, allowMultiple, allowCreateDir);
        return fragment;
    }

    private BroadcastReceiver progressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION.DIALOG)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    if (bundle.getBoolean(ACTION.START, false) == true) {
                        if (pDialog != null && pDialog.isShowing() == false) {
                            pDialog.setCancelable(false);
                            pDialog.show();
                        }
                    } else if (bundle.getBoolean(ACTION.DONE, false) == true) {
                        // Removes the loading dialog
                        if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                    }
                    if (!bundle.getString(ACTION.MESSAGE, "").equals("")) {
                        if (pDialog != null) {
                            pDialog.setMessage(bundle.getString(ACTION.MESSAGE));
                        }
                    }
                    if (!bundle.getString(ACTION.TOAST, "").equals("")) {
                        Toast.makeText(context, bundle.getString(ACTION.TOAST, ""),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    };

}