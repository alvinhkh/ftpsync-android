package com.alvinhkh.ftpsync.filepicker;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.util.SortedList;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.util.Log;

import com.alvinhkh.ftpsync.net.FTPFileHelper;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.net.FtpsUtils;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.File;
import java.io.IOException;

public class FtpsFilePickerFragment extends AbstractFilePickerFragment<FTPFileHelper>{

    private static String TAG = "FtpsFilePickerFragment";
    private FolderCreator folderCreator;

    /**
     * Name is validated to be non-null, non-empty and not containing any
     * slashes.
     *
     * @param name The name of the folder the user wishes to create.
     */
    @Override
    public void onNewFolder(final String name) {
        File folder = new File(currentPath.getPath(), name);

        if (folderCreator == null) {
            folderCreator = new FolderCreator();
        }

        folderCreator.execute(folder.getPath());
    }

    /**
     * Return true if the path is a directory and not a file.
     *
     * @param path
     */
    @Override
    public boolean isDir(final FTPFileHelper path) {
        FTPFile pathFile = path.getFTPFile();
        return pathFile.isDirectory();
    }

    /**
     * Return the path to the parent directory. Should return the root if
     * from is root.
     *
     * @param from
     */
    @Override
    public FTPFileHelper getParent(final FTPFileHelper from) {
        FTPFileHelper parent = new FTPFileHelper();
        parent.setPath(new File(from.getPath()).getParent());
        if (parent == null) parent = from;
        return parent;
    }

    /**
     * Convert the path to the type used.
     *
     * @param path
     */
    @Override
    public FTPFileHelper getPath(final String path) {
        FTPFileHelper file = new FTPFileHelper();
        file.setPath(path);
        return file;
    }

    /**
     * @param path
     * @return the full path to the file
     */
    @Override
    public String getFullPath(final FTPFileHelper path) {
        return new File(path.getPath()).getPath();
    }

    /**
     * @param path
     * @return the name of this file/folder
     */
    @Override
    public String getName(final FTPFileHelper path) {
        FTPFile pathFile = path.getFTPFile();
        return pathFile.getName();
    }

    /**
     * Get the root path.
     */
    @Override
    public FTPFileHelper getRoot() {
        return getPath("/");
    }

    /**
     * Convert the path to a URI for the return intent
     * @param file
     * @return
     */
    @Override
    public Uri toUri(final FTPFileHelper file) {
        Boolean isDirectory = false;
        if (file.getFTPFile() != null) isDirectory = file.getFTPFile().isDirectory();
        return Uri.parse(new File(file.getPath()).getPath())
                .buildUpon()
                .appendQueryParameter("isDirectory", isDirectory.toString())
                .build();
    }

    /**
     * Get a loader that lists the Files in the current path
     */
    @Override
    public Loader<SortedList<FTPFileHelper>> getLoader() {
        return new AsyncTaskLoader<SortedList<FTPFileHelper>>(getActivity()) {

            FTPSClient mFtpsClient = new FTPSClient(false);
            FtpsUtils ftpsUtils = null;

            @Override
            public SortedList<FTPFileHelper> loadInBackground() {

                try {
                    if (ftpsUtils == null) {
                        PreferencesHelper ftpsSettings = new PreferencesHelper().getFtpsSettings(getActivity());
                        ftpsUtils = new FtpsUtils(ftpsSettings);
                    }
                    ftpsUtils.connect();
                    mFtpsClient = ftpsUtils.getClient();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                SortedList<FTPFileHelper> files = new SortedList<FTPFileHelper>(FTPFileHelper.class, new SortedListAdapterCallback<FTPFileHelper>(getDummyAdapter()) {
                    @Override
                    public int compare(FTPFileHelper lhs, FTPFileHelper rhs) {
                        FTPFile lhsFile = lhs.getFTPFile();
                        FTPFile rhsFile = rhs.getFTPFile();
                        if (lhsFile.isDirectory() && !rhsFile.isDirectory()) {
                            return -1;
                        } else if (rhsFile.isDirectory() && !lhsFile.isDirectory()) {
                            return 1;
                        } else {
                            return lhsFile.getName().toLowerCase().compareTo(rhsFile.getName()
                                    .toLowerCase());
                        }
                    }

                    @Override
                    public boolean areContentsTheSame(FTPFileHelper lhs, FTPFileHelper rhs) {
                        FTPFile lhsFile = lhs.getFTPFile();
                        FTPFile rhsFile = rhs.getFTPFile();
                        return lhsFile.getRawListing().equals(rhsFile.getRawListing()) && (lhsFile.isFile() == rhsFile.isFile());
                    }

                    @Override
                    public boolean areItemsTheSame(FTPFileHelper file, FTPFileHelper file2) {
                        return areContentsTheSame(file, file2);
                    }
                }, 0);

                try
                {
                    String remotePath = currentPath.getPath();
                    FTPFile mlistFile = mFtpsClient.mlistFile(remotePath);
                    if (mlistFile == null || !mlistFile.isDirectory())
                    {
                        // handle if directory does not exist. Fall back to root.
                        currentPath = getRoot();
                        remotePath = currentPath.getPath();
                    }
                    FTPFile[] fs = mFtpsClient.listFiles(remotePath);

                    Integer reply = mFtpsClient.getReplyCode();
                    if (!FTPReply.isPositiveCompletion(reply)) {
                        // No valid authentication
                        Log.d(TAG, "No valid authentication");
                        //Toast.makeText(getActivity(), "Fail to get list of remote directories", Toast.LENGTH_SHORT);
                        getActivity().finish();
                    }

                    files.beginBatchedUpdates();

                    for (FTPFile f : fs) {
                        if (f.getName().equals(".") || f.getName().equals("..")) {
                            continue;
                        }

                        if (!f.hasPermission(FTPFile.USER_ACCESS, FTPFile.READ_PERMISSION) ||
                                !f.hasPermission(FTPFile.USER_ACCESS, FTPFile.WRITE_PERMISSION)) {
                            // Skip if user dont have permission to read or write to that file or directory
                            Log.d(TAG, "No valid permission: " + f.getName().toString());
                            continue;
                        }

                        if ((mode == MODE_FILE || mode == MODE_FILE_AND_DIR) || f.isDirectory()) {
                            FTPFileHelper newFile = new FTPFileHelper();
                            String newFilePath = new File(currentPath.getPath() + "/" + f.getName()).getPath();
                            newFile.setPath(newFilePath);
                            newFile.setFTPFile(f);
                            newFile.setParent(currentPath);
                            files.add(newFile);
                        }
                    }
                    files.endBatchedUpdates();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                        .putExtra(FtpsFilePickerActivity.ACTION.DONE, true);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                return files;
            }

            /**
             * Handles a request to start the Loader.
             */
            @Override
            protected void onStartLoading() {
                super.onStartLoading();

                Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                        .putExtra(FtpsFilePickerActivity.ACTION.START, true)
                        .putExtra(FtpsFilePickerActivity.ACTION.MESSAGE, "Loading...");
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                if (currentPath == null || currentPath.getPath() == null) {
                    currentPath = getRoot();
                }

                forceLoad();
            }

            /**
             * Handles a request to completely reset the Loader.
             */
            @Override
            protected void onReset() {
                Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                        .putExtra(FtpsFilePickerActivity.ACTION.DONE, true);
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                super.onReset();
            }

        };
    }

    private class FolderCreator extends AsyncTask<String, Void, String> {

        private FTPSClient mFtpsClient = null;
        private FtpsUtils ftpsUtils = null;

        public FolderCreator() {
            mFtpsClient = new FTPSClient(false);
        }

        @Override
        protected void onPreExecute() {
            Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                    .putExtra(FtpsFilePickerActivity.ACTION.START, true)
                    .putExtra(FtpsFilePickerActivity.ACTION.MESSAGE, "Creating...");
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
        }

        @Override
        protected String doInBackground(final String... paths) {
            try {
                PreferencesHelper ftpsSettings = new PreferencesHelper().getFtpsSettings(getActivity());
                ftpsUtils = new FtpsUtils(ftpsSettings);

                ftpsUtils.connect();
                mFtpsClient = ftpsUtils.getClient();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!FTPReply.isPositiveCompletion(mFtpsClient.getReplyCode())) {
                // No valid authentication
                Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                        .putExtra(FtpsFilePickerActivity.ACTION.TOAST, "Unable to create new folder");
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                return null;
            }
            for (String path : paths) {
                try {
                    mFtpsClient.makeDirectory(path);
                    currentPath = new FTPFileHelper();
                    currentPath.setPath(path);
                } catch (IOException e) {
                    Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                            .putExtra(FtpsFilePickerActivity.ACTION.TOAST, "Unable to create new folder");
                    LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                    .putExtra(FtpsFilePickerActivity.ACTION.DONE, true);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            refresh();
        }

        @Override
        protected void onCancelled() {
            Intent intent = new Intent(FtpsFilePickerActivity.ACTION.DIALOG)
                    .putExtra(FtpsFilePickerActivity.ACTION.DONE, true);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
            super.onCancelled();
        }
    }

}