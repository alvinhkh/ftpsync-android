package com.alvinhkh.ftpsync.view.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.sync.FileTransferHelper;
import com.alvinhkh.ftpsync.sync.TransferService;

import java.util.ArrayList;
import java.util.Date;

/*
 * SyncSelectActivity allows to select local or remote when sync conflict
 */
public class SyncSelectActivity extends Activity implements OnClickListener {

    private static final String TAG = "SyncSelectActivity";

    private Context mContext;
    private Button mLocalButton;
    private TextView mLocalPath;
    private TextView mLocalFileDate;
    private TextView mLocalFileSize;
    private Button mRemoteButton;
    private TextView mRemotePath;
    private TextView mRemoteFileDate;
    private TextView mRemoteFileSize;
    private Button mCancelButton;

    private FileTransferHelper tFile;
    private long logId;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.dialog_select);
        setTitle(getString(R.string.title_select));
        // side dialog width and height
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // prevent cancel when touch outside
        this.setFinishOnTouchOutside(false);
        // get context
        mContext = SyncSelectActivity.this;
        // get widgets
        mLocalButton = (Button) findViewById(R.id.button_local);
        mLocalPath = (TextView) findViewById(R.id.text_localPath);
        mLocalFileDate = (TextView) findViewById(R.id.text_localFileDate);
        mLocalFileSize = (TextView) findViewById(R.id.text_localFileSize);
        mRemoteButton = (Button) findViewById(R.id.button_remote);
        mRemotePath = (TextView) findViewById(R.id.text_remotePath);
        mRemoteFileDate = (TextView) findViewById(R.id.text_remoteFileDate);
        mRemoteFileSize = (TextView) findViewById(R.id.text_remoteFileSize);
        mCancelButton = (Button) findViewById(R.id.button_cancel);
        // set listeners
        mLocalButton.setOnClickListener(this);
        mRemoteButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        // check from the saved Instance
        tFile = (bundle == null) ? null : (FileTransferHelper) bundle.getParcelable(Constants.TRANSFER.FILE);
        // Or passed from the other activity
        if (extras != null) {
            logId = extras.getLong(Constants.TRANSFER.LOG_ID);
            tFile = extras.getParcelable(Constants.TRANSFER.FILE);
            fillData(tFile);

            String action = extras.getString(Constants.TRANSFER.ACTION, Constants.TRANSFER.UNKNOWN);
            if (action.equals(Constants.TRANSFER.CONFLICT)) {

            } else {

            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_cancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.button_local:
                if (tFile.getLocalSize() == -1 && tFile.getLocalTime() == -1) {
                    // Delete Local
                    updateAction(Constants.TRANSFER.REMOVE_REMOTE, tFile);
                } else {
                    // Upload
                    updateAction(Constants.TRANSFER.UPLOAD, tFile);
                }
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.button_remote:
                if (tFile.getRemoteSize() == -1 && tFile.getRemoteTime() == -1) {
                    // Remove Remote
                    updateAction(Constants.TRANSFER.DELETE_LOCAL, tFile);
                } else {
                    // Download
                    updateAction(Constants.TRANSFER.DOWNLOAD, tFile);
                }
                setResult(RESULT_OK);
                finish();
                break;
            default:
                break;
        }
    }

    private void updateAction(String action, FileTransferHelper tFile) {
        // Update log
        ArrayList<FileTransferHelper> tFiles = new ArrayList<FileTransferHelper>();
        tFiles.add(tFile);
        Intent localIntent = new Intent(Constants.ACTION.SINGLE_TRANSFER);
        localIntent.putExtra(Constants.TRANSFER.ACTION, action);
        localIntent.putParcelableArrayListExtra(Constants.TRANSFER.FILES, tFiles);
        localIntent.putExtra(Constants.TRANSFER.LOG_ID, logId);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        // Send Transfer Action (Upload/Download)
        Intent intent = new Intent(this, TransferService.class);
        intent.putExtra(Constants.ACTION.TRANSFER, action);
        intent.putExtra(Constants.TRANSFER.FILE, tFile);
        intent.putExtra(Constants.TRANSFER.LOG_ID, logId);
        startService(intent);
        // TODO: update save list
    }

    private void fillData(FileTransferHelper tFile) {
        if (tFile == null) return;

        String localPath = tFile.getLocalPath();
        String localFileDate = new Date(tFile.getLocalTime()).toString();
        String localFileSize = tFile.getLocalSize().toString();
        if (localFileSize.equals("-1")) {
            localFileSize = getString(R.string.removed);
        }
        String remotePath = tFile.getRemotePath();
        String remoteFileDate = new Date(tFile.getRemoteTime()).toString();
        String remoteFileSize = tFile.getRemoteSize().toString();
        if (remoteFileSize.equals("-1")) {
            remoteFileSize = getString(R.string.removed);
        }
        setTitle(getString(R.string.title_conflict));
        mLocalPath.setText(localPath);
        mLocalFileDate.setText(localFileDate);
        mLocalFileSize.setText(localFileSize);
        mRemotePath.setText(remotePath);
        mRemoteFileDate.setText(remoteFileDate);
        mRemoteFileSize.setText(remoteFileSize);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (tFile != null)
            outState.putParcelable(Constants.TRANSFER.FILE, tFile);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

} 