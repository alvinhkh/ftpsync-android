package com.alvinhkh.ftpsync.view.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.net.FtpsUtils;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.synclist.PathsProvider;
import com.alvinhkh.ftpsync.synclist.PathsTable;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.IOException;

/*
 * SyncListItemCheckActivity allows to check entered item
 */
public class SyncListItemCheckActivity extends Activity implements OnClickListener {

    private static final String TAG = "SyncListItemCheck";

    private Context mContext;
    private TextView mLocalPathText;
    private TextView mRemotePathText;
    private Button mConfirmButton;
    private Button mBackButton;
    private ProgressBar mProgressBar;
    private TextView mMessage;

    private StatusCheck statusCheck;

    private String localPath;
    private String remotePath;
    private String initialLocalPath = "";
    private String initialRemotePath = "";

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.dialog_synclist_item_check);
        setTitle(R.string.title_confirmation);
        // side dialog width and height
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // prevent cancel when touch outside
        this.setFinishOnTouchOutside(false);
        // get context
        mContext = SyncListItemCheckActivity.this;
        // get widgets
        mLocalPathText = (TextView) findViewById(R.id.text_localPath);
        mRemotePathText = (TextView) findViewById(R.id.text_remotePath);
        mConfirmButton = (Button) findViewById(R.id.button_confirm);
        mBackButton = (Button) findViewById(R.id.button_back);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_check);
        mMessage = (TextView) findViewById(R.id.text_message);
        // set listeners
        mConfirmButton.setOnClickListener(this);
        mBackButton.setOnClickListener(this);

        mConfirmButton.setEnabled(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            localPath = extras.getString(PathsTable.COLUMN_LOCAL);
            remotePath = extras.getString(PathsTable.COLUMN_REMOTE);
            initialLocalPath = extras.getString("INIT_" + PathsTable.COLUMN_LOCAL);
            initialRemotePath = extras.getString("INIT_" + PathsTable.COLUMN_REMOTE);
            boolean editing = extras.getBoolean("EDIT", false)
                    && !TextUtils.isEmpty(initialLocalPath)
                    && !TextUtils.isEmpty(initialRemotePath)
                    && initialLocalPath.equals(localPath)
                    && initialRemotePath.equals(remotePath);
            mLocalPathText.setText(localPath);
            mRemotePathText.setText(remotePath);
            if (getDuplicateCount(localPath, remotePath) > 0 && editing == false) {
                // If exact same combination of local and remote path exist
                mProgressBar.setIndeterminate(false);
                mProgressBar.setProgress(100);
                mMessage.setText(R.string.path_checked_duplicated);
                mConfirmButton.setText(R.string.button_fail);
            } else if ((getDuplicatePathCount(localPath, PathsTable.COLUMN_LOCAL) > 0 ||
                        getDuplicatePathCount(remotePath, PathsTable.COLUMN_REMOTE) > 0)
                    && editing == false) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_confirmation)
                        .setMessage(R.string.path_either_path_exist)
                        .setPositiveButton(R.string.button_continue, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                statusCheck = new StatusCheck();
                                statusCheck.execute();
                            }
                        })
                        .setNegativeButton(R.string.button_no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                setResult(RESULT_CANCELED);
                                finish();
                            }
                        }).show();
            } else {
                statusCheck = new StatusCheck();
                statusCheck.execute();
            }

        } else {
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_confirm:
                Intent intentMessage = new Intent();
                // put the message to return as result in Intent
                intentMessage.putExtra(Constants.ACTION.DONE, "Done");
                // Set The Result in Intent
                setResult(RESULT_OK, intentMessage);
                finish();
                break;
            case R.id.button_back:
                clearEntry();
                setResult(RESULT_CANCELED);
                finish();
                break;
            default:
                break;
        }
    }

    private void clearEntry() {
        mLocalPathText.setText("");
        mRemotePathText.setText("");
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private int getDuplicateCount(String localPath, String remotePath) {
        int count = 0;
        Cursor cursor = getContentResolver().query(PathsProvider.CONTENT_URI,
                null,
                PathsTable.COLUMN_LOCAL + "=? AND " + PathsTable.COLUMN_REMOTE + "=?",
                new String[] {localPath, remotePath}, null);
        if (cursor != null) {
            cursor.moveToFirst();
            count = cursor.getCount();
            // always close the cursor
            cursor.close();
        }
        return count;
    }

    private int getDuplicatePathCount(String path, String type) {
        int count = 0;
        Cursor cursor = getContentResolver().query(PathsProvider.CONTENT_URI,
                null,
                type + "=?",
                new String[] {path}, null);
        if (cursor != null) {
            cursor.moveToFirst();
            count = cursor.getCount();
            // always close the cursor
            cursor.close();
        }
        return count;
    }

    private class StatusCheck extends AsyncTask<Void, String, String> {

        FtpsUtils mFtpsUtils = null;
        ImageView imageView_local;
        ImageView imageView_remote;

        @Override
        protected String doInBackground(Void... params) {
            String status = null;
            String local_status = null;
            String remote_status = null;
            String local_type = null;
            String remote_type = null;
            if (mFtpsUtils == null || !mFtpsUtils.isConnected()) {
                PreferencesHelper ftpsSettings = new PreferencesHelper().getFtpsSettings(getApplication());
                mFtpsUtils = new FtpsUtils(ftpsSettings, 6000);
                mFtpsUtils.setup(getApplicationContext());
            }
            TextView local_textView = (TextView) findViewById(R.id.text_localPath);
            TextView remote_textView = (TextView) findViewById(R.id.text_remotePath);
            String localPath = local_textView.getText().toString();
            String remotePath = remote_textView.getText().toString();
            File localFile = new File(localPath);

            local_status = "false";
            if (localFile != null && localFile.exists()) {
                if (localFile.canRead() && localFile.canWrite()) {
                    local_status = "true";
                } else {
                    Log.d(TAG, getString(R.string.path_local_no_right));
                }
                if (localFile.isDirectory()) {
                    local_type = "d";
                } else if (localFile.isFile()) {
                    local_type = "f";
                }
            } else {
                Log.d(TAG, getString(R.string.path_local_not_exist));
            }
            publishProgress(local_status, remote_status);
            if (!local_status.equals("true")) {
                return "false";
            }

            if (mFtpsUtils.isConnected()) {
                try {
                    remote_status = "false";
                    FTPFile current = mFtpsUtils.getClient().mlistFile(remotePath);
                    if (current != null) {
                        if (mFtpsUtils.isDirectory(remotePath)) {
                            remote_type = "d";
                        } else {
                            if (mFtpsUtils.isFile(remotePath)) {
                                remote_type = "f";
                            }
                            FTPFile[] ftpFiles = mFtpsUtils.getClient().listFiles(remotePath);
                            current = ftpFiles[0];
                        }
                        if (!FTPReply.isPositiveCompletion(mFtpsUtils.getClient().getReplyCode())) {
                            Log.d(TAG, "No valid authentication");
                        } else {
                            boolean accessRights = mFtpsUtils.hasAccess(remotePath);
                            if (current != null && accessRights) {
                                remote_status = "true";
                                publishProgress(local_status, remote_status);
                            } else {
                                Log.d(TAG, getString(R.string.path_remote_no_right));
                            }
                        }
                    } else {
                        Log.d(TAG, getString(R.string.path_remote_not_exist));
                    }
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                    remote_status = null;
                }
                publishProgress(local_status, remote_status);
            } else {
                remote_status = "false-connection";
                publishProgress(local_status, remote_status);
            }
            if (local_status != null && remote_status != null) {
                status = "false";
                if (local_status.equals("true") && remote_status.equals("true")) {
                    if (!local_type.equals(remote_type)) {
                        status = "false-type";
                    } else {
                        status = "true";
                    }
                }
            }
            return status;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String status_local = values[0];
            String status_remote = values[1];
            if (status_local != null) {
                if (status_local.equals("true")) {
                    imageView_local.setImageResource(R.drawable.ic_cloud_done_black_48dp);
                } else if (status_local.equals("false")) {
                    imageView_local.setImageResource(R.drawable.ic_cloud_off_black_48dp);
                } else {
                    imageView_local.setImageResource(R.drawable.ic_cloud_queue_black_48dp);
                }
            }
            if (status_remote != null) {
                if (status_remote.equals("true")) {
                    imageView_remote.setImageResource(R.drawable.ic_cloud_done_black_48dp);
                } else if (status_remote.equals("false")) {
                    imageView_remote.setImageResource(R.drawable.ic_cloud_off_black_48dp);
                } else {
                    imageView_remote.setImageResource(R.drawable.ic_cloud_queue_black_48dp);
                }
                if (status_remote.equals("false-connection")) {
                    if (mMessage != null)
                        mMessage.setText(R.string.ftps_connection_fail);
                }
            }
        }

        @Override
        protected void onPreExecute() {
            if (mProgressBar != null)
                mProgressBar.setIndeterminate(true);
            if (mMessage != null)
                mMessage.setText(R.string.path_checking);
            if (mConfirmButton != null)
                mConfirmButton.setText(R.string.button_checking);
            imageView_local = (ImageView) findViewById(R.id.image_localPath);
            imageView_remote = (ImageView) findViewById(R.id.image_remotePath);
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (mProgressBar != null) {
                    mProgressBar.setIndeterminate(false);
                    mProgressBar.setProgress(100);
                }
                if (result.equals("true")) {
                    if (mMessage != null)
                        mMessage.setText(R.string.path_checked_ok);
                    if (mConfirmButton != null) {
                        mConfirmButton.setEnabled(true);
                        mConfirmButton.setText(R.string.button_confirm);
                    }
                } else if (result.equals("false")) {
                    if (mMessage != null)
                        mMessage.setText(R.string.path_checked_fail);
                    if (mConfirmButton != null)
                        mConfirmButton.setText(R.string.button_fail);
                } else if (result.equals("false-type")) {
                    if (mMessage != null)
                        mMessage.setText(R.string.path_checked_false_type);
                    if (mConfirmButton != null)
                        mConfirmButton.setText(R.string.button_fail);
                } else {
                    if (mMessage != null)
                        mMessage.setText("-");
                    if (mConfirmButton != null)
                        mConfirmButton.setText(R.string.button_fail);
                }
            }
        }
    }
} 