package com.alvinhkh.ftpsync.view.dialog;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.synclist.PathsProvider;
import com.alvinhkh.ftpsync.synclist.PathsTable;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.io.File;

/*
 * SyncListEditActivity allows to enter a new path item
 * or to change an existing
 */
public class SyncListEditActivity extends Activity implements OnClickListener {

    private static final String TAG = "SyncListEdit";

    private Context mContext;
    private EditText mLocalPathText;
    private EditText mRemotePathText;
    private ImageButton mLocalPathButton;
    private ImageButton mRemotePathButton;
    private Button mConfirmButton;
    private Button mDismissButton;

    private Uri dirUri;

    private String initialLocalPath = "";
    private String initialRemotePath = "";
    private Boolean mEditing = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.dialog_synclist_item_edit);
        setTitle(R.string.title_add);
        // side dialog width and height
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        // prevent cancel when touch outside
        this.setFinishOnTouchOutside(false);
        // get context
        mContext = SyncListEditActivity.this;
        // get widgets
        mLocalPathText = (EditText) findViewById(R.id.edit_pathLocal);
        mRemotePathText = (EditText) findViewById(R.id.edit_pathRemote);
        mLocalPathButton = (ImageButton) findViewById(R.id.button_selectLocal);
        mRemotePathButton = (ImageButton) findViewById(R.id.button_selectRemote);
        mConfirmButton = (Button) findViewById(R.id.button_confirm);
        mDismissButton = (Button) findViewById(R.id.button_dismiss);
        // set listeners
        mLocalPathButton.setOnClickListener(this);
        mRemotePathButton.setOnClickListener(this);
        mConfirmButton.setOnClickListener(this);
        mDismissButton.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        // check from the saved Instance
        dirUri = (bundle == null) ? null : (Uri) bundle
                .getParcelable(PathsProvider.CONTENT_ITEM_TYPE);
        // Or passed from the other activity
        if (extras != null) {
            dirUri = extras.getParcelable(PathsProvider.CONTENT_ITEM_TYPE);
            setTitle(R.string.title_edit);
            fillData(dirUri);
            mEditing = true;
        }
    }

    @Override
    public void onClick(View view) {
        String localPath = mLocalPathText.getText().toString();
        String remotePath = mRemotePathText.getText().toString();
        switch (view.getId()) {
            case R.id.button_confirm:
                if (!TextUtils.isEmpty(localPath) && !TextUtils.isEmpty(remotePath)) {
                    Intent i = new Intent(mContext, SyncListItemCheckActivity.class);
                    i.putExtra(PathsTable.COLUMN_LOCAL, localPath);
                    i.putExtra(PathsTable.COLUMN_REMOTE, remotePath);
                    i.putExtra("EDIT", mEditing);
                    i.putExtra("INIT_" + PathsTable.COLUMN_LOCAL, initialLocalPath);
                    i.putExtra("INIT_" + PathsTable.COLUMN_REMOTE, initialRemotePath);
                    startActivityForResult(i, Constants.ACTION_ID.SYNC_LIST_ITEM_CHECK);
                } else {
                    makeToast(getString(R.string.require_all_fields));
                }
                break;
            case R.id.button_dismiss:
                if (!(mLocalPathText.getText().toString().equals(initialLocalPath) &&
                        mRemotePathText.getText().toString().equals(initialRemotePath)) &&
                        !(TextUtils.isEmpty(mLocalPathText.getText()) &&
                                TextUtils.isEmpty(mRemotePathText.getText()))
                        ) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.title_confirmation)
                            .setMessage(R.string.made_changes_do_you_want_to_dismiss)
                            .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    clearEntry();
                                    makeToast(getString(R.string.cancelled));
                                    setResult(RESULT_CANCELED);
                                    finish();
                                }
                            })
                            .setNegativeButton(R.string.button_no, null).show();
                } else {
                    clearEntry();
                    makeToast(getString(R.string.cancelled));
                    setResult(RESULT_CANCELED);
                    finish();
                }
                break;
            case R.id.button_selectLocal:
                Intent intentPickLocal = new Intent("com.alvinhkh.ftpsync.filepicker.GET_LOCAL");
                intentPickLocal.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                intentPickLocal.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, true);
                intentPickLocal.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE_AND_DIR);
                File localFile = new File(localPath);
                if (localFile.isFile()) {
                    localPath = localFile.getParent();
                }
                intentPickLocal.putExtra(FilePickerActivity.EXTRA_START_PATH, localPath);
                startActivityForResult(intentPickLocal, Constants.ACTION_ID.FILE_PICKER_LOCAL);
                break;
            case R.id.button_selectRemote:
                Intent intentPickRemote = new Intent("com.alvinhkh.ftpsync.filepicker.GET_REMOTE");
                intentPickRemote.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                intentPickRemote.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, true);
                intentPickRemote.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_DIR);
                intentPickRemote.putExtra(FilePickerActivity.EXTRA_START_PATH, remotePath);
                startActivityForResult(intentPickRemote, Constants.ACTION_ID.FILE_PICKER_REMOTE);
                break;
            default:
                break;
        }
    }

    private void clearEntry() {
        mLocalPathText.setText("");
        mRemotePathText.setText("");
    }

    private void fillData(Uri uri) {
        String[] projection = { PathsTable.COLUMN_LOCAL,
                PathsTable.COLUMN_REMOTE };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            initialLocalPath = cursor.getString(cursor
                    .getColumnIndexOrThrow(PathsTable.COLUMN_LOCAL));
            initialRemotePath = cursor.getString(cursor
                    .getColumnIndexOrThrow(PathsTable.COLUMN_REMOTE));
            mLocalPathText.setText(initialLocalPath);
            mRemotePathText.setText(initialRemotePath);
            // always close the cursor
            cursor.close();
        }
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (dirUri != null)
        outState.putParcelable(PathsProvider.CONTENT_ITEM_TYPE, dirUri);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void saveRecord() {
        String localPath = mLocalPathText.getText().toString();
        String remotePath = mRemotePathText.getText().toString();

        // save only if both local and remote path are available
        if (remotePath.length() == 0 || localPath.length() == 0) {
            return;
        }

        ContentValues values = new ContentValues();
        values.put(PathsTable.COLUMN_LOCAL, localPath);
        values.put(PathsTable.COLUMN_REMOTE, remotePath);

        if (dirUri != null) {
            // Update item
            try {
                getContentResolver().update(dirUri, values, null, null);
            } catch (IllegalArgumentException e) {
                dirUri = getContentResolver().insert(PathsProvider.CONTENT_URI, values);
            }
            if (localPath.equals(initialLocalPath) && remotePath.equals(initialRemotePath)) {
                makeToast(getString(R.string.updated_nothings));
            } else {
                if (!remotePath.equals(initialRemotePath)) {
                    // Make sure wont use wrong saved remote list
                    String path = localPath + File.separator + ".ftpsync-remote";
                    File remoteLocalSave = new File(path);
                    if (remoteLocalSave.exists()) {
                        remoteLocalSave.delete();
                        Log.d(TAG, "Deleted: " + path);
                    }
                }
                makeToast(getString(R.string.updated));
            }
        } else {
            // New item
            dirUri = getContentResolver().insert(PathsProvider.CONTENT_URI, values);
            makeToast(getString(R.string.added));
        }
    }

    private void makeToast(String message) {
        if (mContext == null || message.length() < 1) return;
        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            Log.e(TAG, "resultCode error!");
            return;
        }
        if (requestCode == Constants.ACTION_ID.FILE_PICKER_LOCAL)
        {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false))
            {
                /*
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                {
                    ClipData clip = data.getClipData();

                    if (clip != null)
                    {
                        StringBuilder sb = new StringBuilder();

                        for (int i = 0; i < clip.getItemCount(); i++)
                        {
                            Uri uri = clip.getItemAt(i).getUri();
                            // Do something with the URI

                            sb.append(uri.getPath());
                            sb.append("\n");
                        }

                        textview_service.setText(sb.toString());
                    }
                    // For Ice Cream Sandwich
                }
                else
                {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);

                    if (paths != null)
                    {
                        for (String path: paths)
                        {
                            Uri uri = Uri.parse(path);
                            // Do something with the URI
                        }
                    }
                }*/
            }
            else
            {
                Uri uri = data.getData();
                // Do something with the URI
                mLocalPathText.setText(uri.getPath());

                File localFile = new File(uri.getPath());
                if (localFile.isDirectory()){
                    makeToast("Local Path is a Directory");
                } else if (localFile.isFile()){
                    makeToast("Local Path is a Single file");
                }
            }
        } else if (requestCode == Constants.ACTION_ID.FILE_PICKER_REMOTE)
        {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false))
            {
                // Multiple
            }
            else
            {
                Uri uri = data.getData();
                // Do something with the URI
                Boolean isDirectory = uri.getQueryParameter("isDirectory").toLowerCase().equals("true");
                if (isDirectory == true) {
                    makeToast("Remote Path is a Directory");
                }
                mRemotePathText.setText(uri.getPath());
            }
        } else if (requestCode == Constants.ACTION_ID.SYNC_LIST_ITEM_CHECK)
        {
            saveRecord();
            setResult(RESULT_OK);
            finish();
        }
    }
} 