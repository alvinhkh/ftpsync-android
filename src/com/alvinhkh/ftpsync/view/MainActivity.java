package com.alvinhkh.ftpsync.view;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.net.FtpsUtils;
import com.alvinhkh.ftpsync.preference.PreferencesActivity;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.sync.ForegroundService;
import com.alvinhkh.ftpsync.view.fragment.LogFragment;
import com.alvinhkh.ftpsync.view.fragment.MainFragment;
import com.alvinhkh.ftpsync.view.fragment.SyncListFragment;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class MainActivity extends FragmentActivity {

    private static final String TAG = "MainActivity";

    TabPagerAdapter mTabPagerAdapter;
    ViewPager mViewPager;
    Menu mMenu;
    List<Fragment> mFragments;
    List<String> mTitles;

    private volatile boolean connected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(R.layout.activity_tabs);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        mFragments = new ArrayList<Fragment>();
        mFragments.add(MainFragment.newInstance());
        mFragments.add(LogFragment.newInstance());
        mFragments.add(SyncListFragment.newInstance());

        mTitles = new ArrayList<String>();
        mTitles.add(getString(R.string.title_main));
        mTitles.add(getString(R.string.title_log));
        mTitles.add(getString(R.string.title_path));

        mTabPagerAdapter = new TabPagerAdapter(
                getSupportFragmentManager(), mFragments, mTitles);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(mTabPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);

        // Give the SlidingTabLayout the ViewPager
        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        // Center the tabs in the layout
        slidingTabLayout.setDistributeEvenly(true);
        // Customize tab color
        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary));
        slidingTabLayout.setSelectorTextColor(getResources().getColorStateList(R.color.selector));
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.secondary_text));
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.primary_white);
            }
        });
        slidingTabLayout.setViewPager(mViewPager);
        slidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                ((TabPagerAdapter) mViewPager.getAdapter()).set_current_position(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                getActionBar().show();
            }
        });

        // Start Foreground Service
        Intent i = new Intent(getApplicationContext(), ForegroundService.class);
        i.setAction(Constants.ACTION.START_FOREGROUND);
        getApplicationContext().startService(i);

    }

    @Override
    public void onResume() {

        // If ftps settings have not set, open preference screen.
        PreferencesHelper preferencesHelper = new PreferencesHelper().getFtpsSettings(getApplicationContext());
        if (!preferencesHelper.isSet()) {
            Intent i = new Intent(getBaseContext(), PreferencesActivity.class);
            i.putExtra(Constants.TRIGGER.MISSING_SETTINGS, true);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplication().startActivity(i);
        } else {
            if (connected != true) new FtpsStatusCheck().execute();
        }

        // Make sure we have client id
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (sPref.getString("client_id", "").isEmpty()) {
            sPref.edit().putString("client_id", UUID.randomUUID().toString()).commit();
            Log.d(TAG, "Client ID (Updated): " + sPref.getString("client_id", ""));
        }

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(serviceStateReceiver, new IntentFilter(Constants.ACTION.FOREGROUND_STATE));
        super.onResume();

    }

    @Override
    public void onDestroy() {

        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(serviceStateReceiver);
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        selectMenu(menu);
        mMenu = menu;
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.clear();
        selectMenu(menu);
        mMenu = menu;
        return super.onPrepareOptionsMenu(menu);

    }

    private void selectMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_tabs, menu);
        updateSwitch();

    }

    private void updateSwitch() {

        if (mMenu != null) {
            final MenuItem toggle_service = mMenu.findItem(R.id.toggle_service);
            final Switch actionView = (Switch) toggle_service.getActionView();
            actionView.setChecked(isServiceRunning(ForegroundService.class));
            actionView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // Start or stop your Service
                    if (isChecked) {
                        Intent startIntent = new Intent(getApplicationContext(), ForegroundService.class);
                        startIntent.setAction(Constants.ACTION.START_FOREGROUND);
                        getApplicationContext().startService(startIntent);
                    } else {
                        Intent stopIntent = new Intent(getApplicationContext(), ForegroundService.class);
                        stopIntent.setAction(Constants.ACTION.STOP_FOREGROUND);
                        getApplicationContext().startService(stopIntent);
                    }
                }
            });
        }

    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_preferences:
                try{
                    Intent intent = new Intent(this, PreferencesActivity.class);
                    startActivity(intent);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private BroadcastReceiver serviceStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.FOREGROUND_STATE)) {
                updateSwitch();
            }
        }
    };

    private class FtpsStatusCheck extends AsyncTask<Void, String, String> {

        FtpsUtils mFtpsUtils = null;

        @Override
        protected String doInBackground(Void... params) {
            String status = null;
            if (mFtpsUtils == null || !mFtpsUtils.isConnected()) {
                PreferencesHelper ftpsSettings = new PreferencesHelper().getFtpsSettings(getApplicationContext());
                mFtpsUtils = new FtpsUtils(ftpsSettings, 3600);
                mFtpsUtils.setup(getApplicationContext());
                if (mFtpsUtils.isConnected()) {
                    status = "true";
                    connected = true;
                } else {
                    status = "false";
                }
            }
            return status;
        }

        @Override
        protected void onPreExecute() {
            // Show message via snack bar
            SnackbarManager.show(
                    Snackbar.with(MainActivity.this)
                            .text(R.string.ftps_connection_check)
                            .type(SnackbarType.MULTI_LINE)
                            .swipeToDismiss(false)
                            .animation(false)
                            .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE));
        }

        @Override
        protected void onPostExecute(String result) {
            String status = result;
            int message;
            Snackbar.SnackbarDuration duration;
            if (status.equals("true")) {
                message = R.string.ftps_connection_successful;
                duration = Snackbar.SnackbarDuration.LENGTH_SHORT;
            } else {
                message = R.string.ftps_connection_fail;
                duration = Snackbar.SnackbarDuration.LENGTH_INDEFINITE;
            }
            if (message > 0) {
                // Show message via snack bar
                SnackbarManager.show(
                        Snackbar.with(MainActivity.this)
                                .text(message)
                                .type(SnackbarType.MULTI_LINE)
                                .swipeToDismiss(false)
                                .animation(false)
                                .duration(duration));
            }
        }
    }

}
