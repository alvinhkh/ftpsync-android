package com.alvinhkh.ftpsync.view.fragment;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.log.LogProvider;
import com.alvinhkh.ftpsync.log.LogTable;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ScrollDirectionListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/*
 * LogFragment displays the log items in a list
 *
 * You can delete via a long press on the item
 * You can delete all via FAB
 */

public class LogFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "LogFragment";

    private Context mContext = super.getActivity();
    private Menu mMenu;
    private ListView mListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private static final int DELETE_ID = Menu.FIRST + 1;
    private static final int LOADER_ID = 1;
    private SimpleCursorAdapter adapter;
    private String filter;

    private boolean hide_overview = false;
    private boolean hide_message = false;
    private boolean hide_verbose = false;

    private static final String LIST_STATE = "ListState";
    private Parcelable mListState = null;
    private int mListIndex = 0;
    private int mListTop = 0;

    public static LogFragment newInstance() {
        Bundle args = new Bundle();
        LogFragment fragment = new LogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = super.getActivity();
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_log, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_log);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setDividerHeight(2);
        registerForContextMenu(mListView);
        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.attachToListView(mListView, new ScrollDirectionListener() {

            @Override
            public void onScrollDown() {
                fab.show();
            }

            @Override
            public void onScrollUp() {
                fab.hide();
            }

        }, new AbsListView.OnScrollListener() {

            int mLastFirstVisibleItem = 0;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (view.getId() == mListView.getId()) {
                    final int currentFirstVisibleItem = mListView.getFirstVisiblePosition();
                    if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                        getActivity().getActionBar().hide();
                    } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                        getActivity().getActionBar().show();
                    }
                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (mListView != null && mListView.getChildCount() > 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = mListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = mListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }

        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListView != null && mListView.getCount() > 0) {
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.title_confirmation)
                            .setMessage(R.string.do_you_want_to_clear_log)
                            .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    clearLog();
                                    // Show message via snack bar
                                    SnackbarManager.show(
                                            Snackbar.with(getActivity())
                                                    .text(R.string.cleared)
                                                    .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                                            , (android.view.ViewGroup) getActivity().findViewById(R.id.view_log_list_layout));
                                }
                            })
                            .setNegativeButton(R.string.button_no, null).show();
                } else {
                    // Show message via snack bar
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(R.string.clear_nothing)
                                    .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
                            , (android.view.ViewGroup) getActivity().findViewById(R.id.view_log_list_layout));
                }
            }
        });
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("hide_overview", hide_overview);
        outState.putBoolean("hide_message", hide_message);
        outState.putBoolean("hide_verbose", hide_verbose);
        mListState = getListView().onSaveInstanceState();
        outState.putParcelable(LIST_STATE, mListState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(LIST_STATE);
            // Restore last state
            hide_overview = savedInstanceState.getBoolean("hide_overview", false);
            hide_message = savedInstanceState.getBoolean("hide_message", false);
            hide_verbose = savedInstanceState.getBoolean("hide_verbose", false);
            updateCheckbox();
            updateLogFilter();
        }
        fillData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Restore previous state (including selected item index and scroll position)
        if (mListState != null && getListView() != null) {
            getListView().onRestoreInstanceState(mListState);
        }
        mListState = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mMenu = menu;
        selectMenu(menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mMenu = menu;
        selectMenu(menu);
    }

    private void selectMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.menu_log, menu);
        updateCheckbox();
    }

    private void updateLogFilter() {
        if (mMenu == null) return;
        final MenuItem filter_overview = mMenu.findItem(R.id.action_filter_overview);
        hide_overview = filter_overview.isChecked();
        final MenuItem filter_message = mMenu.findItem(R.id.action_filter_message);
        hide_message = filter_message.isChecked();
        final MenuItem filter_verbose = mMenu.findItem(R.id.action_filter_verbose);
        hide_verbose = filter_verbose.isChecked();

        List<String> filterList = new ArrayList<String>();
        if (hide_overview) {
            filterList.add(LogTable.COLUMN_TYPE + " <> 'overview'");
        }
        if (hide_message) {
            filterList.add(LogTable.COLUMN_TYPE + " <> 'message'");
        }
        if (hide_verbose) {
            filterList.add(LogTable.COLUMN_TYPE + " <> 'verbose'");
        }
        filter = "";
        for (int i = 0; i < filterList.size(); i++) {
            filter += filterList.get(i);
            if (i < filterList.size() - 1) {
                filter += " AND ";
            }
        }
    }

    private void updateCheckbox() {
        if (mMenu == null) return;
        final MenuItem filter_overview = mMenu.findItem(R.id.action_filter_overview);
        filter_overview.setChecked(hide_overview);
        final MenuItem filter_message = mMenu.findItem(R.id.action_filter_message);
        filter_message.setChecked(hide_message);
        final MenuItem filter_verbose = mMenu.findItem(R.id.action_filter_verbose);
        filter_verbose.setChecked(hide_verbose);
    }

    // Reaction to the menu selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter_overview:
            case R.id.action_filter_message:
            case R.id.action_filter_verbose:
                item.setChecked(!item.isChecked());
                updateLogFilter();
                onRefresh();
                return true;
            case R.id.action_clear:
                clearLog();
                fillData();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clearLog() {
        updateLogFilter();
        List<String> selectionList = new ArrayList<String>();
        if (hide_overview)
            selectionList.add("overview");
        if (hide_message)
            selectionList.add("message");
        if (hide_verbose)
            selectionList.add("verbose");
        String selection = "";
        String[] selectionArg = new String[ selectionList.size() ];
        selectionList.toArray(selectionArg);
        for (int i = 0; i < selectionList.size(); i++) {
            selection += LogTable.COLUMN_TYPE + "<>?";
            if (i < selectionList.size() - 1)
                selection += " AND ";
        }
        if (selection.isEmpty()) {
            mContext.getContentResolver().delete(LogProvider.CONTENT_URI, null, null);
        } else {
            mContext.getContentResolver().delete(
                    Uri.withAppendedPath(LogProvider.CONTENT_URI, "/FILTERED"),
                    selection,
                    selectionArg
            );
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case DELETE_ID:
                Uri uri = Uri.parse(LogProvider.CONTENT_URI + "/" + info.id);
                mContext.getContentResolver().delete(uri, null, null);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    // Opens the second activity if an entry is clicked
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        l.showContextMenuForChild(v); // Show Context Menu
    }

    private void fillData() {
        // Fields from the database (projection)
        // Must include the _id column for the adapter to work
        String[] from = new String[] {
                LogTable.COLUMN_ACTION,
                LogTable.COLUMN_DATE,
                LogTable.COLUMN_LOCAL,
                LogTable.COLUMN_REMOTE,
                LogTable.COLUMN_STATUS,
                LogTable.COLUMN_TYPE,
        };
        // Fields on the UI to which we map
        int[] to = new int[] {
                R.id.text_action,
                R.id.text_date,
                R.id.text_localPath,
                R.id.text_remotePath,
                R.id.text_status,
                R.id.text_type
        };

        getActivity().getLoaderManager().destroyLoader(LOADER_ID);
        getActivity().getLoaderManager().initLoader(LOADER_ID, null, this);
        adapter = new SimpleCursorAdapter(mContext, R.layout.list_row_log, null, from, to, 0);
        adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {

            public boolean setViewValue(View aView, Cursor aCursor, int aColumnIndex) {

                if (aColumnIndex == aCursor.getColumnIndexOrThrow(LogTable.COLUMN_ACTION)) {
                    // Sync Action
                    String action = aCursor.getString(aColumnIndex);
                    View parentView = (View) aView.getParent().getParent();
                    TextView aMessageTextView = (TextView) parentView.findViewById(R.id.text_message);
                    aMessageTextView.setText(action);
                    String syncAction;
                    Integer image;
                    if (action.equals(Constants.ACTION.UPLOAD)) {
                        syncAction = getString(R.string.action_upload);
                        image = R.drawable.ic_file_upload_black_48dp;
                    } else if (action.equals(Constants.ACTION.DOWNLOAD)) {
                        syncAction = getString(R.string.action_download);
                        image = R.drawable.ic_file_download_black_48dp;
                    } else if (action.equals(Constants.ACTION.DELETE_LOCAL)) {
                        syncAction = getString(R.string.action_delete_local);
                        image = R.drawable.ic_remove_black_48dp;
                    } else if (action.equals(Constants.ACTION.REMOVE_REMOTE)) {
                        syncAction = getString(R.string.action_remove_remote);
                        image = R.drawable.ic_remove_black_48dp;
                    } else if (action.equals(Constants.ACTION.CONFLICT)) {
                        syncAction = getString(R.string.action_conflict);
                        image = R.drawable.ic_error_black_48dp;
                    } else if (action.equals(Constants.ACTION.SKIP)) {
                        syncAction = getString(R.string.action_skip);
                        image = R.drawable.ic_skip_next_black_48dp;
                    } else if (action.equals(Constants.ACTION.SYNC)) {
                        syncAction = getString(R.string.action_sync);
                        image = R.drawable.ic_cloud_circle_black_48dp;
                    } else {
                        syncAction = getString(R.string.action_unknown);
                        image = R.drawable.ic_error_black_48dp;
                    }
                    ImageView imageView = (ImageView) parentView.findViewById(R.id.sync_icon);
                    if (imageView != null && image > 0) {
                        imageView.setImageResource(image);
                    }
                    TextView textView = (TextView) aView;
                    textView.setText(syncAction);
                    return true;
                } else if (aColumnIndex == aCursor.getColumnIndexOrThrow(LogTable.COLUMN_DATE)) {
                    Long date = aCursor.getLong(aColumnIndex);
                    String pattern = android.text.format.DateFormat.getBestDateTimePattern(Locale.getDefault(), "yyyy-MM-dd HH:mm:ss");
                    String syncDate = new SimpleDateFormat(pattern).format(date);
                    TextView textView = (TextView) aView;
                    textView.setText(syncDate);
                    return true;
                } else if (aColumnIndex == aCursor.getColumnIndexOrThrow(LogTable.COLUMN_STATUS)) {
                    Integer status = aCursor.getInt(aColumnIndex);
                    Integer image = R.drawable.ic_block_black_48dp;
                    String syncStatus = getString(R.string.status_false);
                    if (status > 0) {
                        if (status == 2) {
                            syncStatus = getString(R.string.status_skip);
                        } else {
                            syncStatus = getString(R.string.status_done);
                        }
                        image = R.drawable.ic_done_black_48dp;
                    }
                    View parentView = (View) aView.getParent().getParent();
                    ImageView imageView = (ImageView) parentView.findViewById(R.id.static_icon);
                    if (imageView != null && image > 0) {
                        imageView.setImageResource(image);
                    }
                    TextView textView = (TextView) aView;
                    textView.setText(syncStatus);
                    return true;
                } else if (aColumnIndex == aCursor.getColumnIndexOrThrow(LogTable.COLUMN_TYPE)) {
                    // Log Type
                    String aLogType = aCursor.getString(aColumnIndex);
                    View rowView = (View) aView.getParent().getParent().getParent();
                    View rowLeft = rowView.findViewById(R.id.row_left);
                    View rowMessageView = rowView.findViewById(R.id.row_message);
                    View rowLocalView = rowView.findViewById(R.id.row_local);
                    View rowRemoteView = rowView.findViewById(R.id.row_remote);
                    TextView aStatusTextView = (TextView) rowView.findViewById(R.id.text_status);
                    rowLeft.setVisibility(View.VISIBLE);
                    rowMessageView.setVisibility(View.GONE);
                    rowLocalView.setVisibility(View.VISIBLE);
                    rowRemoteView.setVisibility(View.VISIBLE);

                    TextView textView = (TextView) aView;
                    textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                    textView.setText(aLogType.toUpperCase());
                    aStatusTextView.setVisibility(View.VISIBLE);
                    if (aLogType.equals("overview")) {
                        rowView.setAlpha((float) 1); // 0 to 1
                    } else if (aLogType.equals("message")) {
                        aStatusTextView.setVisibility(View.GONE);
                        rowLeft.setVisibility(View.GONE);
                        rowMessageView.setVisibility(View.VISIBLE);
                        rowLocalView.setVisibility(View.GONE);
                        rowRemoteView.setVisibility(View.GONE);
                        rowView.setAlpha((float) 1); // 0 to 1
                    } else if (aLogType.equals("verbose")) {
                        rowView.setAlpha((float) 0.6); // 0 to 1
                    }
                    return true;
                }
                return false;
            }
        });
        setListAdapter(adapter);

        if (mListView != null)
        getActivity().findViewById(R.id.view_log_list_layout)
                .findViewById(android.R.id.list)
                .setVisibility((mListView != null && mListView.getCount() > 0) ? View.INVISIBLE : View.VISIBLE);

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, DELETE_ID, 0, R.string.action_delete);
        // TODO: repeat action
    }

    // creates a new loader after the initLoader () call
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                LogTable.COLUMN_ID,
                LogTable.COLUMN_LOCAL,
                LogTable.COLUMN_REMOTE,
                LogTable.COLUMN_ACTION,
                LogTable.COLUMN_DATE,
                LogTable.COLUMN_STATUS,
                LogTable.COLUMN_TYPE
        };
        CursorLoader cursorLoader = new CursorLoader(mContext,
                LogProvider.CONTENT_URI, projection, filter, null, LogTable.COLUMN_DATE + " DESC");
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() != LOADER_ID) return;
        adapter.swapCursor(data);
        if (mListView != null && mListIndex > 0 && mListTop > 0) {
            // restore the position of listview
            mListView.setSelectionFromTop(mListIndex, mListTop);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() != LOADER_ID) return;
        if (mListView != null) {
            // save index and top position
            mListIndex = mListView.getFirstVisiblePosition();
            View v = mListView.getChildAt(0);
            mListTop = (v == null) ? 0 : v.getTop();
        }
        // data is not available anymore, delete reference
        adapter.swapCursor(null);
    }

    @Override
    public void onRefresh() {
        getActivity().getLoaderManager().restartLoader(LOADER_ID, null, this);

        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }
}