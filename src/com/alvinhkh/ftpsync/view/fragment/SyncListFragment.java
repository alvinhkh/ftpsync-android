package com.alvinhkh.ftpsync.view.fragment;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.synclist.PathsProvider;
import com.alvinhkh.ftpsync.synclist.PathsTable;
import com.alvinhkh.ftpsync.net.FtpsUtils;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.sync.FileTransferHelper;
import com.alvinhkh.ftpsync.sync.ForegroundService;
import com.alvinhkh.ftpsync.view.dialog.SyncListEditActivity;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ScrollDirectionListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/*
 * SyncListFragment displays the existing path items
 * in a list
 *
 * You can create new ones via the ActionBar entry "Insert"
 * Or via FAB to "Insert"
 * You can delete existing ones via a long press on the item
 */

public class SyncListFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "SyncListFragment";

    private Context mContext = super.getActivity();

    private static final int EDIT_ID = Menu.FIRST + 2;
    private static final int DELETE_ID = Menu.FIRST + 3;
    private static final int MANUAL_SYNC_ID = Menu.FIRST + 4;
    private static final int MANUAL_UPLOAD_ID = Menu.FIRST + 5;
    private static final int MANUAL_DOWNLOAD_ID = Menu.FIRST + 6;
    private static final int MANUAL_REMOVE_REMOTE_ID = Menu.FIRST + 7;
    private static final int MANUAL_DELETE_LOCAL_ID = Menu.FIRST + 8;
    private SimpleCursorAdapter adapter;
    private static final int LOADER_ID = 2;

    private ListView mListView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private volatile RowStatusCheck rowStatusCheck = null;

    private int mListIndex = 0;
    private int mListTop = 0;

    public static SyncListFragment newInstance() {
        Bundle args = new Bundle();
        SyncListFragment fragment = new SyncListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = super.getActivity();
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_path, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_path);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setDividerHeight(2);
        registerForContextMenu(mListView);

        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.attachToListView(mListView, new ScrollDirectionListener() {

            @Override
            public void onScrollDown() {
                fab.show();
            }

            @Override
            public void onScrollUp() {
                fab.hide();
            }

        }, new AbsListView.OnScrollListener() {

            int mLastFirstVisibleItem = 0;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (view.getId() == mListView.getId()) {
                    final int currentFirstVisibleItem = mListView.getFirstVisiblePosition();
                    if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                        getActivity().getActionBar().hide();
                    } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                        getActivity().getActionBar().show();
                    }
                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if (mListView != null && mListView.getChildCount() > 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = mListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = mListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                mSwipeRefreshLayout.setEnabled(enable);
            }

        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createPath();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fillData();
        onRefresh();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        if (rowStatusCheck != null)
        rowStatusCheck.cancel(true);
        if (mSwipeRefreshLayout != null)
        mSwipeRefreshLayout.setRefreshing(false);
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        selectMenu(menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        selectMenu(menu);
    }

    private void selectMenu(Menu menu) {
        getActivity().getMenuInflater().inflate(R.menu.menu_path, menu);
    }

    // Reaction to the menu selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_insert:
                createPath();
                return true;
            case R.id.action_refresh:
                onRefresh();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        Uri uri = Uri.parse(PathsProvider.CONTENT_URI + "/" + info.id);
        ArrayList<FileTransferHelper> tFiles;
        switch (item.getItemId()) {
            case DELETE_ID:
                mContext.getContentResolver().delete(uri, null, null);
                fillData();
                return true;
            case EDIT_ID:
                Intent i = new Intent(mContext, SyncListEditActivity.class);
                i.putExtra(PathsProvider.CONTENT_ITEM_TYPE, uri);
                startActivity(i);
                // Reset Icon that represent status
                View v = info.targetView;
                if (v != null) {
                    ImageView imageView = (ImageView) v.findViewById(R.id.image_icon);
                    imageView.setImageResource(R.drawable.ic_cloud_queue_black_48dp);
                }
                return true;
            case MANUAL_SYNC_ID:
                tFiles = getTransferFiles(uri);
                if (tFiles == null) {
                    makeToast(getString(R.string.sync_list_empty));
                } else {
                    commandAction(tFiles, Constants.ACTION.SYNC);
                }
                return true;
            case MANUAL_UPLOAD_ID:
                tFiles = getTransferFiles(uri);
                if (tFiles == null) {
                    makeToast(getString(R.string.sync_list_empty));
                } else {
                    commandAction(tFiles, Constants.ACTION.UPLOAD);
                }
                return true;
            case MANUAL_DOWNLOAD_ID:
                tFiles = getTransferFiles(uri);
                if (tFiles == null) {
                    makeToast(getString(R.string.sync_list_empty));
                } else {
                    commandAction(tFiles, Constants.ACTION.DOWNLOAD);
                }
                return true;
            case MANUAL_REMOVE_REMOTE_ID:
                tFiles = getTransferFiles(uri);
                if (tFiles == null) {
                    makeToast(getString(R.string.sync_list_empty));
                } else {
                    commandAction(tFiles, Constants.ACTION.REMOVE_REMOTE);
                }
                return true;
            case MANUAL_DELETE_LOCAL_ID:
                tFiles = getTransferFiles(uri);
                if (tFiles == null) {
                    makeToast(getString(R.string.sync_list_empty));
                } else {
                    commandAction(tFiles, Constants.ACTION.DELETE_LOCAL);
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private ArrayList<FileTransferHelper> getTransferFiles(Uri uri) {
        String[] projection = { PathsTable.COLUMN_LOCAL, PathsTable.COLUMN_REMOTE };
        Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
        ArrayList<FileTransferHelper> tFiles = new ArrayList<FileTransferHelper>();
        if (cursor != null) {
            cursor.moveToFirst();
            FileTransferHelper tFile = new FileTransferHelper();
            tFile.setLocalAbsolutePath(cursor.getString(cursor
                    .getColumnIndexOrThrow(PathsTable.COLUMN_LOCAL)));
            tFile.setRemoteAbsolutePath(cursor.getString(cursor
                    .getColumnIndexOrThrow(PathsTable.COLUMN_REMOTE)));
            tFiles.add(tFile);
            // always close the cursor
            cursor.close();
        }
        return tFiles;
    }

    private void makeToast(String message) {
        if (mContext == null || message.length() < 1) return;
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    private void createPath() {
        Intent i = new Intent(mContext, SyncListEditActivity.class);
        startActivity(i);
    }

    // Opens the second activity if an entry is clicked
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent i = new Intent(mContext, SyncListEditActivity.class);
        Uri pathUri = Uri.parse(PathsProvider.CONTENT_URI + "/" + id);
        i.putExtra(PathsProvider.CONTENT_ITEM_TYPE, pathUri);
        startActivity(i);
        // Reset Icon that represent status
        if (v != null) {
            ImageView imageView = (ImageView) v.findViewById(R.id.image_icon);
            imageView.setImageResource(R.drawable.ic_cloud_queue_black_48dp);
        }
    }

    private void fillData() {
        // Fields from the database (projection)
        // Must include the _id column for the adapter to work
        String[] from = new String[] { PathsTable.COLUMN_LOCAL, PathsTable.COLUMN_REMOTE };
        // Fields on the UI to which we map
        int[] to = new int[] { R.id.text_pathLocal, R.id.text_pathRemote};

        getActivity().getLoaderManager().destroyLoader(LOADER_ID);
        getActivity().getLoaderManager().initLoader(LOADER_ID, null, this);
        adapter = new SimpleCursorAdapter(mContext, R.layout.list_row_path, null, from, to, 0);
        setListAdapter(adapter);

        if (mListView != null)
        getActivity().findViewById(R.id.view_path_list_layout)
                .findViewById(android.R.id.list)
                .setVisibility((mListView != null && mListView.getCount() > 0) ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, EDIT_ID, 0, R.string.action_edit);
        menu.add(0, DELETE_ID, 0, R.string.action_delete);
        //menu.add(0, MANUAL_SYNC_ID, 0, R.string.button_sync);
        // TODO: bug, force transfers not working per entry; overview log not update after completed
        //menu.add(0, MANUAL_UPLOAD_ID, 0, R.string.button_upload);
        //menu.add(0, MANUAL_DOWNLOAD_ID, 0, R.string.button_download);
        //menu.add(0, MANUAL_REMOVE_REMOTE_ID, 0, R.string.button_remove_remote);
        //menu.add(0, MANUAL_DELETE_LOCAL_ID, 0, R.string.button_delete_local);
    }

    // creates a new loader after the initLoader () call
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = { PathsTable.COLUMN_ID, PathsTable.COLUMN_LOCAL, PathsTable.COLUMN_REMOTE };
        CursorLoader cursorLoader = new CursorLoader(mContext,
                PathsProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() != LOADER_ID) return;
        adapter.swapCursor(data);
        if (mListView != null && mListIndex > 0 && mListTop > 0) {
            // restore the position of listview
            mListView.setSelectionFromTop(mListIndex, mListTop);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() != LOADER_ID) return;
        if (mListView != null) {
            // save index and top position
            mListIndex = mListView.getFirstVisiblePosition();
            View v = mListView.getChildAt(0);
            mListTop = (v == null) ? 0 : v.getTop();
        }
        // data is not available anymore, delete reference
        adapter.swapCursor(null);
    }

    @Override
    public void onRefresh() {
        getActivity().getLoaderManager().restartLoader(LOADER_ID, null, this);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        if (rowStatusCheck != null) {
            rowStatusCheck.cancel(true);
            rowStatusCheck = null;
        }
        rowStatusCheck = new RowStatusCheck();
        rowStatusCheck.execute();
    }

    private void commandAction(ArrayList<FileTransferHelper> tFiles, String action) {
        if (tFiles == null || action == null || action.isEmpty()) {
            Log.d(TAG, "somethings wrong with commandAction");
        } else {
            Intent i = new Intent(mContext, ForegroundService.class);
            i.setAction(action);
            i.putExtra(Constants.TRIGGER.IS, Constants.TRIGGER.MANUAL);
            i.putParcelableArrayListExtra(Constants.TRANSFER.FILES, tFiles);
            mContext.startService(i);
        }
    }

    protected class RowStatusHelper {
        private View view;
        private String status;

        public RowStatusHelper(View aView, String aStatus) {
            this.view = aView;
            this.status = aStatus;
        }

        public View getView() {
            return this.view;
        }

        public String getStatus() {
            return this.status;
        }
    }

    private class RowStatusCheck extends AsyncTask<Void, RowStatusHelper, String> {

        FtpsUtils mFtpsUtils = null;

        @Override
        protected String doInBackground(Void... params) {
            if (mListView == null) return null;
            String status = null;
            if (mFtpsUtils == null || !mFtpsUtils.isConnected()) {
                PreferencesHelper ftpsSettings = new PreferencesHelper().getFtpsSettings(getActivity());
                mFtpsUtils = new FtpsUtils(ftpsSettings);
                mFtpsUtils.setup(getActivity().getApplicationContext());
            }
            if (mFtpsUtils.isConnected()) {
                for (int i = 0, j = mListView.getCount(); i <= j; i++) {
                    View v = mListView.getChildAt(i);
                    if (v != null) {
                        TextView local_textView = (TextView) v.findViewById(R.id.text_pathLocal);
                        TextView remote_textView = (TextView) v.findViewById(R.id.text_pathRemote);
                        String localPath = local_textView.getText().toString();
                        String remotePath = remote_textView.getText().toString();
                        status = "false";
                        File localFile = new File(localPath);
                        if (localFile.exists() &&
                                localFile.canRead() && localFile.canWrite() &&
                                mFtpsUtils.hasAccess(remotePath)) {
                            status = "true";
                        }
                        publishProgress(new RowStatusHelper(v, status));
                    }
                }
            } else {
                status = "false-connection";
            }
            return status;
        }

        @Override
        protected void onProgressUpdate(RowStatusHelper... values) {
            RowStatusHelper row = values[0];
            View v = row.getView();
            String status = row.getStatus();
            if (v != null) {
                ImageView imageView = (ImageView) v.findViewById(R.id.image_icon);
                if (status.equals("true")) {
                    imageView.setImageResource(R.drawable.ic_cloud_done_black_48dp);
                } else if (status.equals("false")) {
                    imageView.setImageResource(R.drawable.ic_cloud_off_black_48dp);
                } else {
                    imageView.setImageResource(R.drawable.ic_cloud_queue_black_48dp);
                }
            }
        }

        @Override
        protected void onPreExecute() {
            if (mListView == null || mListView.getCount() < 1) {
                this.cancel(true);
                return;
            }
            if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    // Make sure it is called after the SwipeRefreshLayout.onMeasure() to show indicator
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
            for (int i = 0, j = mListView.getCount(); i <= j; i++) {
                // Reset every icons
                View v = mListView.getChildAt(i);
                if (v != null) {
                    ImageView imageView = (ImageView) v.findViewById(R.id.image_icon);
                    imageView.setImageResource(R.drawable.ic_cloud_queue_black_48dp);
                }
            }
            // Show message via snack bar
            SnackbarManager.show(
                    Snackbar.with(getActivity())
                            .text(R.string.sync_directory_check)
                            .swipeToDismiss(false)
                            .duration((long) 10*1000) // 10 seconds
                    , (android.view.ViewGroup) getActivity().findViewById(R.id.view_path_list_layout));
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                int message = 0;
                Snackbar.SnackbarDuration duration = Snackbar.SnackbarDuration.LENGTH_INDEFINITE;
                if (result.equals("false-connection")) {
                    message = R.string.ftps_connection_fail;
                } else if (result.equals("false")) {
                    message = R.string.sync_directory_not_found;
                }
                if (message > 0) {
                    // Show message via snack bar
                    SnackbarManager.show(
                            Snackbar.with(getActivity())
                                    .text(message)
                                    .type(SnackbarType.MULTI_LINE)
                                    .swipeToDismiss(false)
                                    .duration(duration)
                            , (android.view.ViewGroup) getActivity().findViewById(R.id.view_path_list_layout));
                }
            }
            if (mSwipeRefreshLayout != null) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }
}