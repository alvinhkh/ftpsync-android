package com.alvinhkh.ftpsync.view.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.preference.PreferencesActivity;
import com.alvinhkh.ftpsync.sync.ForegroundService;
import com.alvinhkh.ftpsync.R;
import com.alvinhkh.ftpsync.sync.ProgressHelper;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class MainFragment extends Fragment implements OnClickListener {

    private static final String TAG = "MainFragment";

    private Context mContext = super.getActivity();

    private Button button_sync;
    private Button button_upload;
    private Button button_download;
    private Button button_deleteLocal;
    private Button button_removeRemote;
    private Button button_preferences;

    private TextView textView_service;
    private TextView textView_connectivity;
    private ProgressBar progressBar;
    private TextView textView_status;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = super.getActivity();
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        // get widgets
        button_sync = (Button)view.findViewById(R.id.button_sync);
        button_upload = (Button)view.findViewById(R.id.button_upload);
        button_download = (Button)view.findViewById(R.id.button_download);
        button_deleteLocal = (Button)view.findViewById(R.id.button_deleteLocal);
        button_removeRemote = (Button)view.findViewById(R.id.button_removeRemote);
        button_preferences = (Button)view.findViewById(R.id.button_preferences);
        textView_service = (TextView)view.findViewById(R.id.textview_service);
        textView_connectivity = (TextView)view.findViewById(R.id.textview_connectivity);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        textView_status = (TextView)view.findViewById(R.id.textview_status);
        // set listeners
        button_sync.setOnClickListener(this);
        button_upload.setOnClickListener(this);
        button_download.setOnClickListener(this);
        button_deleteLocal.setOnClickListener(this);
        button_removeRemote.setOnClickListener(this);
        button_preferences.setOnClickListener(this);
        // Get Network State
        Intent in = new Intent();
        in.setAction(Constants.ACTION.GET_NETWORK_STATE);
        mContext.sendBroadcast(in);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void init() {
        //
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(progressReceiver, new IntentFilter(Constants.ACTION.TRANSFER_PROGRESS));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(networkStateReceiver, new IntentFilter(Constants.ACTION.NETWORK_STATE));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(serviceStateReceiver, new IntentFilter(Constants.ACTION.FOREGROUND_STATE));
        //
        getServiceStatus();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        getActivity().getActionBar().show();
        init();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(progressReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(networkStateReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(serviceStateReceiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_sync:
                Intent intentSync = new Intent(mContext, ForegroundService.class);
                intentSync.setAction(Constants.ACTION.SYNC)
                        .putExtra(Constants.TRIGGER.IS, Constants.TRIGGER.BUTTON);
                mContext.startService(intentSync);
                break;
            case R.id.button_upload:
                Intent intentUpload = new Intent(mContext, ForegroundService.class);
                intentUpload.setAction(Constants.ACTION.UPLOAD);
                mContext.startService(intentUpload);
                break;
            case R.id.button_download:
                Intent intentDownload = new Intent(mContext, ForegroundService.class);
                intentDownload.setAction(Constants.ACTION.DOWNLOAD);
                mContext.startService(intentDownload);
                break;
            case R.id.button_deleteLocal:
                Intent intentDeleteLocal = new Intent(mContext, ForegroundService.class);
                intentDeleteLocal.setAction(Constants.ACTION.DELETE_LOCAL);
                mContext.startService(intentDeleteLocal);
                break;
            case R.id.button_removeRemote:
                Intent intentRemoveRemote = new Intent(mContext, ForegroundService.class);
                intentRemoveRemote.setAction(Constants.ACTION.REMOVE_REMOTE);
                mContext.startService(intentRemoveRemote);
                break;
            case R.id.button_preferences:
                Intent intent = new Intent(mContext, PreferencesActivity.class);
                mContext.startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Log.e(TAG, "resultCode error!");
            return;
        }
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void getServiceStatus() {
        // Check foreground service
        textView_service.setText("Is Foreground Service running: " + isServiceRunning(ForegroundService.class));
    }

    public void setStatus(int res) {
        setStatus(getString(res));
    }
    public void setStatus(String message) {
        if (textView_status != null) {
            String pattern = android.text.format.DateFormat.getBestDateTimePattern(Locale.getDefault(), "yyyy-MM-dd HH:mm:ss");
            String currentTime = new SimpleDateFormat(pattern).format(System.currentTimeMillis());
            textView_status.setText(message + "\n" + currentTime);
        }
    }

    private BroadcastReceiver serviceStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.FOREGROUND_STATE)) {
                getServiceStatus();
            }
        }
    };

    private BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.NETWORK_STATE)) {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    if (!bundle.getString(Constants.ACTION.NETWORK_STATE, "").equals("")) {
                        String setText = "Please make sure, your network connection is ON";
                        switch (bundle.getString(Constants.ACTION.NETWORK_STATE)) {
                            case Constants.NET.WIFI:
                                setText = "Network: WiFi";
                                break;
                            case Constants.NET.MOBILE:
                                setText = "Network: Mobile";
                                break;
                            case Constants.NET.ETHERNET:
                                setText = "Network: Ethernet";
                                break;
                            case Constants.NET.UNKNOWN:
                            default:
                                setText = "Network: Unknown";
                                break;
                        }
                        textView_connectivity.setText(setText);
                    }
                }
            }
        }
    };

    private BroadcastReceiver progressReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.TRANSFER_PROGRESS)) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    // Indeterminate Progress
                    Boolean indeterminate = extras.getBoolean(Constants.TRANSFER.PROGRESS_INDETERMINATE);
                    if (indeterminate != null) {
                        if (progressBar != null) {
                            progressBar.setIndeterminate(indeterminate);
                            progressBar.setMax(0);
                            progressBar.setProgress(0);
                        }
                    }
                    // Message by string id
                    Integer messageId = extras.getInt(Constants.TRANSFER.MESSAGE_ID, 0);
                    if (messageId > 0) {
                        setStatus(getString(messageId));
                    }
                    // Message by text
                    String message = extras.getString(Constants.TRANSFER.MESSAGE, "");
                    if (!message.equals("")) {
                        setStatus(message);
                    }
                    ProgressHelper progressData = extras.getParcelable(Constants.TRANSFER.PROGRESS);
                    ProgressHelper secondProgressData = extras.getParcelable(Constants.TRANSFER.SECOND_PROGRESS);
                    Integer plusMax = extras.getInt(Constants.TRANSFER.PROGRESS_MAX, -1);
                    if (extras.getBoolean(Constants.TRANSFER.PROGRESS_DONE, false) == true) {
                        if (progressBar != null) {
                            progressBar.setIndeterminate(false);
                            progressBar.setMax(0);
                            progressBar.setProgress(0);
                        }
                    } else if (extras.getBoolean(Constants.TRANSFER.PROGRESS_PLUSONE, false) == true) {
                        if (progressBar != null) {
                            progressBar.setIndeterminate(false);
                            progressBar.setProgress(progressBar.getProgress() + 1);
                        }
                    } else if (plusMax > 0) {
                        if (progressBar != null) {
                            progressBar.setIndeterminate(false);
                            progressBar.setMax(progressBar.getMax() + plusMax);
                        }
                    } else if (secondProgressData != null) {
                        if (progressBar != null) {
                            progressBar.setIndeterminate(secondProgressData.getIndeterminate());
                            progressBar.setSecondaryProgress(secondProgressData.getProgress());
                        }
                        setStatus(secondProgressData.getMessage());
                    } else if (progressData != null) {
                        if (progressBar != null) {
                            progressBar.setIndeterminate(progressData.getIndeterminate());
                            progressBar.setMax(progressData.getMaxProgress());
                            progressBar.setProgress(progressData.getProgress());
                        }
                        setStatus(progressData.getMessage());
                    }
                    if (extras.getBoolean(Constants.TRANSFER.RETURN_STATUS, true) == false) {
                        setStatus(R.string.some_fail_to_progress);
                    } else if (extras.getBoolean(Constants.TRANSFER.RETURN_FILE_NOT_FOUND, false) == true) {
                        setStatus(R.string.some_file_not_found);
                    } else if (extras.getBoolean(Constants.TRANSFER.RETURN_CONNECTION_CLOSED, false) == true) {
                        setStatus(R.string.some_connection_lost);
                    } else if (extras.getBoolean(Constants.TRANSFER.RETURN_STATUS) == true) {
                        setStatus(R.string.complete);
                    }
                }
            }
        }
    };

}
