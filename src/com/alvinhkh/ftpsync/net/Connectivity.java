package com.alvinhkh.ftpsync.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.alvinhkh.ftpsync.Constants;

public class Connectivity {

    NetworkInfo mWifi;
    NetworkInfo mMobile;
    NetworkInfo mEthernet;
    Boolean isInWifi;
    Boolean isInMobile;
    Boolean isInEthernet;

    public Connectivity(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mMobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        mEthernet = manager.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET);
        isInWifi = (mWifi != null && mWifi.isConnected());
        isInMobile = (mMobile != null && mMobile.isConnected());
        isInEthernet = (mEthernet != null && mEthernet.isConnected());
    }

    public String getState() {
        if (isInWifi) {
            return Constants.NET.WIFI;
        } else if (isInMobile) {
            return Constants.NET.MOBILE;
        } else if (isInEthernet) {
            return Constants.NET.ETHERNET;
        }
        return Constants.NET.UNKNOWN;
    }

    public Boolean isInWifi() {
        return isInWifi;
    }

    public Boolean isInMobile() {
        return isInMobile;
    }

    public Boolean isInEthernet() {
        return isInEthernet;
    }

    public Boolean isInWifiOrEthernet() {
        return isInWifi || isInEthernet;
    }

}
