package com.alvinhkh.ftpsync.net;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.file.FileListHelper;
import com.alvinhkh.ftpsync.file.FileUtils;
import com.alvinhkh.ftpsync.preference.PreferencesHelper;
import com.alvinhkh.ftpsync.sync.FileTransferHelper;
import com.alvinhkh.ftpsync.sync.ProgressHelper;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;
import org.apache.commons.net.io.CopyStreamEvent;
import org.apache.commons.net.io.Util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

public class FtpsUtils
{
    private static String TAG = "FtpsUtils";

    public FTPSClient ftps = null;
    private String host;
    private Integer port;
    private String protocol = "TLS"; // SSL/TLS
    private boolean isImplicit = false; // security mode (Implicit/Explicit)
    private String username;
    private String password;
    private CopyStreamAdapter streamListener;
    private int timeout = 10000; // make sure don't hang on invalid connection attempts
    private static String ENCODING = "UTF-8";
    private Integer clock_offset = 0;

    private Context mContext = null;

    /**
     * Constructor
     * @param prefsHelper
     */
    public FtpsUtils(PreferencesHelper prefsHelper)
    {
        this(prefsHelper, false, 10000);
    }
    public FtpsUtils(PreferencesHelper prefsHelper, int timeout)
    {
        this(prefsHelper, false, timeout);
    }
    /**
     * Constructor
     * @param prefsHelper
     * @param isImplicit
     * @param timeout
     */
    public FtpsUtils(PreferencesHelper prefsHelper, Boolean isImplicit, int timeout)
    {
        super();
        this.host = prefsHelper.getFtpsHost();
        this.port = prefsHelper.getFtpsPort();
        this.protocol = prefsHelper.getFtpsProtocol();
        this.username = prefsHelper.getUsername();
        this.password = prefsHelper.getPassword();
        this.isImplicit = isImplicit;
        this.timeout = timeout;
        ftps = new FTPSClient(this.isImplicit);
        try {
            connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setup parent Context
     * @param mContext Context
     */
    public void setup(Context mContext)
    {
        this.mContext = mContext;
    }


    public FTPSClient getClient()
    {
        return ftps;
    }

    /**
     * Initiate a connection to the ftp server
     * @throws IOException
     */
    public void connect() throws IOException
    {
        if (checkConnectionWithOneRetry()) {
            ftps.setFileType(FTP.BINARY_FILE_TYPE);
            // Use passive mode as default because most of us are behind firewalls.
            ftps.enterLocalPassiveMode();

            Integer reply = ftps.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftps.disconnect();
                throw new IOException("Invalid login credentials");
            }
        } else {
            throw new IOException("Fail to connect server");
        }
    }

    protected Boolean reconnect()
    {
        try {
            ftps = new FTPSClient(protocol, isImplicit);
            // outputs all conversation to the console, should comment out after debug.
            //ftps.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
            FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
            conf.setServerLanguageCode(Locale.getDefault().toString());
            //conf.setDefaultDateFormat("d MMM yyyy");
            //conf.setRecentDateFormat("d MMM HH:mm");
            //conf.setServerTimeZoneId(TimeZone.getDefault().toString());
            ftps.configure(conf);
            ftps.setControlEncoding(ENCODING);
            //ftps.setTcpNoDelay(true);
            //ftps.setBufferSize(1024);

            ftps.setConnectTimeout(timeout);
            ftps.connect(host, port);
            Log.d(TAG, "Connected to " + host + ":" + port + "");
            ftps.login(username, password);
            ftps.setKeepAlive(true);

            Log.d(TAG, "getServerLanguageCode: " + conf.getServerLanguageCode());
            Log.d(TAG, "getServerTimeZoneId: " + conf.getServerTimeZoneId());

            // After connection attempt, you should check the reply code to verify success.
            Integer reply = ftps.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply))
            {
                setSecondProgress(new ProgressHelper("Fail to server...", 0));
                ftps.disconnect();
                return false;
            }
            setSecondProgress(new ProgressHelper("Connected to " + host + ":" + port + "", 0));
            return true;
        } catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isConnected() {
        try {
            return ftps.sendNoOp();
        } catch (IOException e) {
            return false;
        }
    }

    private boolean checkConnectionWithOneRetry()
    {
        try
        {
            // Sends a NOOP command to the FTP server.
            boolean answer = ftps.sendNoOp();
            if (answer)
                return true;
            else
            {
                Log.d(TAG, "Server connection failed!");
                boolean success = reconnect();
                if (success)
                {
                    Log.d(TAG, "Reconnect attempt have succeeded!");
                    return true;
                }
                else
                {
                    Log.d(TAG, "Reconnect attempt failed!");
                    return false;
                }
            }
        }
        catch (FTPConnectionClosedException e)
        {
            Log.d(TAG, "Server connection is closed!");
            boolean recon = reconnect();
            if (recon)
            {
                Log.d(TAG, "Reconnect attempt have succeeded!");
                return true;
            }
            else
            {
                Log.d(TAG, "Reconnect attempt have failed!");
                return false;
            }

        }
        catch (IOException e)
        {
            Log.d(TAG, "Server connection failed!");
            boolean recon = reconnect();
            if (recon)
            {
                Log.d(TAG, "Reconnect attempt have succeeded!");
                return true;
            }
            else
            {
                Log.d(TAG, "Reconnect attempt have failed!");
                return false;
            }
        }
        catch (NullPointerException e)
        {
            Log.d(TAG, "Server connection is closed!");
            boolean recon = reconnect();
            if (recon)
            {
                Log.d(TAG, "Reconnect attempt have succeeded!");
                return true;
            }
            else
            {
                Log.d(TAG, "Reconnect attempt have failed!");
                return false;
            }
        }
    }

    /**
     * Disconnect the ftp client
     * @throws IOException
     */
    public void disconnect() throws IOException
    {
        ftps.logout();
        ftps.disconnect();
    }

    /**
     * Compare server time and local time
     * @param remotePath
     */
    public Integer clockSync(String remotePath) {
        return clockSync(remotePath, 0);
    }
    public Integer clockSync(String remotePath, int count)
    {
        if (ftps == null) return 0;
        File remotePathFile = new File(remotePath);
        String remoteDirectory = remotePath;
        try {
            if (isFile(remotePath)) {
                remoteDirectory = remotePathFile.getParent();
            }
        } catch (IOException e) {

        }

        Integer sync_off = null;
        Long localTime1 = 0L;
        Long remoteTime = 0L;
        Long localTime2 = 0L;
        Boolean error = false;
        try
        {
            String fileName = ".clocksync";
            File outputDir = mContext.getCacheDir(); // context being the Activity pointer
            File localFile = new File(outputDir, fileName);
            String remoteFilePath = remoteDirectory + "/" + fileName;

            Log.d(TAG, "clocksync " + localFile.getPath() + " .. " + remoteFilePath);

            ftps.removeDirectory(remoteFilePath);
            ftps.deleteFile(remoteFilePath);

            FileUtils.write(localFile, "temporary file created by ftpsync");
            InputStream input = new FileInputStream(localFile);
            ftps.storeFile(remoteFilePath, input);
            localTime1 = new Date().getTime();
            remoteTime = getLastModified(remoteFilePath).getTime();
            localTime2 = new Date().getTime();

            ftps.deleteFile(remoteFilePath);
            localFile.delete();
        }
        catch (FileNotFoundException e) {
            //e.printStackTrace();
            error = true;
            if (count < 2) {
                return clockSync(new File(remotePath).getParent(), count + 1);
            }
        }
        catch (IOException e)
        {
            //e.printStackTrace();
            error = true;
        }
        finally {
            Log.d(TAG, new Date(localTime1).toString() + " " + localTime1);
            Log.d(TAG, new Date(remoteTime).toString() + " " + remoteTime);
            Log.d(TAG, new Date(localTime2).toString() + " " + localTime2);

            if (localTime2 < remoteTime) { // remote is in future
                sync_off = (int) (remoteTime - localTime2);
            } else if (localTime1 > remoteTime) { // remote is in past or equal
                sync_off = (int) (remoteTime - localTime1);
            } else {
                sync_off = 0;
            }

            if (error == true) sync_off = 0;
            if (localTime1 == 0 || remoteTime == 0 || localTime2 == 0) {
                sync_off = 0;
            }
            if (Math.abs(sync_off) < 5 * 1000) {
                // Ignore less than 5 seconds
                sync_off = 0;
            }
            sync_off = (int) Math.ceil(sync_off / 1800000) * 1800000;
        }

        Log.d(TAG, "clock offset " + sync_off);
        return sync_off;
    }

    /**
     * Get a list of file in a given directory in form of list of FTPFileHelper
     * @return List of files/directories
     * @throws IOException
     */
    public List<FTPFileHelper> getFTPFileList(String path) {
        try {
            connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getFTPFileList(new ArrayList<FTPFileHelper>(),
                "",
                path,
                path);
    }
    public List<FTPFileHelper> getFTPFileList(String path, String root) {
        try {
            connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getFTPFileList(new ArrayList<FTPFileHelper>(),
                "",
                path,
                root);
    }
    public List<FTPFileHelper> getFTPFileList(List<FTPFileHelper> returnFilesList,
                                              String remoteDirectory,
                                              String remoteParentDirectory,
                                              final String remoteRootDirectory)
    {
        try {
            String remoteDirectoryList = remoteParentDirectory;
            if (!remoteDirectory.equals("")) {
                remoteDirectoryList += "/" + remoteDirectory;
            }
            FTPFile[] subFiles = ftps.listFiles(remoteDirectoryList);

            Integer reply = ftps.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply) || subFiles == null) {
                return null;
            }

            if (subFiles != null && subFiles.length > 0)
            {
                for (FTPFile f : subFiles)
                {
                    String currentFileName = f.getName();

                    if (f.isDirectory())
                    {
                        String filePath = remoteDirectoryList + "/" + currentFileName;
                        FTPFileHelper file = new FTPFileHelper();
                        file.setPath(filePath);
                        file.setIsDirectory(true);
                        //returnFilesList.add(file);
                        getFTPFileList(returnFilesList, currentFileName, remoteDirectoryList, remoteRootDirectory);
                        //Log.d(TAG, remoteDirectoryList + " " + f.getTimestamp().getTime());
                    }
                    else
                    {
                        String filePath = remoteDirectoryList + "/" + currentFileName;
                        if (isFile(remoteDirectoryList)) {
                            filePath = remoteDirectoryList;
                        }
                        Date lastModified = getLastModified(filePath);
                        Long fileSize = f.getSize();
                        FTPFileHelper file = new FTPFileHelper();
                        file.setPath(filePath);
                        //Log.d(TAG, "filePath: " + filePath);
                        //Log.d(TAG, "short: " + FileUtils.getShortPath(filePath, remoteRootDirectory));
                        //Log.d(TAG, "directory: " + remoteDirectoryList);
                        //Log.d(TAG, "root: " + remoteRootDirectory);
                        file.setLastModified(lastModified);
                        file.setSize(fileSize);
                        file.setIsFile(f.isFile());
                        returnFilesList.add(file);
                    }
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return returnFilesList;
    }

    /**
     * Get info about a file in form of FTPFileHelper
     * @return List of files/directories
     * @throws IOException
     */
    public FTPFileHelper getFTPFile(String path) {
        try {
            connect();
            FTPFileHelper file = null;
            FTPFile[] fs = ftps.listFiles(path);
            Integer reply = ftps.getReplyCode();
            if (fs != null) {
                if (!FTPReply.isPositiveCompletion(reply)) {
                    return null;
                }
                for (FTPFile f : fs) {
                    Log.d(TAG, "getFTPFile: " + path);
                    file = new FTPFileHelper();
                    file.setPath(path);
                    if (!f.isFile()) {
                        Long fileSize = f.getSize();
                        file.setSize(fileSize);
                        Date lastModified = getLastModified(path);
                        file.setLastModified(lastModified);
                    }
                    file.setIsFile(f.isFile());
                    file.setIsDirectory(f.isDirectory());
                }
            }
            return file;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Get a list of file in a given directory in form of FileListHelper
     * @return List of files/directories
     */
    public List<FileListHelper> getFileList(String rootPath)
    {
        List<FileListHelper> remoteList = new ArrayList<FileListHelper>();
        List<FTPFileHelper> files = getFTPFileList(rootPath);
        for (FTPFileHelper file : files) {
            String filePath = file.getPath();
            filePath = filePath.replaceFirst(Pattern.quote(rootPath), "");
            String name = new File(filePath).getName();
            String[] tokens = name.split("\\.(?=[^\\.]+$)");
            //(tokens.length > 1 && tokens[1].startsWith(".backup"))
            if (name.startsWith(".") || name.startsWith("~")) {
                if (name.startsWith(".clocksync")) {
                    try
                    {
                        Log.d(TAG, "delete .clocksync in remote directory");
                        ftps.deleteFile(filePath);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                continue;
            }
            FileListHelper fileListHelper = new FileListHelper();
            fileListHelper.setPath(filePath);
            fileListHelper.setLastModified(file.lastModified());
            fileListHelper.setIsDirectory(file.getIsDirectory());
            fileListHelper.setIsFile(file.getIsFile());
            remoteList.add(fileListHelper);
            //Log.d(TAG, "Remote File: " + filePath + " Last Modified: " + file.lastModified() + " Size: " + file.getSize());
        }
        return remoteList;
    }

    public void deleteFile(String path) throws IOException {
        ftps.deleteFile(path);
    }


    /**
     * Get a list of directories in a given directory for admin
     * @return List of files/directories
     *
    public List<String> getDirectoryList(String path)
    {
        List<String> returnList = new ArrayList<String>();
        FTPFile[] files = null;
        try
        {
            connect();
            files = ftps.listDirectories(path);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (FTPFile file : files) {
                String details = file.getName();
                if (file.isDirectory()) {
                    details = "[" + details + "]";
                    returnList.add(file.getName());
                }
                details += "\t\t" + file.getSize();
                details += "\t\t" + dateFormat.format(file.getTimestamp().getTime());

                //Log.d(TAG, details);
            }
        }
        catch (FTPConnectionClosedException e)
        {
            System.err.println("Server closed connection.");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return returnList;
    }*/

    /**
     * Determines whether a file exists or not
     * @param filePath
     * @return true if exists, false otherwise
     * @throws IOException thrown if any I/O error occurred.
     */
    public Boolean isFile(String filePath) throws IOException
    {
        connect();
        FTPFile f = ftps.mlistFile(filePath);
        if (f != null) return f.isFile();
        return false;
    }
    /**
     * Determines whether a directory exists or not
     * @param filePath
     * @return true if exists, false otherwise
     * @throws IOException thrown if any I/O error occurred.
     */
    public Boolean isDirectory(String filePath) throws IOException
    {
        connect();
        FTPFile f = ftps.mlistFile(filePath);
        if (f != null) return f.isDirectory();
        return false;
    }

    /**
     * Determines whether a file or directory exists or not
     * @param filePath
     * @return true if exists, false otherwise
     * @throws IOException thrown if any I/O error occurred.
     */
    public Boolean isExist(String filePath) throws IOException
    {
        connect();
        try {
            FTPFile f = ftps.mlistFile(filePath);
            return (f != null);
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }

    /**
     * Change working FTPS directory
     * @return command reply code
     * @throws IOException thrown if any I/O error occurred.
     *
    private Integer changeDirectory(String directory) throws IOException
    {
        ftps.changeWorkingDirectory(directory);
        Log.d(TAG, "Change Directory -->" + directory);

        Integer reply = ftps.getReplyCode();
        switch (reply)
        {
            case 550:
                // no such directory
                Log.d(TAG, "No such directory.");
                break;
            case 250:
                // command successful
                break;
        }
        return reply;
    }*/

    private Date getLastModified(String filePath) throws IOException
    {
        String time = null;
        Date lastModified = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        try
        {
            time = ftps.getModificationTime(filePath);
            if (time != null)
            {
                lastModified = dateFormat.parse(time.substring(time.indexOf(" ") + 1));
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (lastModified != null)
            {
                lastModified = new Date(lastModified.getTime());
            }
            //Log.d(TAG, "getLastModified: " + lastModified + " path: " + filePath);
            if (ftps.getReplyCode() == 550)
            {
                if (isDirectory(filePath)) return null;
                throw new IOException("File unavailable: " + filePath);
            }
        }
        /*
        FTPFile file = null;
        try {
            file = ftps.mlistFile(filePath);
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (file != null)
            {
                lastModified = file.getTimestamp().getTime();
            }
            if (ftps.getReplyCode() == 550)
            {
                throw new IOException("File unavailable");
            }
        }*/
        return lastModified;
    }

    private Integer setLastModified(String filePath, Date fileDate)
    {
        try {
            Date date = new Date(fileDate.getTime() + this.clock_offset);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
            String formattedDateString = formatter.format(date);
            ftps.setModificationTime(filePath, formattedDateString);

            Log.d(TAG, "Set last modified date " + formattedDateString);
            Log.d(TAG, "Get last modified date " + ftps.getModificationTime(filePath));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return ftps.getReplyCode();
    }

    private Boolean checkSameFile(String localFilePath, String remoteFilePath) throws IOException, NullPointerException
    {
        File localFile = new File(localFilePath);
        if (!localFile.exists()) return false;
        if (!isFile(remoteFilePath)) return false;

        Date localFileLastModified = new Date(localFile.lastModified());
        Long localFileSize = localFile.length();
        Log.d(TAG, "Local File Modification Time : " + localFileLastModified + " (" + localFilePath + ")");
        Log.d(TAG, "Local File Size : " + localFileSize);

        FTPFile remoteFile = ftps.mlistFile(remoteFilePath);
        Long remoteModTime = remoteFile.getTimestamp().getTimeInMillis();
        Date remoteFileLastModified = new Date(remoteModTime);
        Long remoteFileSize = remoteFile.getSize();
        Log.d(TAG, "Remote File Modification Time : " + remoteFileLastModified + " (" + remoteFilePath + ")");
        Log.d(TAG, "Remote File Size : " + remoteFileSize);

        return (
                localFileLastModified.equals(remoteFileLastModified) && // check last modified
                localFileSize.equals(remoteFileSize) // check file size
        );
    }

    private Long getFileSize(String filePath) throws IOException
    {
        FTPFile file = null;
        Long fileSize = null;
        try {
            file = ftps.mlistFile(filePath);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        } finally {
            if (file != null)
            {
                fileSize = file.getSize();
            }
            if (ftps.getReplyCode() == 550)
            {
                throw new IOException("File unavailable");
            }
        }
        return fileSize;
    }

    /**
     * Upload single file
     * @param tFile transfer file data container
     * @return upload file data
     */
    public FileTransferHelper uploadSingleFile(FileTransferHelper tFile)
    {
        return uploadSingleFile(tFile, true);
    }
    public FileTransferHelper uploadSingleFile(FileTransferHelper tFile,
                                             Boolean isSkippable)
    {
        tFile.setAction(Constants.ACTION.UPLOAD);
        String fromFilePath = tFile.getLocalPath();
        String toFilePath = tFile.getRemotePath();

        this.clock_offset = clockSync(toFilePath);

        //Log.d(TAG, "from -> " + fromFilePath);
        //Log.d(TAG, "to -> " + toFilePath);

        Boolean status = false;
        Boolean skip = false;
        String timeFormat = "%tY%<tm%<td%<tH%<tM%<tL";
        String timeNow = "";

        InputStream inputStream = null;
        OutputStream outputStream = null;
        try
        {
            connect();

            final File uploadFile = new File(fromFilePath);
            final String uploadFileName = uploadFile.getName();
            Date localFileLastModified = new Date(uploadFile.lastModified());

            if (!uploadFile.exists()) throw new FileNotFoundException();
            if (uploadFile.isDirectory()) {
                setSecondProgress(new ProgressHelper("Creating " + fromFilePath, 0));
                boolean created = makeDirectory(fromFilePath);
                if (created) {
                    Log.d(TAG, "Created the directory: " + fromFilePath);
                }
                tFile.setStatus(true);
                return tFile;
            }
            if (!uploadFile.isFile()) throw new IOException("Not a file");

            boolean isDirectory = isDirectory(toFilePath);
            if (isDirectory == true) {
                toFilePath = tFile.getRemotePath() + "/" + uploadFileName;
                Log.d(TAG, "Uploading a file to a directory - " + toFilePath);
            }
            String storeFileName = toFilePath;
            boolean fileExist = isFile(toFilePath);
            timeNow = String.format(timeFormat, new Date());

            setSecondProgress(new ProgressHelper("Uploading " + uploadFileName + " ...", 0));

            if (fileExist)
            {
                if (isSkippable == true && checkSameFile(fromFilePath, toFilePath))
                {
                    // Check if there are the same file.
                    skip = true;
                    tFile.setSkip(skip);
                    status = true;
                    Log.d(TAG, "File has the same attributes, skip this file.");
                }
                else
                {
                    storeFileName = storeFileName + "-" + timeNow;
                    Log.d(TAG, "File exist in server, upload as " + storeFileName);
                }
            }

            if (!skip)
            {
                ftps.setBufferSize(1024 * 2); // Set Buffer Size
                inputStream = new FileInputStream(uploadFile);
                outputStream = ftps.storeFileStream(storeFileName);
                Util.copyStream(inputStream, outputStream,
                        ftps.getBufferSize(),
                        CopyStreamEvent.UNKNOWN_STREAM_SIZE,
                        new CopyStreamAdapter() {
                            private int pp = 0;

                            @Override
                            public void bytesTransferred(long totalBytesTransferred,
                                                         int bytesTransferred, long streamSize) {
                                if (totalBytesTransferred == 0) return;
                                int percent = (int) ((float) totalBytesTransferred * 100 / (float) uploadFile.length());
                                if (percent >= pp + 4) {
                                    pp = percent;
                                    //Log.d(TAG, "Uploading " + uploadFileName + " ... " + percent + "%");
                                    setSecondProgress(new ProgressHelper("Uploading " + uploadFileName + " ... " + percent + "%", percent));
                                }
                            }
                        }, true);
                inputStream.close();
                outputStream.close();

                ftps.completePendingCommand();
                status = FTPReply.isPositiveCompletion(ftps.getReplyCode());

                if (fileExist && status)
                {
                    Log.d(TAG, "Rename file");
                    timeNow = String.format(timeFormat, new Date());
                    String backupFileName = toFilePath + ".backup-" + timeNow;
                    // Rename original file name to old
                    ftps.rename(toFilePath, backupFileName);
                    // Rename new file as original filename
                    ftps.rename(storeFileName, toFilePath);
                    // Delete old file
                    if (isFile(toFilePath) && isFile(backupFileName))
                    {
                        Log.d(TAG, "Delete backup file");
                        ftps.deleteFile(backupFileName);
                    }
                    status = FTPReply.isPositiveCompletion(ftps.getReplyCode());
                }

                // Edit last modified date of remote file
                if (true)
                {
                    Integer reply = setLastModified(toFilePath, localFileLastModified);
                    if (!FTPReply.isPositiveCompletion(reply)) {
                        throw new IOException("Unable to edit last modified date of remote file.");
                    }
                }
            } else {
                setSecondProgress(new ProgressHelper("Skipped " + uploadFileName, 100));
            }
        }
        catch (FileNotFoundException e)
        {
            status = false;
            tFile.setFileNotFound(true);
        }
        catch (FTPConnectionClosedException e)
        {
            status = false;
            tFile.setConnectionClosed(true);
            Log.e(TAG, "Server closed connection.");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            status = false;
            e.printStackTrace();
        }

        tFile.setStatus(status);

        setProgressPlusOne();

        return tFile;
    }

    /**
     * Upload a whole directory (including its nested sub directories and files)
     * to a FTPS server.
     *
     * @param returnFilesList files data container
     * @param localPath Path of the local directory being uploaded.
     * @param remotePath Path of the destination directory on the server.
     * @param remoteParentDirectory Path of the parent directory of the current directory on the
     *                              server (used by recursive calls).
     * @throws IOException
     */
    public List<FileTransferHelper> uploadDirectory(List<FileTransferHelper> returnFilesList,
                                                  String localPath,
                                                  String remotePath,
                                                  String remoteParentDirectory)
    {
        File localFile = new File(localPath);
        File[] subFiles = localFile.listFiles();
        if (subFiles != null && subFiles.length > 0) {
            if (remoteParentDirectory.equals(""))
            {
                setProgressPlusMax(subFiles.length);
            }
            for (File item : subFiles) {
                String remoteFilePath = remotePath + "/" + remoteParentDirectory
                        + "/" + item.getName();
                if (remoteParentDirectory.equals("")) {
                    remoteFilePath = remotePath + "/" + item.getName();
                }

                if (item.isFile()) {
                    // upload the file
                    String localFilePath = item.getAbsolutePath();
                    if (localFilePath.contains(".ftpsync")) {

                    } else {
                        FileTransferHelper uploadFile = new FileTransferHelper();
                        uploadFile.setLocalAbsolutePath(localFilePath);
                        uploadFile.setRemoteDirectory(remotePath);
                        uploadFile.setRemoteAbsolutePath(remoteFilePath);
                        FileTransferHelper uploadedFile = uploadSingleFile(uploadFile);
                        returnFilesList.add(uploadedFile);
                    }
                } else {
                    try {
                        connect();

                        setSecondProgress(new ProgressHelper("Creating " + remoteFilePath, 0));
                        
                        // create directory on the server
                        boolean created = makeDirectory(remoteFilePath);
                        if (created) {
                            Log.d(TAG, "Created the directory: " + remoteFilePath);
                        }
                        if (!isDirectory(remoteFilePath)) {
                            Log.d(TAG, "Remote File Path Not Found: " + remoteFilePath);
                        }
                        // upload the sub directory
                        String parent = remoteParentDirectory + "/" + item.getName();
                        if (remoteParentDirectory.equals("")) {
                            parent = item.getName();
                        }
                        localPath = item.getAbsolutePath();
                        uploadDirectory(returnFilesList, localPath, remotePath, parent);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return returnFilesList;
    }

    /**
     * Upload multiple files
     * @param tFiles List of files to be transfer
     * @return upload files data with status
     */
    public List<FileTransferHelper> upload(List<FileTransferHelper> tFiles)
    {
        List<FileTransferHelper> returnList = new ArrayList<FileTransferHelper>();
        Integer total = tFiles.size();
        setProgress(new ProgressHelper("", 0, total));
        for (FileTransferHelper file : tFiles) {
            if (!file.getLocalPath().equals("") && new File(file.getLocalPath()).isDirectory())
            {
                List<FileTransferHelper> subList = uploadDirectory(new ArrayList<FileTransferHelper>(), file.getLocalPath(), file.getRemotePath(), "");
                returnList.addAll(subList);
            }
            else if (!file.getLocalPath().contains(".ftpsync"))
            {
                returnList.add(uploadSingleFile(file));
            }
            setProgressPlusOne();
        }
        return returnList;
    }

    /**
     * Download single file
     * @param tFile transfer file data container
     */
    public FileTransferHelper downloadSingleFile(FileTransferHelper tFile)
    {
        return downloadSingleFile(tFile, true);
    }
    public FileTransferHelper downloadSingleFile(FileTransferHelper tFile,
                                               Boolean isSkippable)
    {
        tFile.setAction(Constants.ACTION.DOWNLOAD);
        String remotePath = tFile.getRemotePath();
        String localPath = tFile.getLocalPath();

        //Log.d(TAG, "from -> " + fromFilePath);
        //Log.d(TAG, "to -> " + toFilePath);

        this.clock_offset = clockSync(remotePath);

        Boolean status = false;
        String timeFormat = "%tY%<tm%<td%<tH%<tM%<tL";
        String timeNow = "";

        InputStream inputStream = null;
        OutputStream outputStream = null;
        File downloadFile = null;
        try
        {
            connect();
            if (ftps == null) {
                tFile.setStatus(false);
                return tFile;
            }

            String storePath = localPath;
            downloadFile = new File(storePath);
            Boolean exist = downloadFile.exists();
            Boolean skip = false;
            timeNow = String.format(timeFormat, new Date());
            final String downloadFileName = downloadFile.getName();

            if (isDirectory(remotePath)) {
                remotePath = remotePath + "/" + downloadFileName;
                Log.d(TAG, "Download a file from a directory - " + remotePath);
            }
            final Long downloadFileSize = getFileSize(remotePath);
            Date remoteFileLastModified = getLastModified(remotePath);

            if (exist)
            {
                if (isSkippable == true && checkSameFile(localPath, remotePath))
                {
                    // Check if there are the same file.
                    skip = true;
                    tFile.setSkip(skip);
                    status = true;
                    Log.d(TAG, "File has the same attributes, skip this file.");
                }
                else
                {
                    storePath = storePath + "-" + timeNow;
                    Log.d(TAG, "File exist, download as " + storePath);
                    downloadFile = new File(storePath);
                }
            }
            setSecondProgress(new ProgressHelper("Downloading " + downloadFileName + " ...", 0));

            if (!skip) {
                try {
                    if (downloadFile.exists() && !downloadFile.canWrite()) {
                        throw new IOException("Cannot write to file " + storePath);
                    }
                    ftps.setBufferSize(1024 * 2); // Set Buffer Size
                    inputStream = ftps.retrieveFileStream(remotePath);
                    if (inputStream == null || ftps.getReplyCode() == 550) {
                        throw new IOException("File unavailable - " + remotePath);
                    } else {
                        outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
                        Util.copyStream(inputStream, outputStream,
                                ftps.getBufferSize(),
                                CopyStreamEvent.UNKNOWN_STREAM_SIZE,
                                new CopyStreamAdapter() {
                                    private int pp = 0;
                                    @Override
                                    public void bytesTransferred(long totalBytesTransferred,
                                                                 int bytesTransferred, long streamSize) {
                                        if (totalBytesTransferred == 0) return;
                                        int percent = (int) ((float) totalBytesTransferred * 100 / (float) downloadFileSize);
                                        if (percent >= pp + 4) {
                                            pp = percent;
                                            //Log.d(TAG, "Downloading " + downloadFileName + " ... " + percent + "%");
                                            setSecondProgress(new ProgressHelper("Downloading " + downloadFileName + " ... " + percent + "%", percent));
                                        }
                                    }
                                }, true);
                        ftps.completePendingCommand();

                        if (exist) {
                            Log.d(TAG, "Rename file");
                            timeNow = String.format(timeFormat, new Date());
                            String backupFileName = localPath + ".backup-" + timeNow;
                            // Rename original file name to old
                            File fromO = new File(localPath);
                            File toO = new File(backupFileName);
                            if (fromO.exists()) fromO.renameTo(toO);
                            // Rename new file as original filename
                            File toN = new File(localPath);
                            if (downloadFile.exists()) downloadFile.renameTo(toN);
                            // Delete old file
                            if (new File(localPath).exists() && new File(backupFileName).exists()) {
                                boolean deleted = new File(backupFileName).delete();
                                Log.d(TAG, "remove backup file - " + deleted);
                            }
                        }

                        // Edit last modified date of local file
                        File newLocalFile = new File(localPath);
                        if (newLocalFile.exists() && remoteFileLastModified != null) {
                            Long remoteModTime = remoteFileLastModified.getTime();
                            Long setTime = remoteModTime + this.clock_offset;
                            newLocalFile.setLastModified(setTime);
                            if (!setTime.equals(newLocalFile.lastModified())){
                                Log.d(TAG, "Fail to setLastModified");
                                String touchTime = new SimpleDateFormat("yyyyMMdd.HHmmss").format(setTime);
                                String suCommand = "touch -t " + touchTime + " \"" + newLocalFile.getPath() + "\"";
                                runSuCommand(suCommand);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (outputStream != null) outputStream.close();
                    if (inputStream != null) inputStream.close();
                }
                status = FTPReply.isPositiveCompletion(ftps.getReplyCode());
                /*
                if (status) {
                    if (pDialog != null) pDialog.setSecondaryProgress(100);
                    //Log.d(TAG, "downloaded successfully.");
                }
                File downloadedFile = new File(toFilePath);
                long length = downloadedFile.length();
                if (downloadedFile.exists()) {
                    Log.d(TAG, "Download: " + toFilePath + " (" + length + " bytes)");
                }*/
            } else {
                setSecondProgress(new ProgressHelper("Skipped " + downloadFileName + "", 100));
            }
        }
        catch (FTPConnectionClosedException e)
        {
            tFile.setConnectionClosed(true);
            System.err.println("Server closed connection.");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        tFile.setStatus(status);

        setProgressPlusOne();

        return tFile;
    }

    /**
     * Download a whole directory (including its nested sub directories and files)
     * from a FTPS server.
     *
     * @param returnFilesList files data container
     * @param remoteDirectory Path of the destination directory on the server.
     * @param remoteParentDirectory Path of the parent directory of the current directory on the
     *                              server (used by recursive calls).
     * @param localDirectory Path of the local directory being uploaded.
     * @param remoteRootDirectory Path of the root destination directory on the server.
     *                           Prevent create root level folder on local.
     */
    public List<FileTransferHelper> downloadDirectory(List<FileTransferHelper> returnFilesList,
                                                    String remoteDirectory,
                                                    String remoteParentDirectory,
                                                    String localDirectory,
                                                    final String remoteRootDirectory)
    {
        try {
            connect();

            String remoteDirectoryList = remoteParentDirectory;
            if (!remoteDirectory.equals("")) {
                remoteDirectoryList += "/" + remoteDirectory;
            }

            FTPFile[] subFiles = ftps.listFiles(remoteDirectoryList);

            if (subFiles != null && subFiles.length > 0)
            {
                if (remoteDirectory.equals(""))
                {
                    setProgressPlusMax(subFiles.length);
                }
                for (FTPFile tFile : subFiles)
                {
                    String currentFileName = tFile.getName();
                    if (currentFileName.equals(".") || currentFileName.equals(".."))
                    {
                        // skip parent directory and the directory itself
                        continue;
                    }
                    if (currentFileName.contains(".ftpsync"))
                    {
                        // skip .ftpsync data file
                        continue;
                    }
                    String remoteFilePath = remoteParentDirectory + "/" + remoteDirectory + "/" + currentFileName;
                    if (remoteDirectory.equals(""))
                    {
                        remoteFilePath = remoteParentDirectory + "/" + currentFileName;
                    }

                    //remoteParentDirectory.replaceFirst(Pattern.quote(remoteRootDirecory), "")
                    String remoteSub = remoteParentDirectory.replaceFirst(remoteRootDirectory.replace("/", File.separator).replace("\\", File.separator), "");
                    String newLocalDirectoryPath = localDirectory + remoteSub
                            + File.separator + remoteDirectory + File.separator + currentFileName;
                    if (remoteDirectory.equals(""))
                    {
                        newLocalDirectoryPath = localDirectory + remoteSub + File.separator + currentFileName;
                    }
                    if (tFile.isDirectory())
                    {
                        setSecondProgress(new ProgressHelper("Creating " + newLocalDirectoryPath, 0));
                        // create the directory in saveDir
                        File newLocalDirectory = new File(newLocalDirectoryPath);
                        Boolean created = newLocalDirectory.mkdirs();
                        if (created)
                        {
                            Log.d(TAG, "Created the directory: " + newLocalDirectoryPath);
                        }
                        // download the sub directory
                        downloadDirectory(returnFilesList, currentFileName, remoteDirectoryList, localDirectory, remoteRootDirectory);
                    }
                    else
                    {
                        // download the file
                        FileTransferHelper downloadFile = new FileTransferHelper();
                        downloadFile.setRemoteAbsolutePath(remoteFilePath);
                        downloadFile.setLocalAbsolutePath(newLocalDirectoryPath);
                        FileTransferHelper downloadedFile = downloadSingleFile(downloadFile);
                        returnFilesList.add(downloadedFile);
                    }
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return returnFilesList;
    }

    /**
     * Download multiple files
     * @param tFiles List of files to be transfer
     * @return download files data with status
     */
    public List<FileTransferHelper> download(List<FileTransferHelper> tFiles)
    {
        List<FileTransferHelper> returnList = new ArrayList<FileTransferHelper>();
        Integer total = tFiles.size();
        setProgress(new ProgressHelper("", 0, total));
        for (FileTransferHelper file : tFiles) {
            File localDirectoryFile = new File(file.getLocalPath());
            if (localDirectoryFile.isFile()) {
                Log.d(TAG, "Download a file from a directory - " + localDirectoryFile.getPath());
            }
            if (file.getIsDirectoryOnly() == true && !localDirectoryFile.isFile()) {
                List<FileTransferHelper> subList = downloadDirectory(new ArrayList<FileTransferHelper>(),
                        "",
                        file.getRemotePath(),
                        file.getLocalPath(),
                        file.getRemotePath());
                returnList.addAll(subList);
            } else if (!file.getRemotePath().contains(".ftpsync")) {
                returnList.add(downloadSingleFile(file));
            }
            setProgressPlusOne();
        }
        return returnList;
    }

    /**
     * Remove single file
     * @param tFile transfer file data container
     */
    public FileTransferHelper removeSingleFile(FileTransferHelper tFile)
    {
        tFile.setAction(Constants.ACTION.REMOVE_REMOTE);
        String remotePath = tFile.getRemotePath();

        File deleteFile = new File(remotePath);
        final String deleteFileName = deleteFile.getName();
        setSecondProgress(new ProgressHelper("Removing " + deleteFileName + " ...", 0));

        try {
            connect();
            if (ftps == null) {
                tFile.setStatus(false);
                return tFile;
            }
            ftps.deleteFile(remotePath);
        }
        catch (FTPConnectionClosedException e)
        {
            tFile.setConnectionClosed(true);
            System.err.println("Server closed connection.");
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        try {
            Integer reply = ftps.getReplyCode();
            Boolean status = FTPReply.isPositiveCompletion(reply);
            tFile.setStatus(status);

            if (!isExist(remotePath)) {
                tFile.setStatus(true);
                tFile.setSkip(true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        setSecondProgress(new ProgressHelper("Removing " + deleteFileName + " ...", 100));
        setProgressPlusOne();

        return tFile;
    }

    /**
     * Remove a whole non-empty directory (including its nested sub directories and files)
     *
     * @param returnFilesList files data container
     * @param remoteParentDirectory Path of the parent directory of the current directory on the
     *                              server (used by recursive calls).
     * @param remoteDirectory Path of the destination directory on the server.
     */
    public List<FileTransferHelper> removeDirectory(List<FileTransferHelper> returnFilesList,
                                                  String remoteParentDirectory,
                                                  String remoteDirectory) {
        try {
            connect();
            String remoteDirectoryList = remoteParentDirectory;
            if (!remoteDirectory.equals("")) {
                remoteDirectoryList += "/" + remoteDirectory;
            }

            FTPFile[] subFiles = ftps.listFiles(remoteDirectoryList);
            if (subFiles != null && subFiles.length > 0) {
                if (remoteDirectory.equals(""))
                {
                    setProgressPlusMax(subFiles.length);
                }
                for (FTPFile aFile : subFiles) {
                    String currentFileName = aFile.getName();
                    if (currentFileName.equals(".") || currentFileName.equals("..")) {
                        // skip parent directory and the directory itself
                        continue;
                    }
                    String filePath = remoteParentDirectory + "/" + remoteDirectory + "/"
                            + currentFileName;
                    if (remoteDirectory.equals("")) {
                        filePath = remoteParentDirectory + "/" + currentFileName;
                    }

                    if (aFile.isDirectory()) {
                        // remove the sub directory
                        removeDirectory(returnFilesList, remoteDirectoryList, currentFileName);
                    } else {
                        // remove the file
                        FileTransferHelper removeFile = new FileTransferHelper();
                        removeFile.setRemoteAbsolutePath(filePath);
                        FileTransferHelper removedFile = removeSingleFile(removeFile);
                        returnFilesList.add(removedFile);
                    }
                }
            }
            // finally, remove the directory itself
            setSecondProgress(new ProgressHelper("Removing " + remoteDirectoryList, 0));
            boolean removed = ftps.removeDirectory(remoteDirectoryList);
            if (removed) {
                Log.d(TAG, "Removed the directory: " + remoteDirectoryList);
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return returnFilesList;
    }

    /**
     * Remove multiple files
     * @param tFiles List of files to be transfer
     * @return Removed files data with status
     */
    public List<FileTransferHelper> remove(List<FileTransferHelper> tFiles)
    {
        List<FileTransferHelper> returnList = new ArrayList<FileTransferHelper>();
        Integer total = tFiles.size();
        setProgress(new ProgressHelper("", 0, total));
        for (FileTransferHelper file : tFiles) {
            if (file.getIsDirectoryOnly() == true)
            {
                List<FileTransferHelper> subList = removeDirectory(new ArrayList<FileTransferHelper>(),
                        file.getRemoteDirectory(), "");

                returnList.addAll(subList);
                setProgressPlusOne();
            }
            else {
                returnList.add(removeSingleFile(file));
            }
        }
        return returnList;
    }

    public List<FileTransferHelper> deleteLocalDirectory(FileTransferHelper tFile, List<FileTransferHelper> returnFile)
    {
        tFile.setAction(Constants.ACTION.DELETE_LOCAL);
        String deleteFilePath = tFile.getLocalPath();
        File fileOrDirectory = new File(deleteFilePath);
        if (fileOrDirectory.exists()) {
            if (fileOrDirectory.isDirectory())
            {
                for (File child : fileOrDirectory.listFiles())
                {
                    FileTransferHelper childFile = new FileTransferHelper();
                    childFile.setLocalAbsolutePath(child.getAbsolutePath());
                    deleteLocalDirectory(childFile, returnFile);
                }
            }
            Boolean deleted = fileOrDirectory.delete();
            tFile.setStatus(deleted);
        } else {
            tFile.setStatus(true);
            tFile.setSkip(true);
            //tFile.setFileNotFound(true);
        }
        returnFile.add(tFile);
        return returnFile;
    }
    public List<FileTransferHelper> deleteLocal(List<FileTransferHelper> tFiles)
    {
        List<FileTransferHelper> returnList = new ArrayList<FileTransferHelper>();
        Integer total = tFiles.size();
        setProgress(new ProgressHelper("", 0, total));
        for (FileTransferHelper file : tFiles) {
            List<FileTransferHelper> subList = deleteLocalDirectory(file, new ArrayList<FileTransferHelper>());
            returnList.addAll(subList);
            setProgressPlusOne();
        }
        return returnList;
    }

    public Boolean makeDirectory(String filePath) throws IOException
    {
        return ftps.makeDirectory(filePath);
    }

    public String readFile(String path) throws IOException {
        if (ftps == null) return null;
        if (!isExist(path)) return null;
        String returnFile = "";
        BufferedReader reader = null;

        try {
            InputStream stream = ftps.retrieveFileStream(path);
            if (stream == null || ftps.getReplyCode() == 550) {
                throw new IOException("File unavailable - " + path);
            } else {
                reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                StringBuffer sb = new StringBuffer();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                returnFile = sb.toString();
                stream.close();
            }
        } finally {
            if (reader != null) try { reader.close(); } catch (IOException e) {}
        }
        ftps.completePendingCommand();
        return returnFile;
    }

    public Boolean writeFile(String path, String content) throws IOException {
        if (ftps == null) return false;
        InputStream stream = new ByteArrayInputStream(content.getBytes(Charset.forName("UTF-8")));
        ftps.storeFile(path, stream);
        stream.close();
        Integer reply = ftps.getReplyCode();
        return FTPReply.isPositiveCompletion(reply);
    }

    public Boolean hasAccess(String path) {
        if (ftps == null) return null;
        Boolean result = false;
        try {
            if (!ftps.isConnected()) connect();

            FTPFile current = null;
            try {
                current = ftps.mlistFile(path);
                if (current == null) return false;
                if (current.isDirectory()) {
                    path += File.separator + ".test";
                } else {
                    path = new File(path).getParent() + File.separator + ".test";
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                path += File.separator + ".test";
            }

            Log.d(TAG, "check hasAccess - path: " + path);
            InputStream os = new ByteArrayInputStream("rw".getBytes());
            ftps.storeFile(path, os);
            os.close();
            result = FTPReply.isPositiveCompletion(ftps.getReplyCode());

            if (result) {
                InputStream is = ftps.retrieveFileStream(path);
                if (is == null || ftps.getReplyCode() == 550) {
                    return false;
                } else {
                    is.close();
                    ftps.deleteFile(path);
                }
                ftps.completePendingCommand();
                result = FTPReply.isPositiveCompletion(ftps.getReplyCode());
            }
        } catch (IOException e) {
            return false;
        }
        return result;
    }

    protected void runSuCommand(String cmd) throws IOException
    {
        Process p = Runtime.getRuntime().exec("su");
        DataOutputStream outs = new DataOutputStream(p.getOutputStream());
        outs.writeBytes(cmd + "\nexit");
    }


    public void setSecondProgress(ProgressHelper data) {
        if (mContext == null) return;
        Intent i = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                .putExtra(Constants.TRANSFER.SECOND_PROGRESS, data);
        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).sendBroadcast(i);
    }
    public void setProgress(ProgressHelper data) {
        if (mContext == null) return;
        Intent i = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                .putExtra(Constants.TRANSFER.PROGRESS, data);
        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).sendBroadcast(i);
    }
    public void setProgressPlusOne() {
        if (mContext == null) return;
        Intent i = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                .putExtra(Constants.TRANSFER.PROGRESS_PLUSONE, true);
        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).sendBroadcast(i);
    }
    public void setProgressPlusMax(Integer add) {
        if (mContext == null) return;
        Intent i = new Intent(Constants.ACTION.TRANSFER_PROGRESS)
                .putExtra(Constants.TRANSFER.PROGRESS_MAX, add);
        LocalBroadcastManager.getInstance(mContext.getApplicationContext()).sendBroadcast(i);
    }
}