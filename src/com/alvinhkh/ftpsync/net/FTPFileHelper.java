package com.alvinhkh.ftpsync.net;

import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

public class FTPFileHelper implements Serializable, Comparator<FTPFileHelper> {

    private String path = "/";
    private FTPFile file = null;
    private FTPFileHelper parent = null;
    private Date lastModified = null;
    private Long size = null;

    private Boolean isDirectory = false;
    private Boolean isFile = false;

    public String getPath() {
        if (path == null) return null;
        if (new File(path) == null) return null;
        return new File(path).getPath();
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date lastModified() {
        return lastModified;
    }

    public void setLastModified(Date time) {
        this.lastModified = time;
    }

    public long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Boolean getIsDirectory() {
        return isDirectory;
    }

    public void setIsDirectory(Boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public Boolean getIsFile() {
        return isFile;
    }

    public void setIsFile(Boolean isFile) {
        this.isFile = isFile;
    }

    public FTPFile getFTPFile() {
        if (file == null) file = new FTPFile();
        return file;
    }

    public void setFTPFile(FTPFile ftpFile) {
        this.file = ftpFile;
    }

    public FTPFileHelper getParent() {
        return parent;
    }

    public void setParent(FTPFileHelper parent) {
        this.parent = parent;
    }

    @Override
    public int compare(FTPFileHelper lhs, FTPFileHelper rhs) {
        return lhs.getPath().compareToIgnoreCase(rhs.getPath());
    }

}