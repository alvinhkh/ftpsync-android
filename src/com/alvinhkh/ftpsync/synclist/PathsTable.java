package com.alvinhkh.ftpsync.synclist;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class PathsTable {

    public static final String TABLE_PATHS = "paths";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LOCAL = "local";
    public static final String COLUMN_REMOTE = "remote";

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_PATHS
            + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_LOCAL + " text not null, "
            + COLUMN_REMOTE + " text not null"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(PathsDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PATHS);
        onCreate(db);
    }
}
