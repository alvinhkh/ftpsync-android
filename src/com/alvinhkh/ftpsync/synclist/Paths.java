package com.alvinhkh.ftpsync.synclist;

public class Paths {

    private Long id;
    private String local;
    private String remote;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getRemote() {
        return remote;
    }

    public void setRemote(String remote) {
        this.remote = remote;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return local;
    }

}
