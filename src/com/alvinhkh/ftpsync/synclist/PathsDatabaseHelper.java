package com.alvinhkh.ftpsync.synclist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PathsDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "paths.db";
    private static final int DATABASE_VERSION = 1;


    public PathsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        PathsTable.onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        PathsTable.onUpgrade(database, oldVersion, newVersion);
    }

}