package com.alvinhkh.ftpsync;

public class Constants {

    public interface ACTION {
        String SINGLE_TRANSFER = "com.alvinhkh.ftpsync.action.SINGLE_TRANSFER";
        String TRANSFER = "com.alvinhkh.ftpsync.action.TRANSFER";
        String TRANSFER_PROGRESS = "com.alvinhkh.ftpsync.action.TRANSFER_PROGRESS";
        String SYNC = "com.alvinhkh.ftpsync.action.SYNC";
        String UPLOAD = "com.alvinhkh.ftpsync.action.UPLOAD";
        String DOWNLOAD = "com.alvinhkh.ftpsync.action.DOWNLOAD";
        String REMOVE_REMOTE = "com.alvinhkh.ftpsync.action.REMOVE_REMOTE";
        String DELETE_LOCAL = "com.alvinhkh.ftpsync.action.DELETE_LOCAL";
        String SKIP = "com.alvinhkh.ftpsync.action.SKIP";
        String CONFLICT = "com.alvinhkh.ftpsync.action.CONFLICT";

        String START_FOREGROUND = "com.alvinhkh.ftpsync.action.START_FOREGROUND";
        String STOP_FOREGROUND = "com.alvinhkh.ftpsync.action.STOP_FOREGROUND";
        String FOREGROUND_STATE = "com.alvinhkh.ftpsync.action.FOREGROUND_STATE";
        String STOP_SERVICE = "com.alvinhkh.ftpsync.action.STOP_SERVICE";

        String SYNC_CLOCK = "com.alvinhkh.ftpsync.action.SYNC_CLOCK";
        String START_CLOCK = "com.alvinhkh.ftpsync.action.START_CLOCK";
        String STOP_CLOCK = "com.alvinhkh.ftpsync.action.STOP_CLOCK";

        String NETWORK_STATE = "com.alvinhkh.ftpsync.action.NETWORK_STATE";
        String GET_NETWORK_STATE = "com.alvinhkh.ftpsync.action.GET_NETWORK_STATE";
        String BATTERY_STATE = "com.alvinhkh.ftpsync.action.BATTERY_STATE";

        String DONE = "com.alvinhkh.ftpsync.action.DONE";
    }

    public interface ACTION_ID {
        Integer FILE_PICKER_LOCAL = 1110;
        Integer FILE_PICKER_REMOTE = 1111;
        Integer CURSOR_LOADER = 1200;
        Integer SYNC_LIST_ITEM_CHECK = 1300;
    }

    public interface TRIGGER {
        String IS = "com.alvinhkh.ftpsync.trigger.IS";
        String ALARM = "com.alvinhkh.ftpsync.trigger.ALARM";
        String MANUAL = "com.alvinhkh.ftpsync.trigger.MANUAL";
        String BUTTON = "com.alvinhkh.ftpsync.trigger.BUTTON";
        String CANCEL = "com.alvinhkh.ftpsync.trigger.CANCEL";
        String MISSING_SETTINGS = "com.alvinhkh.ftpsync.trigger.MISSING_SETTINGS";
    }

    public interface NET {
        String UNKNOWN = "com.alvinhkh.ftpsync.net.UNKNOWN";
        String WIFI = "com.alvinhkh.ftpsync.net.WIFI";
        String MOBILE = "com.alvinhkh.ftpsync.net.MOBILE";
        String ETHERNET = "com.alvinhkh.ftpsync.net.ETHERNET";
    }

    public interface TRANSFER {
        String FILE = "com.alvinhkh.ftpsync.transfer.FILE";
        String FILES = "com.alvinhkh.ftpsync.transfer.FILES";
        String LOG_ID = "com.alvinhkh.ftpsync.transfer.LOG_ID";
        String MESSAGE = "com.alvinhkh.ftpsync.transfer.MESSAGE";
        String MESSAGE_ID = "com.alvinhkh.ftpsync.transfer.MESSAGE_ID";
        String PROGRESS = "com.alvinhkh.ftpsync.transfer.PROGRESS";
        String SECOND_PROGRESS = "com.alvinhkh.ftpsync.transfer.SECOND_PROGRESS";
        String PROGRESS_PLUSONE = "com.alvinhkh.ftpsync.transfer.PROGRESS_PLUSONE";
        String PROGRESS_MAX = "com.alvinhkh.ftpsync.transfer.PROGRESS_MAX";
        String PROGRESS_DONE = "com.alvinhkh.ftpsync.transfer.PROGRESS_DONE";
        String PROGRESS_INDETERMINATE = "com.alvinhkh.ftpsync.transfer.PROGRESS_INDETERMINATE";
        String RETURN_STATUS = "com.alvinhkh.ftpsync.transfer.STATUS";
        String RETURN_FILE_NOT_FOUND = "com.alvinhkh.ftpsync.transfer.RETURN_FILE_NOT_FOUND";
        String RETURN_CONNECTION_CLOSED = "com.alvinhkh.ftpsync.transfer.RETURN_CONNECTION_CLOSED";

        String ACTION = "com.alvinhkh.ftpsync.transfer.ACTION";
        String UPLOAD = "com.alvinhkh.ftpsync.transfer.UPLOAD";
        String DOWNLOAD = "com.alvinhkh.ftpsync.transfer.DOWNLOAD";
        String REMOVE_REMOTE = "com.alvinhkh.ftpsync.transfer.REMOVE_REMOTE";
        String DELETE_LOCAL = "com.alvinhkh.ftpsync.transfer.DELETE_LOCAL";
        String SKIP = "com.alvinhkh.ftpsync.transfer.SKIP";
        String UNKNOWN = "com.alvinhkh.ftpsync.transfer.UNKNOWN";
        String CONFLICT = "com.alvinhkh.ftpsync.transfer.CONFLICT";
    }

    public interface NOTIFICATION {
        Integer FOREGROUND_SERVICE = 1000;
    }
}
