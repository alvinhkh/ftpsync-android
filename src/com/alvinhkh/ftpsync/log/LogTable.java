package com.alvinhkh.ftpsync.log;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class LogTable {

    public static final String TABLE_LOG = "log";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LOCAL = "local";
    public static final String COLUMN_REMOTE = "remote";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_ACTION = "action";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_TYPE = "type";

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_LOG
            + "("
            + COLUMN_ID + " INTEGER primary key autoincrement, "
            + COLUMN_LOCAL + " TEXT not NULL, "
            + COLUMN_REMOTE + " TEXT not NULL, "
            + COLUMN_DATE + " INTEGER not NULL, "
            + COLUMN_ACTION + " TEXT not NULL, "
            + COLUMN_STATUS + " INTEGER not NULL, "
            + COLUMN_TYPE + " TEXT not NULL"
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(LogDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG);
        onCreate(db);
    }
}
