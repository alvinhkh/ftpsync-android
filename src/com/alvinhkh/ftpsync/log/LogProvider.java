package com.alvinhkh.ftpsync.log;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;

public class LogProvider extends ContentProvider {

    private LogDatabaseHelper mHelper;

    // used for the UriMatcher
    private static final int LOGS = 10;
    private static final int LOG_ID = 20;

    private static final String AUTHORITY = "com.alvinhkh.ftpsync.log";

    private static final String BASE_PATH = "logs";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
            + "/" + BASE_PATH);

    public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/logs";
    public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/log";

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, BASE_PATH, LOGS);
        sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", LOG_ID);
    }

    @Override
    public boolean onCreate() {
        mHelper = new LogDatabaseHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        // Using SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        // check if the caller has requested a column which does not exists
        checkColumns(projection);

        // Set the table
        queryBuilder.setTables(LogTable.TABLE_LOG);

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case LOGS:
                break;
            case LOG_ID:
                // adding the ID to the original query
                queryBuilder.appendWhere(LogTable.COLUMN_ID + "="
                        + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection,
                selectionArgs, null, null, sortOrder);
        // make sure that potential listeners are getting notified
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mHelper.getWritableDatabase();
        int rowsDeleted = 0;
        long id = 0;
        switch (uriType) {
            case LOGS:
                id = sqlDB.insert(LogTable.TABLE_LOG, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if (id > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.parse(BASE_PATH + "/#" + id);
        } else {
            throw new IllegalArgumentException("Fail to add a new record into " + uri);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mHelper.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case LOGS:
                rowsDeleted = sqlDB.delete(LogTable.TABLE_LOG, selection, selectionArgs);
                break;
            case LOG_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(LogTable.TABLE_LOG,
                            LogTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsDeleted = sqlDB.delete(LogTable.TABLE_LOG,
                            LogTable.COLUMN_ID + "=" + id + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                if (!TextUtils.isEmpty(selection)) {
                    rowsDeleted = sqlDB.delete(LogTable.TABLE_LOG, selection, selectionArgs);
                } else {
                    throw new IllegalArgumentException("Unknown URI: " + uri);
                }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = mHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (uriType) {
            case LOGS:
                rowsUpdated = sqlDB.update(LogTable.TABLE_LOG,
                        values,
                        selection,
                        selectionArgs);
                break;
            case LOG_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(LogTable.TABLE_LOG,
                            values,
                            LogTable.COLUMN_ID + "=" + id,
                            null);
                } else {
                    rowsUpdated = sqlDB.update(LogTable.TABLE_LOG,
                            values,
                            LogTable.COLUMN_ID + "=" + id + " and " + selection,
                            selectionArgs);
                }
                break;
            default:
                if (!TextUtils.isEmpty(selection)) {
                    rowsUpdated = sqlDB.update(LogTable.TABLE_LOG, values, selection, selectionArgs);
                } else {
                    throw new IllegalArgumentException("Unknown URI: " + uri);
                }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        String[] available = {
                LogTable.COLUMN_ACTION,
                LogTable.COLUMN_DATE,
                LogTable.COLUMN_LOCAL,
                LogTable.COLUMN_REMOTE,
                LogTable.COLUMN_STATUS,
                LogTable.COLUMN_TYPE,
                LogTable.COLUMN_ID
        };
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns)) {
                throw new IllegalArgumentException("Unknown columns in projection");
            }
        }
    }

} 