package com.alvinhkh.ftpsync.preference;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.ListPreference;
import android.preference.PreferenceGroup;
import android.preference.SwitchPreference;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;

import com.alvinhkh.ftpsync.Constants;
import com.alvinhkh.ftpsync.R;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

public class PreferencesActivity extends PreferenceActivity {

    private static final String TAG = "PreferencesActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Set Status Bar background colour in LOLLIPOP+
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }
        getActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

        Intent intent = getIntent();
        if (intent.getBooleanExtra(Constants.TRIGGER.MISSING_SETTINGS, false) == true) {
            // Show Missing setting snack bar
            SnackbarManager.show(
                    Snackbar.with(getApplicationContext()) // context
                            .type(SnackbarType.MULTI_LINE) // Set is as a multi-line snackbar
                            .text(R.string.require_ftps_settings)
                            .duration(Snackbar.SnackbarDuration.LENGTH_INDEFINITE)
                    , this); // activity where it is displayed
        }
    }

    public static class SettingsFragment extends PreferenceFragment implements
            OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            // Set Default values from XML attribute
            PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, false);

            // Make sure we have client id
            ClientIdPreference clientIdPreference = (ClientIdPreference) findPreference("client_id");
            if (clientIdPreference.getText().isEmpty()) {
                clientIdPreference.renewId();
                Log.d(TAG, "Client ID (Updated): " + clientIdPreference.getText());
            }

            // Set Summary
            initSummary(getPreferenceScreen());
        }

        @Override
        public void onResume() {
            super.onResume();
            // Set up a listener whenever a key changes
            PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            // Unregister the listener whenever a key changes
            PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(this);
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                              String key) {
            updatePrefSummary(findPreference(key));
        }

        private void initSummary(Preference p) {
            if (p instanceof PreferenceGroup) {
                PreferenceGroup pGrp = (PreferenceGroup) p;
                for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
                    initSummary(pGrp.getPreference(i));
                }
            } else {
                updatePrefSummary(p);
            }
        }

        private void updatePrefSummary(Preference p) {
            if (p instanceof EditTextPreference)
            {
                EditTextPreference editTextPref = (EditTextPreference) p;
                if (p.getTitle().toString().equals(getString(R.string.title_ftps_password)))
                {
                    p.setSummary("******");
                }
                else if (editTextPref.getText() != null)
                {
                    p.setSummary(editTextPref.getText());
                }
            }
            if (p instanceof SwitchPreference)
            {
                SwitchPreference switchPref = (SwitchPreference) p;
                if (p.getTitle().toString().equals(getString(R.string.title_ftps_protocol)))
                {
                    // Use summaryOn and summaryOff on xml
                } else if (p.getTitle().toString().equals(getString(R.string.title_sync_wifi_only))) {
                    // Nothing
                } else {
                    p.setSummary(switchPref.isChecked() ?
                            getString(R.string.enabled) :
                            getString(R.string.disabled));
                }
            }
            if (p instanceof CheckBoxPreference)
            {
                CheckBoxPreference checkBoxPref = (CheckBoxPreference) p;
                p.setSummary(checkBoxPref.isChecked() ?
                        getString(R.string.enabled) :
                        getString(R.string.disabled));
            }
            if (p instanceof ListPreference)
            {
                ListPreference listPref = (ListPreference) p;
                p.setSummary(listPref.getEntry());
            }
            if (p instanceof MultiSelectListPreference)
            {
                EditTextPreference editTextPref = (EditTextPreference) p;
                p.setSummary(editTextPref.getText());
            }
            // Custom Preference
            if (p instanceof NumbersPickerPreference)
            {
                NumbersPickerPreference numbersPickerDialog = (NumbersPickerPreference) p;
                p.setSummary(numbersPickerDialog.getText());
            }
            if (p instanceof ClientIdPreference)
            {
                ClientIdPreference clientIdPreference = (ClientIdPreference) p;
                p.setSummary(clientIdPreference.getText());
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {

        PreferencesHelper preferencesHelper = new PreferencesHelper().getFtpsSettings(getApplicationContext());
        if (!preferencesHelper.isSet()) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.missing_information))
                    .setMessage(getString(R.string.alert_missing_ftps_settings))
                    .setPositiveButton(R.string.button_continue, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    })
                    .setNegativeButton(R.string.button_exit, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
                            startActivity(intent);
                            finish();
                            return;
                        }
                    })
                    .show();
        } else {
            super.onBackPressed();
        }
        return;

    }
}