package com.alvinhkh.ftpsync.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Toast;

import java.util.Arrays;

public class SyncIntervalPreference extends NumbersPickerPreference {

    public SyncIntervalPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SyncIntervalPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setValues(int[] values) {
        if (values[0] == 0 && values[1] == 0) {
            Toast.makeText(getContext(), "Cannot be zero", Toast.LENGTH_SHORT).show();
        } else if (values[0] == 0 && values[1] < 2) {
            Toast.makeText(getContext(), "At least 2 minutes", Toast.LENGTH_SHORT).show();
        } else {
            this.values = values;
            persistValue = Arrays.toString(values);
            persistString(persistValue);
        }
    }

}
