package com.alvinhkh.ftpsync.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.StringTokenizer;

public class PreferencesHelper {

    private String ftpsHost = "";
    private Integer ftpsPort = 21;
    private String ftpsProtocol = "TLS";
    private String ftpsUsername = "";
    private String ftpsPassword = "";

    private Boolean syncAuto = false;
    private Integer syncInterval = 0;
    private Boolean syncWifiOnly = false;
    private String syncPower = "Any";
    private Float syncBattery = (float) 0.1;
    private Integer syncConflictAction = 9; // 0: Skip, 1: Local (Upload), 2: Remote (Download), 9: User Select

    private Boolean notificationAlwaysOn = true;

    private String localClientId = "";

    public PreferencesHelper() {
    }

    // FTPS

    public Boolean isSet() {
        return !ftpsHost.equals("") && ftpsPort > -1;
    }

    public String getFtpsHost() {
        return ftpsHost;
    }

    public void setFtpsHost(String ftpsHost) {
        this.ftpsHost = ftpsHost;
    }
    
    public Integer getFtpsPort() {
        return ftpsPort;
    }

    public void setFtpsPort(Integer ftpsPort) {
        this.ftpsPort = ftpsPort;
    }

    public String getFtpsProtocol() {
        return ftpsProtocol;
    }

    public void setFtpsProtocol(String ftpsProtocol) {
        this.ftpsProtocol = ftpsProtocol;
    }

    public String getUsername() {
        return ftpsUsername;
    }

    public void setUsername(String username) {
        this.ftpsUsername = username;
    }

    public String getPassword() {
        return ftpsPassword;
    }

    public void setPassword(String password) {
        this.ftpsPassword = password;
    }

    public String getLocalClientId() {
        return localClientId;
    }

    public void setLocalClientId(String localClientId) {
        this.localClientId = localClientId;
    }

    public PreferencesHelper getFtpsSettings(Context ctx) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        setFtpsHost(sPref.getString("ftps_server", ""));
        try {
            setFtpsPort(Integer.valueOf(sPref.getString("ftps_port", "21")));
        } catch (ClassCastException e) {
            e.printStackTrace();
            setFtpsPort(-1);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            setFtpsPort(-1);
        }
        if (sPref.getBoolean("ftps_protocol_tls", false)) {
            setFtpsProtocol("TLS");
        } else {
            setFtpsProtocol("SSL");
        }
        setUsername(sPref.getString("ftps_username", ""));
        setPassword(sPref.getString("ftps_password", ""));
        setLocalClientId(sPref.getString("client_id", ""));
        return this;
    }

    // Sync

    public Boolean getSyncAuto() {
        return syncAuto;
    }

    public void setSyncAuto(Boolean syncAuto) {
        this.syncAuto = syncAuto;
    }

    public Integer getSyncInterval() {
        return syncInterval;
    }

    public void setSyncInterval(Integer syncInterval) {
        this.syncInterval = syncInterval;
    }

    public Boolean getSyncWifiOnly() {
        return syncWifiOnly;
    }

    public void setSyncWifiOnly(Boolean syncWifiOnly) {
        this.syncWifiOnly = syncWifiOnly;
    }

    public String getSyncPower() {
        return syncPower;
    }

    public void setSyncPower(String syncPower) {
        this.syncPower = syncPower;
    }

    public Float getSyncBattery() {
        return syncBattery;
    }

    public void setSyncBattery(Float syncBattery) {
        this.syncBattery = syncBattery;
    }

    public Integer getSyncConflictAction() {
        return this.syncConflictAction;
    }

    public void setSyncConflictAction(Integer syncConflictAction) {
        this.syncConflictAction = syncConflictAction;
    }

    public PreferencesHelper getSyncSettings(Context ctx) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        setSyncAuto(sPref.getBoolean("sync_automatically", false));
        Integer[] times = toIntegers(sPref.getString("sync_interval", "0"));
        setSyncInterval(toTimeInMillis(times));
        setSyncWifiOnly(sPref.getBoolean("sync_wifi_only", false));
        setSyncPower(sPref.getString("sync_power", ""));
        setSyncBattery((float) toIntegers(sPref.getString("sync_battery", "0"))[0] / 100);
        setSyncConflictAction(Integer.parseInt(sPref.getString("sync_conflict_action", "3")));
        return this;
    }

    public Boolean getNotificationAlwaysOn() {
        return this.notificationAlwaysOn;
    }

    public void setNotificationAlwaysOn(Boolean notificationAlwaysOn) {
        this.notificationAlwaysOn = notificationAlwaysOn;
    }

    public PreferencesHelper getNotificationSettings(Context ctx) {
        SharedPreferences sPref = PreferenceManager.getDefaultSharedPreferences(ctx);
        setNotificationAlwaysOn(sPref.getBoolean("notification_always_on", true));
        return this;
    }

    public static Integer[] toIntegers(String numbers) {
        numbers = numbers.replaceAll("[^0-9,]", "" );
        StringTokenizer st = new StringTokenizer(numbers, ",");
        Integer[] intArr = new Integer[st.countTokens()];
        Integer i = 0;
        while (st.hasMoreElements()) {
            intArr[i] = Integer.parseInt((String) st.nextElement());
            i++;
        }
        return intArr;
    }

    public static Integer toTimeInMillis(Integer[] values) {
        Integer m = 0;
        for (int i = 0; i < values.length; i++) {
            /*if (i == 0) {
                // Days
                m += values[i] * 24 * 60 * 60 * 1000;
            } else */if (i == 0) {
                // Hours
                m += values[i] * 60 * 60 * 1000;
            } else if (i == 1) {
                // Minutes
                m += values[i] * 60 * 1000;
            }
        }
        return m;
    }



}
