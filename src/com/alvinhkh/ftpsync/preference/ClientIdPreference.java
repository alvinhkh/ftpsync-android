package com.alvinhkh.ftpsync.preference;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.alvinhkh.ftpsync.R;

import java.util.UUID;

public class ClientIdPreference extends DialogPreference {

    private TextView mIdText = null;

    private final String DEFAULT_VALUE = UUID.randomUUID().toString();

    public ClientIdPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.dialog_pref_client_id);
    }

    @Override
    protected void onBindDialogView(View view) {
        mIdText = (TextView) view.findViewById(R.id.text_id);
        mIdText.setText(getText());

        super.onBindDialogView(view);
    }

    @Override
    protected void showDialog(Bundle state)
    {
        super.showDialog(state);

        final AlertDialog d = (AlertDialog) getDialog();
        d.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.title_confirmation)
                        .setMessage(R.string.are_you_sure_to_renew_id)
                        .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                renewId();
                                updateSummary();
                            }
                        })
                        .setNegativeButton(R.string.button_no, null).show();
            }
        });
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return UUID.randomUUID().toString();
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        String id;
        if (restorePersistedValue) {
            id = getPersistedString(DEFAULT_VALUE);
        } else {
            id = defaultValue.toString();
        }
        persistString(id);
    }

    @Override
    protected void onPrepareDialogBuilder(AlertDialog.Builder builder)
    {
        super.onPrepareDialogBuilder(builder);
        builder.setNegativeButton(R.string.button_renew, this);
    }

    public void renewId() {
        persistString(UUID.randomUUID().toString());
        if (mIdText != null)
            mIdText.setText(getText());
        notifyDependencyChange(shouldDisableDependents());
        notifyChanged();
    }

    protected void updateSummary() {
        setSummary(getText());
    }

    protected String getText() {
        return getPersistedString("");
    }

}