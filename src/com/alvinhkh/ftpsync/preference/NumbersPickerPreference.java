package com.alvinhkh.ftpsync.preference;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.alvinhkh.ftpsync.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

/**
 * A {@link android.preference.Preference} that displays a number picker as a dialog.
 */
public class NumbersPickerPreference extends DialogPreference {

    protected List<NumberPicker> pickers = new ArrayList<NumberPicker>();
    protected int[] values;
    protected String persistValue;
    protected int[] max_value;
    protected int[] min_value;
    protected String[] entries;
    protected int numberOfEntry = 0;

    public NumbersPickerPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumbersPickerPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(attrs);
    }

    protected void initAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(
                attrs,
                R.styleable.NumbersPickerPreference);
        int entries_id = a.getResourceId(R.styleable.NumbersPickerPreference_entries, 0);
        if (entries_id != 0) {
            this.entries = getContext().getResources().getStringArray(entries_id);
        }
        int entryValuesMin_id = a.getResourceId(R.styleable.NumbersPickerPreference_entryValuesMin, 0);
        if (entryValuesMin_id != 0) {
            this.min_value = getContext().getResources().getIntArray(entryValuesMin_id);
        }
        int entryValuesMax_id = a.getResourceId(R.styleable.NumbersPickerPreference_entryValuesMax, 0);
        if (entryValuesMax_id != 0) {
            this.max_value = getContext().getResources().getIntArray(entryValuesMax_id);
        }
        this.numberOfEntry = Math.min(this.entries.length, Math.min(this.min_value.length, this.max_value.length));
        a.recycle();

    }

    @Override
    protected View onCreateDialogView() {
        LinearLayout dialogLayout = new LinearLayout(getContext());
        dialogLayout.setOrientation(LinearLayout.HORIZONTAL);
        dialogLayout.setGravity(Gravity.CENTER);
        dialogLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;

        this.pickers = new ArrayList<NumberPicker>();
        for (int i = 0; i < numberOfEntry; i++) {
            NumberPicker picker = new NumberPicker(getContext());
            picker.setLayoutParams(layoutParams);
            dialogLayout.addView(picker);
            this.pickers.add(picker);

            StringBuilder addText = new StringBuilder();
            if (i < entries.length) {
                addText.append(" ");
                addText.append(entries[i]);
                addText.append(" ");
            } else if (i < numberOfEntry - 1) {
                addText.append(" ");
            } else {
                addText.append("");
            }
            TextView textView = new TextView(getContext());
            textView.setLayoutParams(layoutParams);
            textView.setText(addText.toString());
            dialogLayout.addView(textView);
        }

        return dialogLayout;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        if (pickers == null || min_value == null || max_value == null) return;
        int i = 0;
        for (NumberPicker picker: pickers) {
            if (picker != null) {
                setPicker(picker, i);
            }
            i++;
        }
    }

    protected void setPicker(NumberPicker picker, int i) {
        picker.setMinValue(i < min_value.length ? min_value[i] : 0);
        picker.setMaxValue(i < max_value.length ? max_value[i] : 0);
        picker.setValue(getValue(i));
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            int i = 0;
            for (NumberPicker picker: pickers) {
                setValue(i, picker.getValue());
                i++;
            }
            updateSummary();
        }
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        int id = a.getResourceId(index, 0);
        if (id != 0) {
            return getContext().getResources().getIntArray(id);
        }
        return null;
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            if (defaultValue == null) {
                setValues(getPersistedString(persistValue));
            }
            else {
                setValues(getPersistedString(persistValue));
            }
        }
        else {
            setValues(getValues(defaultValue));
        }
    }

    protected void updateSummary() {
        setSummary(getText());
    }

    protected String getText() {
        if (entries == null) return "";
        int[] values = getIntegers(getPersistedString(persistValue));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            if (values[i] == 0) continue;
            sb.append(values[i]);
            if (entries != null && i < entries.length) {
                sb.append(" ");
                sb.append(entries[i]);
                sb.append(" ");
            } else if (i < values.length - 1) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    protected void setValue(int position, int value) {
        values[position] = value;
        setValues(values);
    }

    protected void setValues(int[] values) {
        this.values = values;
        persistValue = Arrays.toString(values);
        persistString(persistValue);
    }

    protected void setValues(String values) {
        this.values = getValues(values);
        persistValue = values;
        persistString(persistValue);
    }

    protected int getValue(int position) {
        this.values = getValues(persistValue);
        if (position < values.length) {
            return values[position];
        }
        return 0;
    }

    protected int[] getValues(Object value) {
        if (value instanceof int[]) {
            return (int[]) value;
        }
        return null;
    }

    protected int[] getValues(String persistValue) {
        return getIntegers(getPersistedString(persistValue));
    }

    protected static int[] getIntegers(String numbers) {
        numbers = numbers.replaceAll("[^0-9,]", "" );
        StringTokenizer st = new StringTokenizer(numbers, ",");
        int[] intArr = new int[st.countTokens()];
        int i = 0;
        while (st.hasMoreElements()) {
            intArr[i] = Integer.parseInt((String) st.nextElement());
            i++;
        }
        return intArr;
    }
}