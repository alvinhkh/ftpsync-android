package com.alvinhkh.ftpsync.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.NumberPicker;

public class SyncBatteryPreference extends NumbersPickerPreference {

    public SyncBatteryPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SyncBatteryPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setPicker(NumberPicker picker, final int i) {
        /*
        Integer integer = (int) ((max_value[i]+1)/5);
        String[] numbers = new String[integer];
        for (int j=0; j<numbers.length; j++) {
            if (j == 0) numbers[j] = j < min_value.length ? Integer.toString(min_value[j]) : "0";
            numbers[j] = Integer.toString(j * 5);
        }
        picker.setDisplayedValues(numbers);
        picker.setMinValue(0);
        picker.setMaxValue(numbers.length-1);
        */
        picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                int minVal = i < min_value.length ? min_value[i] : 0;
                int maxVal = i < max_value.length ? max_value[i] : 0;
                int setVal = minVal;
                if (newVal < oldVal) {
                    if (newVal == minVal) {
                        setVal = minVal;
                    } else {
                        setVal = oldVal - 5;
                    }
                } else {
                    if (oldVal == minVal) {
                        setVal = maxVal - 5;
                    } else {
                        setVal = oldVal + 5;
                    }
                }
                picker.setValue(setVal);
            }
        });
        picker.setMinValue(i < min_value.length ? min_value[i] : 0);
        picker.setMaxValue(i < max_value.length ? max_value[i] : 0);
        picker.setValue(getValue(i));
    }

}
